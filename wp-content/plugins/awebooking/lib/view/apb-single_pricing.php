<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 *  Detail room pricing
 *
 * @package		AweBooking/admin/
 * @author 		AweTeam
 */

$_date_curent = date_create(date('Y-m-d'));
date_add($_date_curent, date_interval_create_from_date_string((isset($_GET['mon']) ? $_GET['mon'] : '0').' month'));
$new_date = date_format($_date_curent, 'Y-m-d');

$month_curent = isset($_GET['mon']) ? $_GET['mon']+2 : 2;
$month_sub = isset($_GET['mon']) ? $_GET['mon']-2 : '-2';

if(date('m',strtotime($new_date)) == 12){
	$_year = date('Y',strtotime($new_date))+1;
}else{
	 $_year = date('Y',strtotime($new_date));
}

?>
<div id="awe-booking-wraper" class="awe-booking-wraper">
<div class="awe-plugin">
   <div class="clearfix" id="branding">       
	<?php do_action("header_room_single"); ?>
   </div>
	<div id="page">
		<div class="tabs-secondary clearfix"></div>

		<div class="clearfix" id="content">
			<div class="element-invisible"><a id="main-content"></a></div>

			<div class="region region-content">
				<div class="update-pricing">
					<div class="awe-form-item">
						<input id="awe_datepicker_start"  name="rooms_start_date" placeholder="Arrival Date" name="" type="text" class="form-text date-start-js">
					</div>
					<div class="awe-form-item">
						<input id="awe_datepicker_end"  name="rooms_end_date" placeholder="Departure Date " type="text" class="form-text date-end-js">
					</div>
					<div class="awe-form-item">
						<label><?php _e("Sun", 'awebooking') ?></label> <input name="day_options[]" value="Sunday" type="checkbox" class="form-text get_day_js">
						<label><?php _e("Mon", 'awebooking') ?></label> <input name="day_options[]" value="Monday" type="checkbox" class="form-text get_day_js">
						<label><?php _e("Tue", 'awebooking') ?></label> <input name="day_options[]" value="Tuesday" type="checkbox" class="form-text get_day_js">
						<label><?php _e("Wed", 'awebooking') ?></label> <input name="day_options[]" value="Wednesday" type="checkbox" class="form-text get_day_js">
						<label><?php _e("Thu", 'awebooking') ?></label> <input name="day_options[]" value="Thursday" type="checkbox" class="form-text get_day_js">
						<label><?php _e("Fri", 'awebooking') ?></label> <input name="day_options[]" value="Friday" type="checkbox" class="form-text get_day_js">
						<label><?php _e("Sat", 'awebooking') ?></label> <input name="day_options[7]" value="Saturday" type="checkbox" class="form-text get_day_js">
					</div>
					<div class="awe-form-item">
						<select name="operation">
							<option value="add"><?php _e("Add to price", 'awebooking') ?></option>
							<option value="sub"><?php _e("Subtract from price", 'awebooking') ?></option>
							<option value="replace"><?php _e("Replace price", 'awebooking') ?></option>
							<option value="increase"><?php _e("Increase price by % amount", 'awebooking') ?></option>
							<option value="decrease"><?php _e("Decrease price by % amount", 'awebooking') ?></option>
						</select>
					</div>
					<div class="awe-form-item awe-amount">
						<input name="amount" placeholder="Amount" type="text" class="form-text apb-int">
					</div>
					<div class=awe-button-margin>
						<button type="button" class="awe-add-pricing-js button"><?php _e("Update Unit Pricing", 'awebooking') ?></button>
						 <span class="spinner" style="display: none;"></span>
					</div>
				</div>
				<?php 
					$next = $this->awe_get_link("pricing","&mon=".$month_curent ."&year=".(date('Y', strtotime($new_date)))."&room_id=".$_GET['room_id']); 
					$prew = $this->awe_get_link("pricing","&mon=".$month_sub ."&year=".(date('Y', strtotime($new_date)))."&room_id=".$_GET['room_id']); 
				?>
				<div class="awe-block-calendar block block-system" >
					<div class="month">
						<a href="<?php echo $prew ?>" class="button btn-left" type="button"><?php _e('Prev','awebooking') ?></a>
						<div id="calendar"> </div>  
					</div>
					<div class="month">
						<a href="<?php echo $next ?>" class="button btn-right" type="button"><?php _e('Next','awebooking') ?></a>
						<div id="calendar2"> </div> 
					</div>
				</div>
				 <script>
					var awe_date_curent_1   = '<?php echo date('Y', strtotime($new_date)); ?>-<?php echo date('m', strtotime($new_date));?>';
					var awe_date_curent_2   = '<?php echo $_year; ?>-<?php echo date('m',strtotime(date('Y', strtotime($new_date))."-".(date('m', strtotime($new_date))+1)));?>';
					var awe_start_date      = '<?php echo date('m', strtotime($new_date));?>/01/<?php echo date('Y', strtotime($new_date)); ?>';
					var awe_end_date        = '<?php echo date('m',strtotime(date('Y', strtotime($new_date))."-".(date('m', strtotime($new_date))+1)));?>/01/<?php echo $_year ?>';
					var room_id             = <?php echo wp_kses($_GET['room_id'], '') ?>;
				</script>      
			</div>
		</div>
	</div>
</div>
</div>



