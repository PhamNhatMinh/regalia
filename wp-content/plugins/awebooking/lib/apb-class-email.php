<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 *   Email Class
 *
 * @class       WC_Email
 * @version     1.6
 * @package     awebooking/Classes/Emails
 * @author      awebooking
 */

Class APB_Email{

	public function __construct(){
		add_action( 'apb_email_header', array( $this, 'apb_email_header' ) );
		add_action( 'apb_email_footer', array( $this, 'apb_email_footer' ) );
	}

	/**
	 * Remove email object.
	 *
	 * @return void
	 * @since 1.11
	 */
	public function destroy(){
		remove_action( 'apb_email_header', array( $this, 'apb_email_header' ) );
		remove_action( 'apb_email_footer', array( $this, 'apb_email_footer' ) );
	}


	public function apb_sendMail( $to, $subject, $message, $no_message = 0 ) {
		require_once( ABSPATH . 'wp-includes/class-phpmailer.php' );
		$apb_mail_config = get_option( 'apb_mail_config' );
		if ( empty( $apb_mail_config ) ) {
			return false;
		}
		$mail = new PHPMailer();
		$mail->isSMTP();
		$mail->Host = strtolower( $apb_mail_config['host'] );
		$mail->Port = intval( $apb_mail_config['port'] );
		$mail->SMTPSecure = strtolower( $apb_mail_config['smtp_secure'] );
		$mail->SMTPAuth = true;
		$mail->Username = isset( $apb_mail_config['mail'] ) ? $apb_mail_config['mail'] : '';
		$mail->Password = isset( $apb_mail_config['pass'] ) ? $apb_mail_config['pass'] : '';
		//from
		$mail->setFrom( isset( $apb_mail_config['mail'] ) ? $apb_mail_config['mail'] : '', get_option( 'apb_email_from_name' ) );
		//to
		$mail->addAddress( $to, 'Awebooking User' );

		$mail->CharSet = 'utf-8';
		$mail->ContentType = 'text/html';

		/*content meseger*/
		//title
		$mail->Subject = $subject;
		//content
		$mail->MsgHTML( $message );

		//result notice
		if ( ! $mail->send() ) {
			echo 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
			echo 'Message sent!';
		}
	}

	public function apb_style_inline( $content ) {
		ob_start();

		include AWE_function::template_exsits( 'emails/apb-email-style' );

		$css = apply_filters( 'apb_email_styles', ob_get_clean() );


		// apply CSS styles inline for picky email clients
		$emogrifier = new Emogrifier( $content, $css );
		$content = $emogrifier->emogrify();

		return $content;
	}

	/**
	 * Wraps a message in the awebooking mail template.
	 *
	 * @param mixed $email_heading
	 * @param string $message
	 * @return string
	 */
	public function apb_wrap_message( $email_heading, $message, $plain_text = false ) {
		// Buffer
		ob_start();

		do_action( 'apb_email_header', $email_heading );

		echo wpautop( wptexturize( $message ) );

		do_action( 'apb_email_footer' );

		// Get contents
		$message = ob_get_clean();

		return $message;
	}

	/**
	 * Get the email header.
	 *
	 * @param mixed $email_heading heading for the email
	 */
	public function apb_email_header( $email_heading ) {
		include AWE_function::template_exsits( 'emails/apb-email-header', array( 'email_heading' => $email_heading ) );
	}

	/**
	 * Get the email footer.
	 */
	public function apb_email_footer() {
		include AWE_function::template_exsits( 'emails/apb-email-footer' );
	}

}
