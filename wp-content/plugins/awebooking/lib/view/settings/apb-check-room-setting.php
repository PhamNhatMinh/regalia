<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 *  Tempalte setting default room setting
 *
 * @version     1.0
 * @package     AweBooking/admin/
 * @author      AweTeam
 */

?>
 <div class="form-elements">
     <p><?php _e("How soon a booking can start ", 'awebooking') ?></p>
     <select name="rooms_booking_start_date">
         <option value="0"><?php _e('Same day bookings','awebooking') ?></option>
         <?php for($i = 1; $i<= 10; $i++): ?>
         <option <?php if(get_option('rooms_booking_start_date') == $i) echo "selected='selected'"; ?> value="<?php echo esc_attr($i) ?>"><?php _e('Day in advance','awebooking') ?> <?php echo $i ?></option>
         <?php endfor; ?>
     </select>
     <span class="description">
       <?php _e('Current date + some date.') ?>
    </span>
</div>
<div class="form-elements">
     <p><?php _e("Max Room", 'awebooking') ?></p>
     <select name="max_room">
         <?php for($i = 1; $i<= 10; $i++): ?>
         <option <?php if(get_option('max_room') == $i) echo "selected='selected'"; ?> value="<?php echo esc_attr($i) ?>"><?php echo $i ?></option>
         <?php endfor; ?>
     </select>
     <span class="description">
       <?php _e('Number of displayed room in check availability.') ?>
    </span>
</div>
   <div class="form-elements">
     <p><?php _e("Max adult", 'awebooking') ?></p>
     <select name="max_adult">
         <?php for($i = 1; $i<= 10; $i++): ?>
         <option <?php if(get_option('max_adult') == $i) echo "selected='selected'"; ?> value="<?php echo esc_attr($i) ?>"><?php echo $i ?></option>
         <?php endfor; ?>
     </select>
     <span class="description">
       <?php _e('Number of adult.') ?>
    </span>
</div>
 <div class="form-elements">
     <p><?php _e("Max Child", 'awebooking') ?></p>
     <select name="max_child">
         <?php for($i = 0; $i<= 10; $i++): ?>
         <option <?php if(get_option('max_child') == $i) echo "selected='selected'"; ?> value="<?php echo esc_attr($i) ?>"><?php echo $i ?></option>
         <?php endfor; ?>
     </select>
     <span class="description">
       <?php _e('Number of child.') ?>
    </span>
</div>
<div class="form-elements">
     <p><?php _e("Max Night", 'awebooking') ?></p>
     <select name="max_night">
         <?php for($i = 1; $i<= 10; $i++): ?>
         <option <?php if(get_option('max_night') == $i) echo "selected='selected'"; ?> value="<?php echo esc_attr($i) ?>"><?php echo $i ?></option>
         <?php endfor; ?>
     </select>
     <span class="description">
       <?php _e('Number of night.') ?>
    </span>
</div>

