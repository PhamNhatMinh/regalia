<?php
/**
 * AWE WC Setting
 *
 * @class 		Apb_Wc_Setting
 * @version		1.0
 * @package		AweBooking/Classes/
 * @author 		AweTeam
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class Apb_Wc_Setting.
 */
class Apb_Wc_Setting extends Process_ajax{

	/**
	 * Constructor.
	 */
	public function __construct() {
		add_filter( 'woocommerce_is_purchasable', array( $this, 'fix_purchased' ), 10, 2 );
		// add_filter( 'woocommerce_add_cart_item_data', array( $this, 'add_cart_item_custom_data_value' ), 10, 2 );
		add_filter( 'woocommerce_add_cart_item', array( $this, 'add_cart_item' ), 10, 1 );
		add_filter( 'woocommerce_get_cart_item_from_session', array( $this, 'apb_get_cart_item_from_session' ), 10, 2 );

		add_action( 'woocommerce_checkout_update_order_meta', array( $this, 'book_save_extra_checkout_fields' ), 10, 2 );
		add_filter( 'wc_order_statuses', array( $this, 'wc_order_statuses' ), 10, 1 );

		add_filter( 'woocommerce_checkout_after_customer_details', array( $this, 'add_new_fields_checkout' ), 10, 1 );

		add_action( 'woocommerce_thankyou', array( $this, 'apb_display_order_data' ), 20 );
		add_action( 'woocommerce_view_order', array( $this, 'apb_display_order_data' ), 20 );
	}

	/**
	 * Filter cart item.
	 * @param array $cart_item_meta Cart item meta.
	 * @param int   $product_id     Product ID.
	 */
	public function add_cart_item_custom_data_value( $cart_item_meta, $product_id ) {
		$cart_item_meta['from']     = $_POST['from'];
		$cart_item_meta['to']       = $_POST['to'];
		if ( isset( $_POST['num'] ) ) {
			$cart_item_meta['adult']     = $_POST['adult'][ $_POST['num'] ]['adult'];
			$cart_item_meta['child']     = $_POST['child'][ $_POST['num'] ]['child'];
		} else {
			$cart_item_meta['adult']     = $_POST['adult'];
			$cart_item_meta['child']     = $_POST['child'];
		}

		$price_package = 0;

		$count_date = count( AWE_function::range_date( $_POST['from'], $_POST['to'] ) );

		/*----------  Add package from check only room type ----------*/
		if ( isset( $_POST['search_by'] ) && 'room_type' == $_POST['search_by'] ) {

			$_price = ( float ) $_POST['price'];
			// var_dump($_POST);
			if ( isset( $_POST['package_data'] ) ) {
				$cart_item_meta['package_data']  =  $_POST['package_data'];
				global $wpdb;
				foreach ( $_POST['package_data'] as $item_package ) {
					$sql = $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}apb_booking_options where entity_type  = '%s' and id = %d",'room_type',$item_package['package_id']);
					$data_package = $wpdb->get_results( $sql );
					$_price += $data_package[0]->option_value;
				}
			}
		} else {
		  /*----------  Add package from check all room ----------*/
			if ( isset( $_POST['package_data'] ) ) {
				$cart_item_meta['package_data'] = $_POST['package_data'];
				foreach ( AWE_function::get_room_option( $_POST['product_id'], 'apb_room_type' ) as $item_package ) {
					foreach ( $_POST['package_data'] as $use_package ) {
						if ( $use_package['package_id'] == $item_package->id ) {
							// var_dump($use_package, $item_package);
							if ( ! empty( $item_package->revision_id ) ) {
								$price_package += ($item_package->option_value * ( $count_date - 1 ) );
							} else {
								$price_package += ( $item_package->option_value * $use_package['total'] );
							}
						}
					}
				}
			}
			$_price = AWE_function::get_total_price( AWE_function::get_pricing_of_days( $_POST['from'], $_POST['to'], $_POST['product_id'] ) );
		}

		// Get Price of option extra price.
		$room_id = absint( $_POST['product_id'] );
		$extra_adult    =  get_post_meta( $room_id, 'extra_adult', true );
		$extra_child    =  get_post_meta( $room_id, 'extra_child', true );
		$extra_sale     =  get_post_meta( $room_id, 'extra_sale', true );
		if ( ! empty( $extra_adult ) ) {
			$total_date = $count_date - 1;
			foreach ( $extra_adult as $item_extra_adult ) {
				if ( $_POST['adult'][ $_POST['num'] ]['adult'] == $item_extra_adult['number'] ) {
					$_price += $total_date * $item_extra_adult['price'];
				}
			}
		}
		if ( ! empty( $extra_child ) ) {
			$total_date = $count_date - 1;
			foreach ( $extra_child as $item_extra_child ) {
				if ( $_POST['child'][ $_POST['num'] ]['child'] == $item_extra_child['number'] ) {
					$_price += $total_date * $item_extra_child['price'];
				}
			}
		}
		$total_price = $_price;

		if ( ! empty( $extra_sale ) ) {
			$total_date = $count_date;
			$data_extra_sale = AWE_function::apb_get_extra_sale( $extra_sale, $total_date, $_POST['from'] );
			if ( ! empty( $data_extra_sale ) ) {
				if ( 'sub' == $data_extra_sale['sale_type'] ) {
					$total_price = $total_price - $data_extra_sale['amount'];
				}
				if ( 'decrease' == $data_extra_sale['sale_type'] ) {
					$total_price = $total_price - $data_extra_sale['amount'] / 100 * $total_price;
				}
			}
		}

		$total_price += $price_package;
		$cart_item_meta['price'] = $total_price;

		return $cart_item_meta;
	}

	public function add_cart_item( $cart_item ) {
		$cart_item['data']->set_price( $cart_item['price'] );
		$cart_item['quantity'] = 1;
		return $cart_item;
	}

	public function apb_get_cart_item_from_session( $cart_item, $values ) {
		if ( isset( $values['from'] ) ) {
			$cart_item['from']          = $values['from'];
			$cart_item['to']            = $values['to'];
			$cart_item['adult']         = $values['adult'];
			$cart_item['child']         = $values['child'];
			$cart_item['price']         = $values['price'];
			$cart_item['package_data']  = isset( $values['package_data'] ) ? $values['package_data'] : array();
		}

		$cart_item_data = $this->add_cart_item( $cart_item );
		return $cart_item_data;
	}

	public function book_save_extra_checkout_fields( $order_id, $posted ) {

		$data_order = WC()->cart->get_cart();
		$total_price = 0;
		foreach ( $data_order as $item ) {
			$from = $item['from'];
			$to = $item['to'];

			/**
			 * Add new order.
			 */
			$order_data['post_type']     = 'apb_order';
			$order_data['post_status']   = 'apb-pending';
			$order_data['ping_status']   = 'closed';
			$order_data['post_author']   = 1;
			$order_data['post_password'] = uniqid( 'order_' );
			$order_data['post_title']    = '#' . $order_id;
			$id = wp_insert_post( $order_data );
			$apb_order_id[] = $id;

			/*
			 * Add meta box
			 */
			$args_order[] = array(
				'id'              => $id,
				'custommer'       => get_current_user_id(),
				'room_adult'      => $item['adult'],
				'room_child'      => $item['child'],
				'from'            => $from,
				'to'              => $to,
				'order_room_id'   => $item['product_id'],
				'package_data'    => serialize( $item['package_data'] ),
				'total_price'     => $item['price'],
			);
			$total_price  += $item['price'];
			update_post_meta( $id, 'custommer', get_current_user_id() );
			update_post_meta( $id, 'room_adult', $item['adult'] );
			update_post_meta( $id, 'room_child', $item['child'] );
			update_post_meta( $id, 'from', $from );
			update_post_meta( $id, 'to', $to );
			update_post_meta( $id, 'order_room_id', $item['product_id'], '' );
			update_post_meta( $id, 'package_data', serialize( $item['package_data'] ), '' );
			update_post_meta( $id, 'posttype_shop_order_id', $order_id, '' );


			$start_month     = date( 'm', strtotime( $from ) );
			$end_month       = date( 'm', strtotime( $to ) );
			$start_year      = date( 'Y', strtotime( $from ) );
			$end_year        = date( 'Y', strtotime( $to ) );
			$start_date      = date( 'd', strtotime( $from ) );
			$end_date        = date( 'd', strtotime( $to ) );
			$list_month_avb  = $this->get_list_month_availability( $start_year, $end_year, $start_month, $end_month, $from, date( 'm/d/Y', strtotime( $to ) - DAY_IN_SECONDS ), 2 );
			$list_update     = $this->insert_availability( $list_month_avb, $item['product_id'] );

			################## UPDATE AVAILABILITY ##################
			if ( ! empty( $list_update ) ) {
				/*
				 *  Update  - array > 2
				 */
				if ( count( $list_update ) > 2 ) {
					/*
					 * Update availability Start date
					 */
				} else {
					################## Update  - array <= 2 ###################

					/*
					 * Update availability Start date
					 */
					$this->list_update_availability( $list_update, $item['product_id'], $from, date( 'm/d/Y', strtotime( $to ) - DAY_IN_SECONDS ), 3, '', 'start' );

					/*
					 * Update pring end date
					 */
					if ( count( $list_update ) > 1 ) {
						 $this->list_update_availability( $list_update, $item['product_id'], $from, date( 'm/d/Y', strtotime( $to ) - DAY_IN_SECONDS ), 3, '', 'end', 1 );
					}
				}
			}
		}

		update_post_meta( $order_id, 'custommer', get_current_user_id() );
		update_post_meta( $order_id, 'from', $args_order[0]['from'] );
		update_post_meta( $order_id, 'to', $args_order[0]['to'] );
		update_post_meta( $order_id, 'apb_order_id', $apb_order_id );
		update_post_meta( $order_id, 'apb_data_order', $args_order );
		update_post_meta( $order_id, 'order_type', 1 );

	}


	public function apb_display_order_data( $order_id ) {
		if ( ! empty( $_SESSION['apb_cart'] ) ) {
			$this->apb_sendmail( $order_id );
		}
		$order = wc_get_order( $order_id );
		$my_order = array(
			'ID'           =>  $order_id,
			'post_title'   => '#'. $order_id,
			'post_status'  => 'apb-pending',
		);
		wp_update_post( $my_order );
		$total_order_room = get_post_meta( $order_id, 'apb_data_order', true );
		?>

		<h2><?php esc_html_e( 'Booking Details', 'awebooking' ); ?></h2>
		<?php
		foreach ( $total_order_room as $item_book ) :
			$room_info = get_post( $item_book['order_room_id'] );
			AWE_function::update_available( $item_book['from'], $item_book['to'], $item_book['order_room_id'], 3 );
			?>
			<ul class="order_details">
				<li class="order">
					<?php esc_html_e( 'Room:', 'awebooking' ); ?> <strong><?php echo esc_html( $room_info->post_title ); ?></strong>
				</li>

				<li class="date">
					<?php esc_html_e( 'Booking duration:', 'awebooking' ); ?> <strong><?php echo date( get_option( 'date_format' ), strtotime( $item_book['from'] ) ) ?> - <?php echo date( get_option( 'date_format' ), strtotime( $item_book['to'] ) ) ?> </strong>
				</li>
				<li class="total">
					<?php esc_html_e( 'Numbers of Guests', 'awebooking' ); ?>
					<strong>
						<?php printf( esc_html__( 'Adult: %s', 'awebooking' ), absint( $item_book['room_adult'] ) ); ?>
						<?php printf( esc_html__( 'Child: %s', 'awebooking' ), absint( $item_book['room_child'] ) ); ?>
					</strong>
				</li>
				<li class="method">
					<?php esc_html_e( 'Price', 'awebooking' ); ?>
					<strong><span class="amount"><?php echo AWE_function::apb_price( $item_book['total_price'] ) ?></span></strong>
				</li>
			</ul>
		<?php endforeach;
	}

	public function apb_sendmail( $order_id ) {
		$order = wc_get_order( $order_id );
		/*----------  Send mail after checkout  ----------*/
		$config_mail = get_option( 'apb_mail_new_booking' );
		$subject = ! empty( $config_mail['subject'] ) ? $config_mail['subject'] : '[{site_title}] New customer booking ({order_number}) - {order_date}';
		$subject = AWE_function::email_str_replace( $subject, $order_id );
		$heading = ! empty( $config_mail['header'] ) ? $config_mail['header'] : 'New customer booking';
		$heading = AWE_function::email_str_replace( $heading, $order_id );

		ob_start();
		do_action( 'apb_mail_new_booking' );
		$message       = ob_get_clean();
		$email         = new APB_Email();

		if ( isset( $config_mail['admin_notice_status'] ) && 1 == $config_mail['admin_notice_status'] ) {
			$mail_admin = explode( ',', $config_mail['admin_mail_multi'] );
			foreach ( $mail_admin as $email_admin ) {

				$message_admin = '<h2>' . esc_html__( 'Orders Info', 'awebooking' ) . '</h2>';
				$message_admin .= $order->get_formatted_billing_address();
				$message_admin .= $message;
				$message_admin = $email->apb_style_inline( $email->apb_wrap_message( $heading, $message_admin ) );
				$email->apb_sendMail( $email_admin, $subject, $message_admin, 1 );

			}
		}
		$email->destroy();

		/* Send pending email */
		$config_mail = get_option( 'apb_mail_pending' );
		$subject = ! empty( $config_mail['subject'] ) ? $config_mail['subject'] : 'Your {site_title} booking receipt from {order_date}';
		$subject = AWE_function::email_str_replace( $subject, $order_id );
		$heading = ! empty( $config_mail['header'] ) ? $config_mail['header'] : 'Thank you for your booking';
		$heading = AWE_function::email_str_replace( $heading, $order_id );

		ob_start();
		do_action( 'apb_mail_pending_order' );
		$message       = ob_get_clean();
		$email         = new APB_Email();

		if ( isset( $config_mail['user_notice_status'] ) && 1 == $config_mail['user_notice_status'] ) {
			$message_user      = $email->apb_style_inline( $email->apb_wrap_message( $heading, $message ) );
			$email->apb_sendMail( $order->billing_email, $subject, $message_user );
		}
		$email->destroy();
		$_SESSION['apb_cart'] = array();
	}

	/**
	 * Set order wc alway true.
	 */
	public function fix_purchased( $purchasable, $product ) {
		return $purchasable = true;
	}

	public function wc_order_statuses( $order_statuses ) {
		unset( $order_statuses['wc-on-hold'] );
		$order_statuses_new = array(
			'apb-pending'    => _x( 'Pending Payment', 'Order status', 'awebooking' ),
			'apb-processing' => _x( 'Processing', 'Order status', 'awebooking' ),
			'apb-completed'  => _x( 'Completed', 'Order status', 'awebooking' ),
			'apb-cancelled'  => _x( 'Cancelled', 'Order status', 'awebooking' ),
		);

		return array_merge( $order_statuses_new, $order_statuses );
	}

	public function add_new_fields_checkout() {
		?>
		<div class="apb-room-selected_content">
			<?php do_action( 'apb_loop_item_cart_info' ); ?>
		</div>
		<?php
	}
}
