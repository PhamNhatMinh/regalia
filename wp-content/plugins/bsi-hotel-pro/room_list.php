<?php

include("includes/conf.class.php");
include("includes/admin.class.php");
if(isset($_GET['rid']) && isset($_GET['cid'])){
	$bsiAdminMain->delete_room();
	header("location:admin.php?page=Room-Manager");	
	exit;
}
//wp_enqueue_script( 'custom_script10', plugins_url().'/bsi-hotel-pro/js/bsi_datatables.js');
wp_enqueue_script( 'custom_script11', plugins_url().'/bsi-hotel-pro/js/DataTables/jquery.dataTables.js');
wp_enqueue_style('custom-style9', plugins_url().'/bsi-hotel-pro/css/data.table.css');
wp_enqueue_style('custom-style10', plugins_url().'/bsi-hotel-pro/css/jqueryui.css');
?>
<p>&nbsp;</p>
<div id="container-inside">
<span style="font-size:16px; font-weight:bold"><?php echo ROOM_LIST;?></span><span style="font-size:13px; color:#F00; padding-left:200px;"></span>
    <input type="button" value="<?php echo ADD_NEW_ROOM;?>" onClick="window.location.href='admin.php?page=aad-new-room&rid=0&cid=0'" style="background: #EFEFEF; float:right"/>
 <hr style="margin-top:10px;" />
  <table class="display datatable" border="0">
    <thead>
      <tr>
        <th><?php echo ROOM_TYPE;?></th>
        <th><?php echo ADULT_ROOM;?></th>
        <th><?php echo MAX_CHILD_PER_ROOM;?></th>
        <th><?php echo TOTAL_ROOM;?></th>
        <th>&nbsp;</th>
      </tr>
    </thead>
   <?php echo $bsiAdminMain->generateRoomListHtml()?> </table>
</div>
<script>
 jQuery(document).ready(function() {
	 	var oTable = jQuery('.datatable').dataTable( {
				 "responsive": true,
				"bJQueryUI": true,
				"sScrollX": "",
				"bSortClasses": false,
				"aaSorting": [[0,'asc']],
				"bAutoWidth": true,
				"bInfo": true,
				"sScrollY": "100%",	
				"sScrollX": "100%",
				"bScrollCollapse": true,
				"sPaginationType": "full_numbers",
				"bRetrieve": true,
				"oLanguage": {
								"sSearch": "Search:",
								"sInfo": "Showing _START_ to _END_ of _TOTAL_ entries",
								"sInfoEmpty": "Showing 0 to 0 of 0 entries",
								"sZeroRecords": "No matching records found",
								"sInfoFiltered": "(filtered from _MAX_ total entries)",
								"sEmptyTable": "No data available in table",
								"sLengthMenu": "Show _MENU_ entries",
								"oPaginate": {
												"sFirst":    "First",
												"sPrevious": "Previous",
												"sNext":     "Next",
												"sLast":     "Last"
											  }
							 }
	} ); 
} );
</script> 
</div>