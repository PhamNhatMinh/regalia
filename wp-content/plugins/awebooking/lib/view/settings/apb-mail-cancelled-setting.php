<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 *  Tempalte setting mail option
 *
 * @version		1.0
 * @package		AweBooking/admin/
 * @author 		AweTeam
 */
$apb_mail_cancel = get_option('apb_mail_cancel');
$list_setting_email_cancel_order = array(
	array(
		'class' => 'form-elements',
		'name' => __('Enable/Disable', 'awebooking'),
		'type' => array(
			'type'	=> 'checkbox',
			'name' => 'apb_mail_cancel[notice_status]',
			'class' => '',
			'id' => '',
			'placeholder' =>  '',
			'value' => '1',
			'_value' => isset($apb_mail_cancel['notice_status']) ? $apb_mail_cancel['notice_status'] : 1,
		),
		'desc' => 'Enable this email notification',
	),
	array(
		'class' => 'form-elements',
		'name' => __('Subject', 'awebooking'),
		'type' => array('type'	=> 'text','name' => 'apb_mail_cancel[subject]','class' => '','id' => '','placeholder' =>  'Cancelled order',
			'value' =>  isset($apb_mail_cancel['subject']) ? $apb_mail_cancel['subject'] : '[{site_title}] Cancelled order ({order_number})' ),
		'desc' => 'This controls the email subject line. Leave blank to use the default subject: <code>[{site_title}] Cancelled order ({order_number})</code>.',
	),
	array(
		'class' => 'form-elements',
		'name' => __('Email Heading', 'awebooking'),
		'type' => array('type'	=> 'text','name' => 'apb_mail_cancel[header]','class' => '','id' => '','placeholder' =>  'Cancelled order',
			'value' =>  isset($apb_mail_cancel['header']) ? $apb_mail_cancel['header'] : 'Cancelled booking' ),
		'desc' => 'This controls the main heading contained within the email notification. Leave blank to use the default heading: <code>Cancelled booking</code>.',
	),
	array(
		'class' => 'form-elements',
		'name'  => __( 'Email Text', 'awebooking' ),
		'type'  => array(
			'type'	      => 'textarea',
			'name'        => 'apb_mail_cancel[text]',
			'cols'        => '60',
			'class'       => '',
			'id'          => '',
			'placeholder' => '',
			'value'       => isset( $apb_mail_cancel['text'] ) ? $apb_mail_cancel['text'] : '',
		),
		'desc'	=> 'Leave blank or fill in some notices that you want to send to customer',
	),
);
?>
<h2><?php _e( 'Cancelled booking','awebooking' ); ?></h2>
<p><?php _e( 'Cancelled booking emails are sent to the recipient list when bookings have been marked cancelled ( Delete )','awebooking' ); ?>.</p>
<?php
$this->apb_render_setting_html($list_setting_email_cancel_order)
?>
<div class="form-elements">
	<p><?php _e('HTML template', 'awebooking' ); ?></p>
	<?php
	if(file_exists(get_stylesheet_directory().'/apb-template/emails/apb-customer-cancel-order.php')){
	?>
	<span class="description">
		<?php _e('To override and edit this email template copy', 'awebooking' ); ?>
		<?php _e('This template has been overridden by your theme and can be found in:','awebooking') ?> <code>yourtheme/apb-template/emails/apb-customer-cancel-order.php</code>.
	</span>
	<a class="button" href="<?php echo esc_url( wp_nonce_url(add_query_arg( 'remove_template', 'apb-customer-cancel-order' ) , 'apb_email_template_nonce', '_apb_email_nonce' ) ); ?>"><?php _e('Delete template file','awebooking' ); ?></a>
	<a class="button toggle_editor" href="#"><?php _e('View template','awebooking' ); ?></a>
	<div class="form-input editor">
		<textarea  cols="25" rows="20" data-name="template_html_code"><?php echo file_get_contents(AWE_function::template_exsits("emails/apb-customer-cancel-order")) ?></textarea>
		<input type="hidden" name="template_html_file" value="apb-customer-cancel-order">
	</div>
	<?php
	}else{
	?>
	<span class="description">
		<?php _e('To override and edit this email template copy', 'awebooking' ); ?>
		<code>awebooking/apb-template/emails/apb-customer-cancel-order.php</code><?php _e('to your theme folder', 'awebooking' ); ?>: <code>yourtheme/apb-template/emails/apb-customer-cancel-order.php</code>.
	</span>
	<a class="button" href="<?php echo esc_url( wp_nonce_url(add_query_arg( 'move_template', 'apb-customer-cancel-order' ) , 'apb_email_template_nonce', '_apb_email_nonce' ) ); ?>"><?php _e('Copy file to theme','awebooking' ); ?></a>
	<a class="button toggle_editor" href="#"><?php _e('View template','awebooking' ); ?></a>
	<div class="form-input editor">
		<textarea readonly="readonly" disabled="disabled" cols="25" rows="20"><?php echo file_get_contents(AWE_function::template_exsits("emails/apb-customer-cancel-order")) ?></textarea>
	</div>
	<?php
	}
	?>
</div>
