<div class="form-elements">
	<p><?php esc_html_e( 'DatePicker Language', 'awebooking' ); ?></p>
	<select name="datepicker_lang">
		<option value="default"><?php esc_html_e( 'English (United States)', 'awebooking' ); ?></option>
		<?php
		foreach ( AWE_function::apb_datepicker_lang() as $key => $value ) {
			?>
			<option <?php selected( AWE_function::get_datepicker_lang(), $key ); ?> value="<?php echo esc_attr( $key ) ?>"><?php echo esc_html( ucfirst( $value ) ) ?></option>
			<?php
		}
		?>
	</select>

	<span class="description">
		<?php esc_html_e( 'Select language default.', 'awebooking' ); ?>
	</span>
</div>

<div class="form-elements">
	<p><?php esc_html_e( 'Number calendar', 'awebooking' ); ?></p>
	<select name="datepicker_num_calendar">
		<?php for ( $i = 1; $i<= 12; $i++ ) : ?>
			<option <?php selected( $i, get_option( 'datepicker_num_calendar' ) ); ?> value="<?php echo esc_attr( $i ) ?>"><?php echo esc_html( $i ); ?></option>
		<?php endfor; ?>
	</select>

	<span class="description">
		<?php esc_html_e( 'Number of calendar datepicker', 'awebooking' ); ?>
	</span>
</div>
