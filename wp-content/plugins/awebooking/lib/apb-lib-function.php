<?php
/**
 * AWE Library Function
 *
 * @class       AWE_function
 * @version     1.0
 * @package     AweBooking/Classes/
 * @author      AweTeam
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Helper functions.
 */
class AWE_function {

	/**
	 * Create rooms for a room type.
	 *
	 * @param  int $room_type_id    Room type ID.
	 * @param  int $number_of_rooms Number of rooms to create.
	 * @param  int $begin           Begin create from.
	 *
	 * @return void
	 * @since 2.0
	 */
	public static function bulk_create_rooms( $room_type_id, $number_of_rooms, $begin = 1 ) {
		$room_type = get_post( $room_type_id );
		for ( $i = $begin; $i <= $number_of_rooms; $i++ ) {
			wp_insert_post( array(
				'post_type'		=> 'apb_room',
				'post_title'	=> $room_type->post_title . ' ' . $i,
				'post_name'		=> $room_type->post_name . '-' . $i,
				'post_parent'	=> $room_type_id,
				'post_status'	=> 'publish',
			) );
		}
	}

	/**
	 * Remove rooms for a room type.
	 *
	 * @param  int $room_type_id    Room type ID.
	 * @param  int $number_of_rooms Number of rooms to create.
	 * @param  int $begin           Begin remove from.
	 *
	 * @return void
	 * @since 2.0
	 */
	public static function bulk_remove_rooms( $room_type_id, $number_of_rooms, $begin = 1 ) {
		$room_type = get_post( $room_type_id );
		for ( $i = $begin; $i <= $number_of_rooms; $i++ ) {
			$room_slug = $room_type->post_name . '-' . $i;
			$temp_rooms = get_posts( array(
				'name' => $room_slug,
				'post_type'	=> 'apb_room',
				'post_status'	=> 'any',
			) );
			if ( ! empty( $temp_rooms[0] ) ) {
				wp_delete_post( $temp_rooms[0]->ID, true );
			}
		}
	}


	/**
	 * Get rooms of room type.
	 *
	 * @param  int $room_type_id Room type ID.
	 * @return array             Array of room object: (id => room).
	 * @since 2.0
	 */
	public static function get_rooms_of_room_type( $room_type_id ) {
		if ( function_exists( 'icl_object_id' ) ) {
			$lang = wpml_get_default_language();
			$room_type_id = icl_object_id( $room_type_id, 'post', true, $lang );
		}
		$room = get_children( array(
			'post_parent' => $room_type_id,
			'post_type'   => 'apb_room',
			'numberposts' => -1,
			'orderby'     => 'ID',
			'order'       => 'asc',
		) );

		return $room;
	}


	/**
	 * Get a room from room type to book.
	 *
	 * @param  int    $room_type_id Room type ID.
	 * @param  string $from         From date.
	 * @param  string $to           To date.
	 *
	 * @return int
	 * @since 2.0
	 */
	public static function get_room_available_from_room_type( $room_type_id, $from, $to ) {
		$room = AWE_function::get_rooms_of_room_type( $room_type_id );

		if ( empty( $room ) ) {
			return false;
		}

		$unavailable_room = AWE_function::get_room_unavailable( $from, $to, $room_type_id );
		$unavailable_room_id = array();
		foreach ( $unavailable_room as $v ) {
			$unavailable_room_id[] = $v->unit_id;
		}

		$cart = AWE_function::get_cart();

		foreach ( $room as $id => $object ) {
			if ( in_array( $id, $unavailable_room_id ) ) {
				continue;
			}

			if ( isset( $cart[ $id ] ) ) {
				continue;
			}

			return $id;
		}

		return false;
	}


	/**
	 * Get list nights.
	 *
	 * @param  string $start  Arrival date.
	 * @param  string $end    Departure date.
	 * @param  string $format Output date format.
	 * @param  string $step   Date step.
	 * @return array
	 */
	public static function range_night( $start, $end, $format = 'm/d/Y', $step = '+1 day' ) {
		$dates = array();

		$current = strtotime( $start );
		$last = strtotime( $end );

		while ( $current < $last ) {
			$dates[] = date( $format, $current );
			$current = strtotime( $step, $current );
		}
		return $dates;
	}


	/**
	 * Get room unavailable.
	 *
	 * @param  string $from         Arrival date. Converted date.
	 * @param  string $to           Departure date. Converted date.
	 * @param  int    $room_type_id Room type ID.
	 * @return array
	 * @since 2.0
	 */
	public static function get_room_unavailable( $from, $to, $room_type_id = null ) {
		global $wpdb;

		// $from = AWE_function::convert_date_to_mdY( $from );
		// $to = AWE_function::convert_date_to_mdY( $to );
		$dates = AWE_function::range_night( $from, $to );

		if ( empty( $dates ) ) {
			return array();
		}

		$room_ids = array();
		if ( ! empty( $room_type_id ) ) {
			$rooms = AWE_function::get_rooms_of_room_type( $room_type_id );
			foreach ( $rooms as $k => $v ) {
				$room_ids[] = $k;
			}
		}

		$where = array();
		foreach ( $dates as $date ) {
			$d = explode( '/', $date );
			$field_year = $d[2];
			$field_month = absint( $d[0] );
			$field_day = 'd' . absint( $d[1] );

			if ( AWE_function::prevent_book_pending() ) {
				$where[] = sprintf(
					'year = %d AND month = %d AND %s != 2',
					$field_year,
					$field_month,
					$field_day
				);
			} else {
				$where[] = sprintf(
					'year = %d AND month = %d AND %s < 2',
					$field_year,
					$field_month,
					$field_day
				);
			}
		}

		$where = implode( ' OR ', $where );

		if ( ! empty( $room_ids ) ) {
			$where .= ' AND unit_id IN ( ' . implode( ',', $room_ids ) . ' )';
		}

		$sql = "SELECT unit_id FROM {$wpdb->prefix}apb_availability WHERE {$where}";
		return $wpdb->get_results( $sql );
	}


	/**
	 * Check available.
	 *
	 * @param  string $from         Arrival date.
	 * @param  string $to           Departure date.
	 * @param  int    $adult        Number of adult.
	 * @param  int    $child        Number of child.
	 * @param  int    $room_type_id Room type ID.
	 * @return array                (room_type_id => remain_rooms).
	 * @since 2.0
	 */
	public static function check_available( $from, $to, $adult, $child, $room_type_id = null ) {
		$unavailable = AWE_function::get_room_unavailable( $from, $to, $room_type_id );

		$number_nights = AWE_function::get_number_of_nights( $from, $to );

		$unavailable_room_type = array();
		foreach ( $unavailable as $v ) {
			$room_type_id2 = wp_get_post_parent_id( $v->unit_id );
			if ( isset( $unavailable_room_type[ $room_type_id2 ] ) ) {
				$unavailable_room_type[ $room_type_id2 ] += 1;
			} else {
				$unavailable_room_type[ $room_type_id2 ] = 1;
			}
		}

		$cart = AWE_function::get_cart();
		foreach ( $cart as $k => $v ) {
			$room_type_id2 = wp_get_post_parent_id( $k );
			if ( isset( $unavailable_room_type[ $room_type_id2 ] ) ) {
				$unavailable_room_type[ $room_type_id2 ] += 1;
			} else {
				$unavailable_room_type[ $room_type_id2 ] = 1;
			}
		}

		$room_type = array();
		$args = array(
			'post_type'		=> 'apb_room_type',
			'posts_per_page'	=> -1,
			'meta_query'	=> array(
				'relation'		=> 'AND',
				array(
					'key'		=> 'min_night',
					'value'		=> $number_nights,
					'compare'	=> '<=',
					'type'		=> 'NUMERIC',
				),
				array(
					'key'		=> 'min_sleeps',
					'value'		=> $adult + $child,
					'compare'	=> '<=',
					'type'		=> 'NUMERIC',
				),
				array(
					'key'		=> 'max_sleeps',
					'value'		=> $adult + $child,
					'compare'	=> '>=',
					'type'		=> 'NUMERIC',
				),
				array(
					'relation'	=> 'OR',
					array(
						'key'		=> 'min_children',
						'compare'	=> 'NOT EXISTS',
					),
					array(
						'key'		=> 'min_children',
						'value'		=> $child,
						'compare'	=> '<=',
						'type'		=> 'NUMERIC',
					),
					array(
						'key'		=> 'min_children',
						'value'		=> '',
						'compare'	=> '=',
					),
				),
				array(
					'relation'	=> 'OR',
					array(
						'key'		=> 'max_children',
						'compare'	=> 'NOT EXISTS',
					),
					array(
						'key'		=> 'max_children',
						'value'		=> $child,
						'compare'	=> '>=',
						'type'		=> 'NUMERIC',
					),
					array(
						'key'		=> 'max_children',
						'value'		=> '',
						'compare'	=> '=',
					),
				),
			),
		);

		if ( ! is_null( $room_type_id ) ) {
			$args['p'] = $room_type_id;
		}

		$apb_query = new WP_Query( $args );

		if ( $apb_query->have_posts() ) {
			while ( $apb_query->have_posts() ) {
				$apb_query->the_post();

				$number_rooms = get_post_meta( get_the_ID(), 'number_of_rooms', true );
				if ( isset( $unavailable_room_type[ get_the_ID() ] ) && $unavailable_room_type[ get_the_ID() ] >= $number_rooms ) {
					continue;
				}

				$count = isset( $unavailable_room_type[ get_the_ID() ] ) ? $unavailable_room_type[ get_the_ID() ] : 0;
				$room_type[ get_the_ID() ] = $number_rooms - $count;
			}
			wp_reset_postdata();

		}
		return $room_type;
	}


	/**
	 * Update room available.
	 *
	 * @param  string $from    Arrival date.
	 * @param  string $to      Departure date.
	 * @param  int    $room_id Room ID.
	 * @param  int    $status  Available status
	 *     0: completed
	 *     1: not available
	 *     2: Available
	 *     3: Pending.
	 *
	 * @since 2.0
	 */
	public static function update_available( $from, $to, $room_id, $status ) {
		global $wpdb;
		// $from = AWE_function::convert_date_to_mdY( $from );
		// $to = AWE_function::convert_date_to_mdY( $to );
		$dates = AWE_function::range_night( $from, $to );

		if ( empty( $dates ) ) {
			return;
		}

		foreach ( $dates as $date ) {
			$d = explode( '/', $date );
			$field_year = $d[2];
			$field_month = $d[0];
			$field_day = 'd' . absint( $d[1] );

			$check_exists = $wpdb->get_var(
				$wpdb->prepare(
					"SELECT COUNT(*) FROM {$wpdb->prefix}apb_availability WHERE unit_id = %d AND year = %d AND month = %d",
					$room_id,
					$field_year,
					$field_month
				)
			);
			$check_exists = absint( $check_exists );

			if ( $check_exists ) {
				$wpdb->update(
					"{$wpdb->prefix}apb_availability",
					array(
						$field_day => $status,
					),
					array(
						'unit_id' => $room_id,
						'year'    => $field_year,
						'month'   => $field_month,
					),
					array( '%s' )
				);
			} else {
				$wpdb->insert(
					"{$wpdb->prefix}apb_availability",
					array(
						'unit_id'  => $room_id,
						'year'     => $field_year,
						'month'    => $field_month,
						$field_day => $status,
					),
					array( '%d', '%d', '%d', '%d' )
				);
			}
		}
	}


	/**
	 * Check if preventing booking in pending day.
	 *
	 * @return bool
	 * @since 2.0
	 */
	public static function prevent_book_pending() {
		return ( bool ) get_option( '_booking_pending' );
	}


	/**
	 * Check if use woo checkout.
	 *
	 * @return bool
	 * @since 2.0
	 */
	public static function use_woo_checkout() {
		return 1 == get_option( 'rooms_checkout_style' );
	}


	/**
	 * Get list days.
	 *
	 * @param  string $start  Arrival date.
	 * @param  string $end    Departure date.
	 * @param  string $format Output date format.
	 * @param  string $step   Date step.
	 * @return array
	 */
	public static function range_date( $start, $end, $format = 'm/d/Y', $step = '+1 day' ) {
		$dates = array();

		$current = strtotime( $start );
		$last = strtotime( $end );

		while ( $current <= $last ) {
			$dates[] = date( $format, $current );
			$current = strtotime( $step, $current );
		}
		return $dates;
	}


	/**
	 * Get number of nights.
	 *
	 * @param  string $from Arrival date with format m/d/Y or Y-m-d.
	 * @param  string $to   Departure date with format m/d/Y or Y-m-d.
	 *
	 * @return int          Number of nights.
	 * @since 2.0
	 */
	public static function get_number_of_nights( $from, $to ) {
		$from = strtotime( $from );
		$to = strtotime( $to );
		return ceil( ( $to - $from ) / DAY_IN_SECONDS );
	}

	/**
	 * Register order status.
	 * @return array Order status.
	 * @since 1.0
	 */
	public static function apb_get_order_statuses() {
		$order_statuses = array(
			'apb-pending'    => _x( 'Pending Payment', 'Order status', 'awebooking' ),
			'apb-completed'  => _x( 'Completed', 'Order status', 'awebooking' ),
			'apb-cancelled'  => _x( 'Cancelled', 'Order status', 'awebooking' ),
		);
		return apply_filters( 'apb_order_statuses', $order_statuses );
	}


	/**
	 * check_room_available - Show room available.
	 * @param date $from
	 * @param date $to
	 */
	public static function check_room_available( $from, $to, $room_id = '', $check_all_day = false, $status_filter = false, $status = null ) {
		global $wpdb;
		$to = strtotime( $to );
		$to = $to - DAY_IN_SECONDS;
		$to = date( 'm/d/Y', $to );
		// var_dump( $to);

		// Default year.
		$start_year     = date( 'Y', strtotime( $from ) );
		$end_year       = date( 'Y', strtotime( $to ) );

		// Default month.
		$start_month    = date( 'm', strtotime( $from ) );
		$end_month      = date( 'm', strtotime( $to ) );

		// Default day.
		$start_day      = date( 'd', strtotime( $from ) );
		$end_day        = date( 'd', strtotime( $to ) );

		/*if ( $end_day > 1 ) {
			$end_day--;
		} else {
			if ( 1 == $end_month ) {
				$end_month = 12;
				$end_year--;
				$end_day = 31;
			} else {
				$end_month--;
				for ( $i = 31; $i >= 28; $i++ ) {
					if ( checkdate( $end_month, $i, $end_year ) ) {
						$end_day = $i;
						break;
					}
				}
			}
		}*/

		// Get list month of start date and end date.
		$list_month = self::get_list_days( $start_year, $end_year, $start_month, $end_month );

		if ( count( $list_month ) > 2 ) {
			for ( $day = $start_day; $day <= 31; $day++ ) {
				$for_date = $start_year . '-' . $start_month . '-' . $day;
				if ( strtotime( $for_date ) >= strtotime( $from ) && strtotime( $for_date ) <= strtotime( $to ) ) {
					$_days[ $start_year . '-' . $start_month ][ 'd' . ( int ) $day ] = ( int ) $day;
				}
			}
			for ( $d_center = 1; $d_center <= count( $list_month ) - 2; $d_center++ ) {
				for ( $day = 1; $day <= 31; $day++ ) {
					$_days[ $list_month[ $d_center ]['y'] . '-' . $list_month[ $d_center ]['m'] ][ 'd' . $day ] = $day;
				}
			}

			for ( $day = 1; $day <= $end_day; $day++ ) {
				$for_date = $end_year . '-' . $end_month . '-' . $day;
				if ( strtotime( $for_date ) <= strtotime( $to ) ) {
					$_days[ $end_year . '-' . $end_month ][ 'd' . $day ] = $day;
				}
			}
		} else {
			for ( $day = $start_day; $day <= 31; $day++ ) {
				$for_date = $start_year . '-' . $start_month . '-' . $day;
				if ( strtotime( $for_date ) >= strtotime( $from ) && strtotime( $for_date ) <= strtotime( $to ) ) {
					$_days[ $start_year . '-' . $start_month ][ 'd' . ( int ) $day ] = ( int ) $day;
				}
			}
			if ( count( $list_month ) > 1 ) {
				for ( $day = 1; $day <= $end_day; $day++ ) {
					$for_date = $end_year . '-' . $end_month . '-' . $day;
					if ( strtotime( $for_date ) <= strtotime( $to ) ) {
						$_days[ $end_year . '-' . $end_month ][ 'd' . $day ] = $day;
					}
				}
			}
		}

		/*
		 * Query by list day
		 * Status 0 : completed
		 * Status 1 : not available
		 * Status 2 : Available
		 * Status 3 : Pending
		 */
		if ( isset( $_days) ) {
			foreach ( $_days as $key_item => $val_item ) {
				$esc_param = implode( ' AND ', self::available_get_array_param( $val_item ) );

				$m = ( int ) date( 'm', strtotime( $key_item ) );
				$y = date( 'Y', strtotime( $key_item ) );

				if ( '' == $room_id ) {
					$param_day = array( $y, $m );
					for ( $i = 0; $i<=count( $val_item)-1;$i++) {
						$param_day[] = 1;
					}
					$sql = $wpdb->prepare("SELECT unit_id FROM {$wpdb->prefix}apb_availability where year = %d and month = %d and $esc_param ", $param_day);

				 }else{
				   if ( $check_all_day == FALSE) {
						/*----------  Check by Status room by start date and End Date : Type ">" ----------*/
					   $param_day = array( $y, $m, $room_id);
					   $sql = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}apb_availability where year = %d and month = %d and unit_id = %d ", $param_day);
				   }
				   if ( $check_all_day == TRUE) {

						if ( $status_filter == TRUE) {
							/*----------  Check by Status room by start date and End Date : Type "=" ----------*/

							/**
							 *
							 * check row date exists
							 * insert row if not exists
							 */

							$date_exists = $wpdb->get_results( $wpdb->prepare("SELECT unit_id FROM {$wpdb->prefix}apb_availability where year = %d and month = %d and unit_id = %d ",array( $y, $m, $room_id) ) );
							if ( empty( $date_exists ) ) {
								AWE_function::update_available( $from, $to, $room_id , 2 );
								// AWE_Controller::update_day_available( $from, $to, $room_id , 2);
							}
							$esc_param = implode(" and ", self::available_get_array_param( $val_item,"=") );

							$param_day = array( $y, $m, $room_id);

							for( $i = 0; $i<=count( $val_item)-1;$i++) {
								$param_day[] = $status;
							}
							$sql = $wpdb->prepare("SELECT unit_id FROM {$wpdb->prefix}apb_availability where year = %d and month = %d and unit_id = %d and $esc_param ", $param_day);
						}

						if ( $status_filter == FALSE) {
							/*----------  Check by Status room by start date and End Date : Type ">" ----------*/

							$param_day = array( $y, $m, $room_id);
							for( $i = 0; $i<=count( $val_item)-1;$i++) {
								$param_day[] = 1;
							}
							$sql = $wpdb->prepare("SELECT unit_id FROM {$wpdb->prefix}apb_availability where year = %d and month = %d and unit_id = %d and $esc_param ", $param_day);
						}

				   }
			   }
			   return $wpdb->get_results( $sql);
			}
		}

	}

	public static function available_get_array_param( $param, $custom = false ) {
		foreach ( $param as $key => $val ) {
			if ( false == $custom ) {
				$array_param[ $key ] = "$key > %d";
			} else {
				$array_param[ $key ] = "$key " . $custom . " %d";
			}
		}
		return  $array_param;
	}

	public static function get_new_post_id() {
		global $wpdb;
		$data_id =  $wpdb->get_results( "SELECT * FROM {$wpdb->posts} order by ID DESC limit 1");
		return $data_id[0]->ID;
	}

	/**
	 * Get_room_option (Room package).
	 * @param int    $object_id Room type id.
	 * @param string $type      Deprecated from 2.0.
	 *
	 * @return array
	 */
	public static function get_room_option( $object_id, $type = 'apb_room_type' ) {
		global $wpdb;
		$sql = $wpdb->prepare(
			"SELECT * FROM {$wpdb->prefix}apb_booking_options WHERE entity_type = '%s' AND object_id = %d",
			$type,
			$object_id
		);
		return $wpdb->get_results( $sql );
	}

	public static function awe_get_link( $action, $paramt = '' ) {
		return admin_url( 'edit.php?post_type=apb_room_type&page=rooms.php&action=' . $action . $paramt );
	}

	/**
	 * Type change price of room.
	 *
	 * @return array
	 * @deprecated 2.0
	 */
	public static function operation() {
		_deprecated_function( __FUNCTION__, '2.0' );
		return array(
			'add'       => 'Add to price',
			'sub'       => 'Subtract from price',
			'replace'   => 'Replace price',
			'increase'  => 'Increase price by % amount',
			'decrease'  => 'Decrease price by % amount',
		);
	}

	/**
	 * Get template from plugin or theme.
	 *
	 * @param string $file  Template file name.
	 * @param array  $param Params to add to template.
	 *
	 * @return string
	 */
	public static function template_exsits( $file, $param = array() ) {
		extract( $param );
		if ( is_dir( get_template_directory() . '/apb-template/' ) ) {
			if ( file_exists( get_template_directory() . '/apb-template/' . $file . '.php' ) ) {
				$template_load = get_template_directory() . '/apb-template/' . $file . '.php';
			} else {
				$template_load = AWE_BK_PLUGIN_DIR . '/apb-template/' . $file . '.php';
			}
		} else {
			$template_load = AWE_BK_PLUGIN_DIR . '/apb-template/' . $file . '.php';
		}
		return $template_load;
	}

	/**
	 * Display select page.
	 * @param  string $name Field name.
	 * @param  int    $id   Page id.
	 * @return void
	 */
	public static function select_page( $name, $id = 0 ) {
		$args = array(
			'post_type' => 'page',
		);
		$posts_array = get_pages( $args );
		$data = '<select name="' . esc_attr( $name ) . '">';
		$data .= '<option>-- ' . esc_html__( 'Select a page', 'awebooking' ) . ' --</option>';
		foreach ( $posts_array as $item ) {
			$data .= ' <option ' . selected( $id, $item->ID, false ) . ' value="' . absint( $item->ID ). '">' . esc_html( $item->post_title ) . '</option>';
		}
		$data .= '</select>';

		echo $data;
	}

	/**
	 *
	 * Func get_list_days
	 * fucntion result data
	 *
	 */

	public static function get_list_days( $start_year, $end_year, $start_month, $end_month ) {
		$total_year = $end_year - $start_year;

		if ( $start_year != $end_year ) {
			$days = array();

			for ( $m = $start_month; $m <= 12; $m++ ) {
				$days[] = array(
					'm' => $m,
					'y' => $start_year,
				);
			}
			for ( $y = 1; $y <= $total_year - 1; $y++ ) {
				for ( $m_n = 1; $m_n <= 12; $m_n++ ) {
					$days[] = array(
						'm' => $m_n,
						'y' => $start_year + $y,
					);
				}
			}
			for ( $m2 = 1; $m2 <= $end_month; $m2++ ) {
				$days[] = array(
					'm' => $m2,
					'y' => $end_year,
				);
			}
		} else {
			$days = array();
			for ( $m = $start_month; $m <= $start_month + ( $end_month - $start_month ); $m++ ) {
				$days[] = array(
					'm' => $m,
					'y' => $end_year,
				);
			}
		}
		return $days;
	}

	/**
	 * List currencies.
	 * @return array
	 * @since 1.0
	 */
	public static function list_currencies() {
		return array(
			'AED' => 'United Arab Emirates Dirham',
			'ARS' => 'Argentine Peso',
			'AUD' => 'Australian Dollars',
			'BDT' => 'Bangladeshi Taka',
			'BRL' => 'Brazilian Real',
			'BGN' => 'Bulgarian Lev',
			'CAD' => 'Canadian Dollars',
			'CLP' => 'Chilean Peso',
			'CNY' => 'Chinese Yuan',
			'COP' => 'Colombian Peso',
			'CZK' => 'Czech Koruna',
			'DKK' => 'Danish Krone',
			'DOP' => 'Dominican Peso',
			'EUR' => 'Euros',
			'HKD' => 'Hong Kong Dollar',
			'HRK' => 'Croatia kuna',
			'HUF' => 'Hungarian Forint',
			'ISK' => 'Icelandic krona',
			'IDR' => 'Indonesia Rupiah',
			'INR' => 'Indian Rupee',
			'NPR' => 'Nepali Rupee',
			'ILS' => 'Israeli Shekel',
			'JPY' => 'Japanese Yen',
			'KIP' => 'Lao Kip',
			'KRW' => 'South Korean Won',
			'MYR' => 'Malaysian Ringgits',
			'MXN' => 'Mexican Peso',
			'NGN' => 'Nigerian Naira',
			'NOK' => 'Norwegian Krone',
			'NZD' => 'New Zealand Dollar',
			'PYG' => 'Paraguayan Guaraní',
			'PHP' => 'Philippine Pesos',
			'PLN' => 'Polish Zloty',
			'GBP' => 'Pounds Sterling',
			'RON' => 'Romanian Leu',
			'RUB' => 'Russian Ruble',
			'SGD' => 'Singapore Dollar',
			'ZAR' => 'South African rand',
			'SEK' => 'Swedish Krona',
			'CHF' => 'Swiss Franc',
			'TWD' => 'Taiwan New Dollars',
			'THB' => 'Thai Baht',
			'TRY' => 'Turkish Lira',
			'UAH' => 'Ukrainian Hryvnia',
			'USD' => 'US Dollars',
			'VND' => 'Vietnamese Dong',
			'EGP' => 'Egyptian Pound',
		);
	}


	/**
	 * Get currency symbol.
	 *
	 * @param  string $currency Currency code.
	 * @return string
	 */
	public static function get_currency( $currency = '' ) {
		switch ( $currency ) {
			case 'AED' :
				$currency_symbol = 'د.إ';
				break;
			case 'AUD' :
			case 'ARS' :
			case 'CAD' :
			case 'CLP' :
			case 'COP' :
			case 'HKD' :
			case 'MXN' :
			case 'NZD' :
			case 'SGD' :
			case 'USD' :
				$currency_symbol = '&#36;';
				break;
			case 'BDT':
				$currency_symbol = '&#2547;&nbsp;';
				break;
			case 'BGN' :
				$currency_symbol = '&#1083;&#1074;. ';
				break;
			case 'BRL' :
				$currency_symbol = '&#82;&#36;';
				break;
			case 'CHF' :
				$currency_symbol = '&#67;&#72;&#70;';
				break;
			case 'CNY' :
			case 'JPY' :
			case 'RMB' :
				$currency_symbol = '&yen;';
				break;
			case 'CZK' :
				$currency_symbol = '&#75;&#269;';
				break;
			case 'DKK' :
				$currency_symbol = 'DKK';
				break;
			case 'DOP' :
				$currency_symbol = 'RD&#36;';
				break;
			case 'EGP' :
				$currency_symbol = 'EGP';
				break;
			case 'EUR' :
				$currency_symbol = '&euro;';
				break;
			case 'GBP' :
				$currency_symbol = '&pound;';
				break;
			case 'HRK' :
				$currency_symbol = 'Kn';
				break;
			case 'HUF' :
				$currency_symbol = '&#70;&#116;';
				break;
			case 'IDR' :
				$currency_symbol = 'Rp';
				break;
			case 'ILS' :
				$currency_symbol = '&#8362;';
				break;
			case 'INR' :
				$currency_symbol = 'Rs. ';
				break;
			case 'ISK' :
				$currency_symbol = 'Kr. ';
				break;
			case 'KIP' :
				$currency_symbol = '&#8365;';
				break;
			case 'KRW' :
				$currency_symbol = '&#8361;';
				break;
			case 'MYR' :
				$currency_symbol = '&#82;&#77;';
				break;
			case 'NGN' :
				$currency_symbol = '&#8358;';
				break;
			case 'NOK' :
				$currency_symbol = '&#107;&#114;';
				break;
			case 'NPR' :
				$currency_symbol = 'Rs. ';
				break;
			case 'PHP' :
				$currency_symbol = '&#8369;';
				break;
			case 'PLN' :
				$currency_symbol = '&#122;&#322;';
				break;
			case 'PYG' :
				$currency_symbol = '&#8370;';
				break;
			case 'RON' :
				$currency_symbol = 'lei';
				break;
			case 'RUB' :
				$currency_symbol = '&#1088;&#1091;&#1073;. ';
				break;
			case 'SEK' :
				$currency_symbol = '&#107;&#114;';
				break;
			case 'THB' :
				$currency_symbol = '&#3647;';
				break;
			case 'TRY' :
				$currency_symbol = '&#8378;';
				break;
			case 'TWD' :
				$currency_symbol = '&#78;&#84;&#36;';
				break;
			case 'UAH' :
				$currency_symbol = '&#8372;';
				break;
			case 'VND' :
				$currency_symbol = '&#8363;';
				break;
			case 'ZAR' :
				$currency_symbol = '&#82;';
				break;
			default :
				$currency_symbol = '';
				break;
		}
		return $currency_symbol;
	}

	public static function apb_cart( $name = 'apb_cart' ) {
		_deprecated_function( __FUNCTION__, '2.0' );
		if ( isset( $_SESSION[ $name ] ) ) {
			return $_SESSION[ $name ];
		}

	}

	/**
	 * Get cart.
	 *
	 * @return array
	 * @since 2.0
	 */
	public static function get_cart() {
		$cart = isset( $_SESSION['apb_cart'] ) && is_array( $_SESSION['apb_cart'] ) ? $_SESSION['apb_cart'] : array();
		return $cart;
	}


	/**
	 * Update cart.
	 *
	 * @param  array $data New cart data.
	 * @since 2.0
	 */
	public static function update_cart( $data ) {
		$_SESSION['apb_cart'] = $data;
	}


	/**
	 * Delete cart.
	 *
	 * @since 2.0
	 */
	public static function delete_cart() {
		$_SESSION['apb_cart'] = array();

		if ( AWE_function::use_woo_checkout() ) {
			if ( class_exists( 'WooCommerce' ) ) {
				WC()->cart->empty_cart();
			}
		}
	}


	/**
	 * Add room to cart.
	 *
	 * @param int    $room_id      Room ID.
	 * @param string $from         Arrival date.
	 * @param string $to           Departure date.
	 * @param int    $adult        Adult.
	 * @param int    $child        Child.
	 * @param float  $price        Price.
	 * @param array  $sale_info    Sale data.
	 * @param array  $package_data Package data.
	 */
	public static function add_room_to_cart( $room_id, $from, $to, $adult, $child, $price, $sale_info = null, $package_data = null, $cart_index = -1 ) {
		$cart = AWE_function::get_cart();

		if ( isset( $cart[ $room_id ] ) ) {
			return false;
		}

		$room_data = array(
			'room_id'       => $room_id,
			'from'          => $from,
			'to'            => $to,
			'price'         => $price,
			'adult'         => $adult,
			'child'         => $child,
			'sale_info'     => $sale_info,
			'package_data'  => $package_data,
		);

		if ( $cart_index >= 0 ) {
			array_splice( $cart, $cart_index, 0, array( $room_id => $room_data ) );
			$new_cart = array();
			foreach ( $cart as $room ) {
				$new_cart[ $room['room_id'] ] = $room;
			}
			$cart = $new_cart;
		} else {
			$cart[ $room_id ] = $room_data;
		}
		AWE_function::update_cart( $cart );


		if ( AWE_function::use_woo_checkout() && class_exists( 'WooCommerce' ) ) {
			$cart_item_data = $room_data;
			WC()->cart->add_to_cart( $room_id, 1, null, null, $cart_item_data );
		}

		return true;
	}


	/**
	 * Remove room from cart.
	 *
	 * @param int $room_id Room ID.
	 * @since 2.0
	 */
	public static function remove_room_from_cart( $room_id ) {
		$cart = AWE_function::get_cart();

		if ( ! isset( $cart[ $room_id ] ) ) {
			return;
		}

		unset( $cart[ $room_id ] );

		AWE_function::update_cart( $cart );

		// var_dump($cart);

		if ( AWE_function::use_woo_checkout() && class_exists( 'WooCommerce' ) ) {
			$cart = WC()->cart->get_cart();
			foreach ( $cart as $cart_key => $cart_item ) {
				if ( $room_id == $cart_item['product_id'] ) {
					WC()->cart->remove_cart_item( $cart_key );
				}
			}
		}
	}

	public static function apb_get_option_to_selected( $args = array() ) {
		if ( isset( $args['name'] ) && isset( $args['count_num'] ) ) {
			$html = '<select ';
			if ( isset( $args['name'] ) && '' != $args['name'] ) {
				$html .= 'name="' . esc_attr( $args['name'] ) . '" ';
			}
			if ( isset( $args['data'] ) ) {
				foreach ( $args['data'] as $attr => $value ) {
					$html .= $attr . '="' . esc_attr( $value ) . '" ';
				}
			}
			$start = isset( $args['start_num'] ) ? $args['start_num'] : 1;

			$html .= '>';
			if ( isset( $args['default'] ) ) {
				$html .= '<option selected="selected" value="' . $args['default']['value'] . '">' . $args['default']['label'] . '</option>';
			}
			for ( $i = $start; $i <= $args['count_num']; $i++ ) {
				$html .= '<option ';
				if ( isset( $args['select'] ) && $args['select'] == $i ) {
					$html .= 'selected="selected"';
				}
				$html .= ' value="' . esc_attr( $i ) . '">' . $i . '</option>';
			}
			$html .= '</select>';
			echo $html;
		}
	}

	public static function apb_gen_input( $args = array() ) {
		$html ='<input ';
		if ( ! empty( $args ) ) {
			foreach ( $args as $attr => $value ) {
				$html .= ' ' . $attr . '="' . esc_attr( $value ) . '" ';
			}
		}
		$html .='>';
		echo $html;
	}

	public static function apb_icon_type_price( $type ) {
		_deprecated_function( __FUNCTION__, '2.0' );
		$list = array(
			'add' => 'Add',
			'sub' => 'Sub',
			'replace' => 'Replace',
			'increase' => 'Add %',
			'decrease' => 'Sub %',
		);
		return $list[ $type ];
	}

	/**
	 * Get price detail in a range days of room.
	 *
	 * @param  string  $from    From date.
	 * @param  string  $to      To date.
	 * @param  int     $room_id Room ID.
	 * @param  integer $night   Number of nights.
	 *
	 * @return array
	 * @since 1.0.0
	 */
	public static function get_pricing_of_days( $from, $to, $room_id, $night = 1 ) {
		$start_month = date( 'm', strtotime( $from ) );
		$end_month = date( 'm', strtotime( $to ) );

		$start_year = date( 'Y', strtotime( $from ) );
		$end_year = date( 'Y', strtotime( $to ) );

		$start_date = date( 'd', strtotime( $from ) );
		$end_date = date( 'd', strtotime( $to ) );

		$key_pricing = array();
		if ( $start_month == $end_month ) {

			$check_pricing = AWE_function::check_apb_pricing( $start_year, $start_month, $room_id );
			if ( ! empty( $check_pricing ) ) {

				for ( $day = ( int ) $start_date; $day <= ( int ) $end_date - $night; $day++ ) {
					$get_price = 'd' . $day;
					if ( checkdate( $start_month, $day, $start_year ) ) {
						$key_pricing[ $start_month ][ $day ] = $check_pricing[0]->$get_price;
					}
				}
			} else {
				for ( $day = ( int ) $start_date; $day <= ( int ) $end_date - $night; $day++ ) {
					$get_price = 'd' . $day;
					if ( checkdate( $start_month, $day, $start_year ) ) {
						$key_pricing[ $start_month ][ $day ] = get_post_meta( $room_id, 'base_price', true );
					}
				}
			}
		} else {
			$list_month = AWE_function::get_list_day( $start_year, $end_year, $start_month, $end_month );
			if ( count( $list_month ) > 2 ) {
				//====== List month > 2  ============//
				// Month day start

				$_monthStart = $list_month[0];
				$_monthEnd = $list_month[ count( $list_month ) - 1 ];
				unset( $list_month[0] );
				unset( $list_month[ count( $list_month ) ] );

				$check_pricing_start = AWE_function::check_apb_pricing( $_monthStart['y'], $_monthStart['m'], $room_id );
				if ( ! empty( $check_pricing_start ) ) {
					$total_day = date( 't', mktime( 0, 0, 0, $start_year, 1, $start_month ) );
					for ( $day = ( int ) $start_date; $day <= ( int ) $total_day; $day++ ) {
						$get_price = 'd' . $day;
						if ( checkdate( $_monthStart['m'], $day, $start_year ) ) {
							$key_pricing[ $_monthStart['m'] ][ $day ] = $check_pricing_start[0]->$get_price;
						}
					}
				} else {
					$total_day = date( 't', mktime( 0, 0, 0, $start_year, 1, $start_month ) );
					for ( $day = ( int ) $start_date; $day <= ( int ) $total_day; $day++ ) {
						$get_price = 'd' . $day;
						if ( checkdate( $_monthStart['m'], $day, $start_year ) ) {
							$key_pricing[ $_monthStart['m'] ][ $day ] = get_post_meta( $room_id, 'base_price', true );
						}
					}
				}
				foreach ( $list_month as $monthCenter ) {
					$check_pricing = AWE_function::check_apb_pricing( $monthCenter['y'], $monthCenter['m'], $room_id );

					if ( ! empty( $check_pricing ) ) {

						$total_day = date( 't', mktime( 0, 0, 0, $start_year, 1, $start_month ) );
						for ( $day = 0; $day <= ( int ) $total_day; $day++ ) {
							$get_price = 'd' . $day;
							if ( checkdate( $monthCenter['m'], $day, $start_year ) ) {
								$key_pricing[ $monthCenter['m'] ][ $day ] = $check_pricing[0]->$get_price;
							}
						}
					} else {

						$total_day = date( 't', mktime( 0, 0, 0, $start_year, 1, $start_month ) );
						for ( $day = 0; $day <= ( int ) $total_day; $day++ ) {
							if ( checkdate( $monthCenter['m'], $day, $start_year ) ) {
								$key_pricing[ $monthCenter['m'] ][ $day ] = get_post_meta( $room_id, 'base_price', true );
							}
						}
					}
				}

				 // Month day end.
				$check_pricing_end = AWE_function::check_apb_pricing( $_monthEnd['y'], $_monthEnd['m'], $room_id );
				if ( ! empty( $check_pricing_end ) ) {
					for ( $day = 1; $day <= ( int ) $end_date - $night; $day++ ) {
						$get_price = 'd' . $day;
						if ( checkdate( $_monthEnd['m'], $day, $start_year ) ) {
							$key_pricing[ $_monthEnd['m'] ][ $day ] = $check_pricing_end[0]->$get_price;
						}
					}
				} else {
					for ( $day = 1; $day <= ( int ) $end_date - $night; $day++ ) {
						$get_price = 'd' . $day;
						if ( checkdate( $_monthEnd['m'], $day, $start_year ) ) {
							$key_pricing[ $_monthEnd['m'] ][ $day ] = get_post_meta( $room_id, 'base_price', true );
						}
					}
				}
			} else {
				// Month day start.
				$check_pricing_start = AWE_function::check_apb_pricing( $list_month[0]['y'], $list_month[0]['m'], $room_id );
				if ( ! empty( $check_pricing_start ) ) {

					$total_day = date( 't', mktime( 0, 0, 0, $start_year, 1, $start_month ) );
					for ( $day = ( int ) $start_date; $day <= ( int ) $total_day; $day++ ) {
						$get_price = 'd' . $day;
						if ( checkdate( $list_month[0]['m'], $day, $start_year ) ) {
							$key_pricing[ $list_month[0]['m'] ][ $day ] = $check_pricing_start[0]->$get_price;
						}
					}
				} else {
					$total_day = date( 't', mktime( 0, 0, 0, $start_year, 1, $start_month ) );
					for ( $day = ( int ) $start_date; $day <= ( int ) $total_day; $day++ ) {
						$get_price = 'd' . $day;
						if ( checkdate( $list_month[0]['m'], $day, $start_year ) ) {
							$key_pricing[ $list_month[0]['m'] ][ $day ] = get_post_meta( $room_id, 'base_price', true );
						}
					}
				}
				// Month day start.
				$check_pricing_end = AWE_function::check_apb_pricing( $list_month[ count( $list_month ) - 1 ]['y'], $list_month[ count( $list_month ) - 1 ]['m'], $room_id );
				if ( ! empty( $check_pricing_end ) ) {
					for ( $day = 1; $day <= ( int ) $end_date - $night; $day++ ) {
						$get_price = 'd' . $day;
						if ( checkdate( $list_month[ count( $list_month ) - 1 ]['m'], $day, $start_year ) ) {
							$key_pricing[ $list_month[ count( $list_month ) - 1 ]['m'] ][ $day ] = $check_pricing_end[0]->$get_price;
						}
					}
				} else {
					for ( $day = 1; $day <= ( int ) $end_date - $night; $day++ ) {
						$get_price = 'd' . $day;
						if ( checkdate( $list_month[ count( $list_month ) - 1 ]['m'], $day, $start_year ) ) {
							$key_pricing[ $list_month[ count( $list_month ) - 1 ]['m'] ][ $day ] = get_post_meta( $room_id, 'base_price', true );
						}
					}
				}
			}
		}
		return $key_pricing;
	}


	static public function get_list_day( $start_year, $end_year, $start_month, $end_month ) {
		$total_year = $end_year - $start_year;

		if ( $start_year != $end_year ) {
			$days = array();

			for ( $m = $start_month; $m <= 12; $m++ ) {
				$days[] = array(
					'm' => $m,
					'y' => $start_year,
				);
			}
			for ( $y = 1; $y <= $total_year - 1; $y++ ) {
				for ( $m_n = 1; $m_n <= 12; $m_n++ ) {
					$days[] = array(
						'm' => $m_n,
						'y' => $start_year + $y,
					);
				}
			}
			for ( $m2 = 1; $m2 <= $end_month; $m2++) {
				$days[] = array(
					'm' => $m2,
					'y' => $end_year,
				);
			}
		} else {
			$days = array();
			for ( $m = $start_month; $m <= $start_month + ( $end_month - $start_month); $m++) {
				$days[] = array(
					'm' => $m,
					'y' => $end_year,
				);
			}
		}
		return $days;
	}


	public static function get_total_price( $list_pricing_of_days ) {
		$price = 0;
		if ( ! empty( $list_pricing_of_days ) ) {
			foreach ( $list_pricing_of_days as $key => $value ) {
				foreach ( $value as $pr ) {
					$price += $pr;
				}
			}
			return $price;
		}
	}


	public static function apb_setState( $name, $value ) {
		$_SESSION[$name] = $value;
	}


	public static function apb_getStateFlash( $name ) {
		if (isset( $_SESSION[$name] ) ) {
			return $_SESSION[$name];
		}
	}


	public static function Apb_get_permalink( $object = '', $args = array(), $action = '' ) {
		$current = get_option( 'permalink_structure' );
		if ( '' != $current ) {
			$uri = trailingslashit( get_permalink( $object ) );
			if ( 'action' == $action ) {
				$uri .= '?action=apb';
			}
			foreach ( $args as $key => $value ) {
				$uri .= $key . '/' . $value;
			}
			$url = str_replace( 'archives/', '', $uri );
			return str_replace( $uri, get_permalink( $object ), ( $url ) );
		} else {

			$uri = get_permalink( $object );

			foreach ( $args as $key => $value ) {
				if ( 'room_type_info' == $key ) {
					$uri .= '&rt_id=' . $value;
				}
			}
			return rtrim( $uri );
		}
	}

	public static function awe_help( $text = '' ) {
		echo '<img class="help_tip" data-tip="' . esc_attr( $text ) . '" src="' . esc_url( AWE_BK_BASE_URL_PLUGIN . '/assets/backend/images/help.png' ) . '" height="16" width="16" /></p>';
	}

	public static function get_type_of_sale( $key ) {
		$list = array( 'replace' => 'Replace price', 'sub' => 'Subtract from price', 'decrease' => 'Decrease price by %' );
		return $list[ $key ];
	}

	public static function get_symbol_of_sale( $key ) {
		$list = array( 'sub' => '-', 'decrease' => '%' );
		return $list[ $key ];
	}

	public static function apb_price( $price = 0 ) {
		$price = ( float ) $price;
		$currency_pos = get_option( 'woocommerce_currency_pos' ) ? esc_attr( get_option( 'woocommerce_currency_pos' ) ) : 'left';
		switch ( $currency_pos ) {
			case 'left':
				$price_format = '%2$s%1$s';
			break;

			case 'right':
				$price_format = '%1$s%2$s';
			break;

			case 'left_space':
				$price_format = '%2$s&nbsp;%1$s';
			break;

			case 'right_space':
				$price_format = '%1$s&nbsp;%2$s';
			break;
		}

		$args = array(
			'currency'  => get_option( 'woocommerce_currency' ) ? esc_attr( get_option( 'woocommerce_currency' ) ) : '$',
			'thousand_separator'    => get_option( 'woocommerce_price_thousand_sep' ) ? esc_attr( get_option( 'woocommerce_price_thousand_sep' ) ) : ',',
			'decimal_separator'     => get_option( 'woocommerce_price_decimal_sep' ) ? esc_attr( get_option( 'woocommerce_price_decimal_sep' ) ) : '. ',
			'decimals'              => get_option( 'woocommerce_price_num_decimals' ) ? absint( get_option( 'woocommerce_price_num_decimals' ) ) : 0,
			'price_format'          => $price_format,
		);

		extract( $args );
		$price = number_format( $price, $decimals, $decimal_separator, $thousand_separator );

		return sprintf( $price_format, esc_attr( $price ), esc_attr( AWE_function::get_currency( $currency ) ) );
	}

	public static function apb_get_extra_sale( $extra_sale, $total_day, $from = '' ) {
		$BeforeDay = count( AWE_function::range_date( date( 'Y-m-d' ), $from ) );
		$days = array();
		foreach ( $extra_sale as $item_extra_sale ) {
			if ( 'Month' == $item_extra_sale['type_duration'] ) {
				$days[ $item_extra_sale['total'] * 30 ] = array(
					'type_duration' => $item_extra_sale['type_duration'],
					'total_day'     => $item_extra_sale['total'] * 30,
					'amount'        => $item_extra_sale['amount'],
					'sale_type'     => $item_extra_sale['sale_type'],
				);
			}
			if ( 'Week' == $item_extra_sale['type_duration'] ) {
				$days[ $item_extra_sale['total'] * 8 ] = array(
					'type_duration' => $item_extra_sale['type_duration'],
					'total_day'     => $item_extra_sale['total'] * 8,
					'amount'        => $item_extra_sale['amount'],
					'sale_type'     => $item_extra_sale['sale_type'],
				);
			}
			if ( 'Day' == $item_extra_sale['type_duration'] ) {
				$days[ $item_extra_sale['total'] ] = array(
					'type_duration' => $item_extra_sale['type_duration'],
					'total_day'     => $item_extra_sale['total'],
					'amount'        => $item_extra_sale['amount'],
					'sale_type'     => $item_extra_sale['sale_type'],
				);
			}
			if ( 'Before-Day' == $item_extra_sale['type_duration'] ) {
				if ( $BeforeDay >= $item_extra_sale['total'] ) {
					$days[ 'before-' . $item_extra_sale['total'] ] = array(
						'type_duration' => $item_extra_sale['type_duration'],
						'total_day'     => $item_extra_sale['total'],
						'amount'        => $item_extra_sale['amount'],
						'sale_type'     => $item_extra_sale['sale_type'],
						'type_sale'     => 'before-day',
					);
				}
			}
		}
		$_days = array();
		foreach ( $days as $day ) {
			if ( 'Before-Day' == $day['type_duration'] ) {
				if ( $BeforeDay >= $day['total_day'] ) {
					$_days[] = 'before-' . $day['total_day'];
				}
			} else {
				if ( $total_day - 1 >= $day['total_day'] ) {
					$_days[] = $day['total_day'];
				}
			}
		}
		if ( ! empty( $_days ) ) {
			sort( $_days );
			return( $days[ $_days[ count( $_days ) - 1 ] ] );
		} else {
			return '';
		}
	}


	public static function get_room_type() {
		_deprecated_function( __FUNCTION__, '2.0' );
	}

	public static function apb_datepicker_lang( $key = '' ) {
		$i18 =  array(
			// 'datepicker-af'	=> 'datepicker-af',
			// 'datepicker-ar-DZ'	=> 'datepicker-ar-DZ',
			// 'datepicker-ar'	=> 'datepicker-ar',
			// 'datepicker-az'	=> 'datepicker-az',
			// 'datepicker-be'	=> 'datepicker-be',
			// 'datepicker-bg'	=> 'Bulgarian',
			// 'datepicker-ca'	=> 'Catalan',
			// 'datepicker-cs'	=> 'Czech',
			// 'datepicker-cy-GB'	=> 'datepicker-cy-GB',
			// 'datepicker-da'	=> 'datepicker-da',
			'datepicker-zh-CN'	=> 'Chinese (China)',
			// 'datepicker-zh-HK'	=> 'Chinese (Hong Kong) ',
			'datepicker-zh-TW'	=> 'Chinese (Taiwan)',
			'datepicker-nl-BE'	=> 'Dutch (Belgium)',
			'datepicker-nl'		=> 'Dutch (The Netherlands)',
			'datepicker-en-AU'	=> 'English (Australia)',
			'datepicker-en-GB'	=> 'English (United Kingdom)',
			'datepicker-en-NZ'	=> 'English (New Zealand)',
			'datepicker-fr-CA'	=> 'French (Canada)',
			'datepicker-fr-CH'	=> 'French (Switzerland)',
			'datepicker-fr'		=> 'French (France)',
			'datepicker-de'		=> 'German',
			'datepicker-ja'		=> 'Japanese',
			'datepicker-it'		=> 'Italian',
			'datepicker-it-CH'	=> 'Italian (Switzerland)',
			'datepicker-pl'		=> 'Polish (Poland)',
			'datepicker-pt-BR'	=> 'Portuguese (Brazil)',
			'datepicker-pt'		=> 'Portuguese (Portugal)',
			'datepicker-ru'		=> 'Russian',
			'datepicker-es'		=> 'Spanish (Spain)',
			'datepicker-ta'		=> 'Tamil (India)',
			'datepicker-tr'		=> 'Turkish',
			// 'datepicker-el'	=> 'datepicker-el',
			// 'datepicker-eo'	=> 'datepicker-eo',
			// 'datepicker-et'	=> 'Estonian',
			// 'datepicker-eu'	=> 'datepicker-eu',
			// 'datepicker-fa'	=> 'datepicker-fa',
			// 'datepicker-fi'	=> 'datepicker-fi',
			// 'datepicker-fo'	=> 'datepicker-fo',
			// 'datepicker-gl'	=> 'datepicker-gl',
			// 'datepicker-he'	=> 'datepicker-he',
			// 'datepicker-hi'	=> 'datepicker-hi',
			// 'datepicker-hr'	=> 'datepicker-hr',
			// 'datepicker-hu'	=> 'datepicker-hu',
			// 'datepicker-hy'	=> 'datepicker-hy',
			// 'datepicker-id'	=> 'Indonesian',
			// 'datepicker-is'	=> 'datepicker-is',
			// 'datepicker-ka'	=> 'datepicker-ka',
			// 'datepicker-kk'	=> 'datepicker-kk',
			// 'datepicker-km'	=> 'datepicker-km',
			// 'datepicker-ko'	=> 'datepicker-ko',
			// 'datepicker-ky'	=> 'datepicker-ky',
			// 'datepicker-lb'	=> 'datepicker-lb',
			// 'datepicker-lt'	=> 'datepicker-lt',
			// 'datepicker-lv'	=> 'datepicker-lv',
			// 'datepicker-mk'	=> 'datepicker-mk',
			// 'datepicker-ml'	=> 'datepicker-ml',
			// 'datepicker-ms'	=> 'datepicker-ms',
			// 'datepicker-nb'	=> 'datepicker-nb',
			// 'datepicker-nn'	=> 'datepicker-nn',
			// 'datepicker-no'	=> 'datepicker-no',
			// 'datepicker-rm'	=> 'datepicker-rm',
			// 'datepicker-ro'	=> 'datepicker-ro',
			// 'datepicker-sk'	=> 'datepicker-sk',
			// 'datepicker-sl'	=> 'datepicker-sl',
			// 'datepicker-sq'	=> 'datepicker-sq',
			// 'datepicker-sr-SR'	=> 'datepicker-sr-SR',
			// 'datepicker-sr'	=> 'datepicker-sr',
			// 'datepicker-sv'	=> 'datepicker-sv',
			// 'datepicker-th'	=> 'datepicker-th',
			// 'datepicker-tj'	=> 'datepicker-tj',
			// 'datepicker-uk'	=> 'datepicker-uk',
			// 'datepicker-vi'	=> 'datepicker-vi',
		);
		if ( '' != $key ) {
			return $i18[ $key ];
		} else {
			return $i18;
		}
	}

	public static function Apb_Js_FormatDate( $key ) {
		_deprecated_function( __FUNCTION__, '2.0' );
		$list_format = array(
			'dd/mm/yy'  => 'd/m/Y',
			'dd.mm.yy'  => 'd.m.Y',
			'dd-mm-yy'  => 'd-m-Y',
			'yy-mm-dd'  => 'Y-m-d',
			'yy/mm/dd'  => 'Y/m/d',
			'd.m.yy'    => 'd.m.Y',
			'yy.mm.dd. ' => 'Y.m.d',
			'yy. m. d. ' => 'Y.m.d',
			'mm/dd/yy'  => 'm/d/Y',
		);
		return $list_format[ $key ];
	}

	/**
	 * Detect if we should use a light or dark colour on a background colour
	 *
	 * @param mixed $color
	 * @param string $dark (default: '#000000' )
	 * @param string $light (default: '#FFFFFF' )
	 * @return string
	 */
	public static function apb_light_or_dark( $color, $dark = '#000000', $light = '#FFFFFF' ) {

		$hex = str_replace( '#', '', $color );

		$c_r = hexdec( substr( $hex, 0, 2 ) );
		$c_g = hexdec( substr( $hex, 2, 2 ) );
		$c_b = hexdec( substr( $hex, 4, 2 ) );

		$brightness = ( ( $c_r * 299 ) + ( $c_g * 587 ) + ( $c_b * 114 ) ) / 1000;

		return $brightness > 155 ? $dark : $light;
	}

	/**
	 * Hex darker/lighter/contrast functions for colours
	 *
	 * @param mixed $color
	 * @return string
	 */
	public static function apb_rgb_from_hex( $color ) {
		$color = str_replace( '#', '', $color );
		// Convert shorthand colors to full format, e.g. "FFF" -> "FFFFFF"
		$color = preg_replace( '~^(.)(.)(.)$~', '$1$1$2$2$3$3', $color );

		$rgb      = array();
		$rgb['R'] = hexdec( $color{0}.$color{1} );
		$rgb['G'] = hexdec( $color{2}.$color{3} );
		$rgb['B'] = hexdec( $color{4}.$color{5} );

		return $rgb;
	}

	public static function apb_hex_darker( $color, $factor = 30 ) {
		$base  = AWE_function::apb_rgb_from_hex( $color );
		$color = '#';

		foreach ( $base as $k => $v ) {
			$amount      = $v / 100;
			$amount      = round( $amount * $factor );
			$new_decimal = $v - $amount;

			$new_hex_component = dechex( $new_decimal );
			if ( strlen( $new_hex_component ) < 2 ) {
				$new_hex_component = '0' . $new_hex_component;
			}
			$color .= $new_hex_component;
		}

		return $color;
	}

	/**
	 * Hex darker/lighter/contrast functions for colours
	 *
	 * @param mixed $color
	 * @param int $factor (default: 30)
	 * @return string
	 */
	public static function apb_hex_lighter( $color, $factor = 30 ) {
		$base  = AWE_function::apb_rgb_from_hex( $color );
		$color = '#';

		foreach ( $base as $k => $v ) {
			$amount      = 255 - $v;
			$amount      = $amount / 100;
			$amount      = round( $amount * $factor );
			$new_decimal = $v + $amount;

			$new_hex_component = dechex( $new_decimal );
			if ( strlen( $new_hex_component ) < 2 ) {
				$new_hex_component = "0" . $new_hex_component;
			}
			$color .= $new_hex_component;
		}

		return $color;
	}

	/**
	 * Move template action.
	 *
	 * @param string $template_type
	 */
	public static function move_template_action( $template_file , $path ) {
		if ( ! is_dir( get_stylesheet_directory() . '/apb-template/' ) ) {
			mkdir( get_stylesheet_directory() . '/apb-template/' );
		}
		if ( ! is_dir( get_stylesheet_directory() . $path ) ) {
			mkdir( get_stylesheet_directory() . $path );
		}
		if ( ! file_exists( get_stylesheet_directory() . $path . $template_file . '.php' ) ) {
			$f = fopen( get_stylesheet_directory() . $path . $template_file . '.php', 'w+' );
			copy( AWE_BK_PLUGIN_DIR . $path . $template_file . '.php', get_stylesheet_directory() . $path . $template_file . '.php' );
		}
	}

	/**
	 * Delete template action.
	 *
	 * @param string $template_type
	 */
	public static function delete_template_action( $template_file, $path ) {
		if ( is_dir( get_stylesheet_directory() . $path ) ) {
			unlink( get_stylesheet_directory() . $path . $template_file . '.php' );
		}
	}

	public static function apb_print_js( $code ) {

		echo "<!-- Awebooking JavaScript -->\n<script type=\"text/javascript\">\njQuery(function( $) {";

		// Sanitize
		$wc_queued_js = wp_check_invalid_utf8( $code );
		$wc_queued_js = preg_replace( '/&#(x)?0*(?(1)27|39);?/i', "'", $code );
		$wc_queued_js = str_replace( "\r", '', $code );

		echo $wc_queued_js . "});\n</script>\n";
	}


	/**
	 * Check if room in cart.
	 *
	 * @param  int $room_id Room ID.
	 * @return bool         True if room in cart.
	 * @since 1.9
	 */
	public static function check_room_in_cart( $room_id ) {
		$cart = AWE_function::apb_cart( 'apb_cart' );
		if ( ! empty( $cart ) && is_array( $cart ) ) {
			foreach ( $cart as $key_item => $item_cart ) {
				if ( $item_cart['room_id'] == $room_id ) {
					return true;
				}
			}
		}
		return false;
	}


	/**
	 * Get_day_for_month_start : Get all day for one month.
	 * @param $year int
	 * @param $month int
	 * @param $start_date int
	 * @param $end_date int
	 * @param $room_id int
	 * @param $price int
	 * @param $action status get result
	 * @param $control String
	 */
	static public function get_day_for_month_start( $year, $month, $start_date, $end_date, $room_id, $price, $operation, $action, $control = '' ) {
		if ( 'start' == $action ) {

			for ( $day = 1; $day <= 31; $day++ ) {
				$for_date = $year . '-' . $month . '-' . $day;
				if ( strtotime( $for_date ) >= strtotime( $start_date ) && strtotime( $for_date ) <= strtotime( $end_date ) ) {
					$days_price[ 'd' . $day ] = AWE_function::get_price_operation( $operation, $room_id, $month, $year, 'd' . $day, $price);
				} else {
					$days_price[ 'd' . $day ] = get_post_meta( wp_kses( $room_id, '' ), 'base_price', true );
				}
			}
		} elseif ( 'default' == $action ) {
			for ( $day = 1; $day <= 31; $day++ ) {
				$for_date = $year . '-' . $month . '-' . $day;
				$days_price[ 'd' . $day ] = AWE_function::get_price_operation( $operation, $room_id, $month, $year, 'd' . $day, $price );
			}
		} elseif ( 'end' == $action ) {
			for ( $day = 1; $day <= 31; $day++ ) {
				$for_date = $year . '-' . $month . '-' . $day;
				if ( strtotime( $for_date ) <= strtotime( $end_date ) ) {
					$days_price[ 'd' . $day ] = AWE_function::get_price_operation( $operation, $room_id, $month, $year, 'd' . $day, $price );
				} else {
					$days_price[ 'd' . $day ] = get_post_meta( wp_kses( $room_id, '' ), 'base_price', true );
				}
			}
		}
		return $days_price;
	}


	/**
	 * get_price_operation : Get price by operation.
	 * @param $operation string
	 * @param $room_id int
	 * @param $month int
	 * @param $year int
	 * @param $day int
	 * @param $input_price int
	 */
	static public function get_price_operation( $operation, $room_id, $month, $year, $day, $input_price ) {
		$room = AWE_function::check_apb_pricing( $year, $month, $room_id );

		if ( ! empty( $room ) && intval( $room[0]->$day ) ) {
			$price_default = $room[0]->$day;
		} else {
			$price_default = get_post_meta( $room_id, 'base_price', true );
		}

		switch ( $operation ) {
			case 'add':
				return $input_price + $price_default;
				break;
			case 'sub':
				return $price_default - $input_price;
				break;
			case 'replace':
				return $input_price;
				break;
			case 'increase':
				return $price_default + $input_price / 100 * $price_default;
				break;
			case 'decrease':
				return $price_default - $input_price / 100 * $price_default;
				break;
			default :
				return $input_price;
		}
	}


	/**
	 * check_rooms_avb : Check room availability manage exists
	 * @param $year int
	 * @param $month int
	 * @param $room_id int
	 */
	public static function check_rooms_avb( $year, $month, $room_id ) {
		global $wpdb;
		$sql = $wpdb->prepare(
			"SELECT * FROM {$wpdb->prefix}apb_availability where unit_id = %d and year = %d and month = %d",
			absint( $room_id ),
			absint( $year ),
			absint( $month )
		);
		return $wpdb->get_results( $sql );
	}


	/**
	 * check_apb_pricing : Check room pricing manage exists
	 * @param $year int
	 * @param $month int
	 * @param $room_id int
	 */
	static public function check_apb_pricing( $year, $month, $room_id ) {
		global $wpdb;
		return $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}apb_pricing where unit_id = $room_id and year = '{$year}' and month = '{$month}'" );
	}


	/**
	 * Get minimum night of room.
	 *
	 * @param  int $room_id Room ID.
	 * @return int          Minimum of night.
	 * @since  1.9
	 */
	public static function get_room_min_night( $room_id ) {
		return absint( get_post_meta( $room_id, 'mid_night', true ) );
	}


	/**
	 * Get total of day price.
	 *
	 * @param  array $price_days Day price array.
	 *
	 * @return float
	 * @since 1.10
	 */
	public static function get_total_day_price( $price_days ) {
		$result = 0;
		foreach ( $price_days as $month => $list_day ) {
			foreach ( $list_day as $day => $price ) {
				$result += $price;
			}
		}
		return ( float ) $result;
	}


	/**
	 * Get number of day price.
	 *
	 * @param  array $price_days Day price array.
	 *
	 * @return int
	 * @since 1.10
	 */
	public static function get_number_day_price( $price_days ) {
		$result = 0;
		foreach ( $price_days as $month => $list_day ) {
			foreach ( $list_day as $day => $price ) {
				$result++;
			}
		}
		return absint( $result );
	}


	/**
	 * Change language code from WordPress to FullCalendar.
	 *
	 * @param  string $code Datepicker language code.
	 *
	 * @return string       FullCalendar language code.
	 * @since 1.11
	 */
	public static function lang_code_datepicker_to_fullcalendar( $code ) {
		if ( 'default' == $code || '' == $code ) {
			$fc_code = false;
		} else {
			$code = strtolower( substr( $code, 11 ) );
			switch ( $code ) {
				case 'ar-dz':
					$fc_code = 'ar-tn';
				break;

				case 'cy-gb':
					$fc_code = 'en-gb';
				break;

				case 'fr-ch':
					$fc_code = 'fr';
				break;

				case 'it-ch':
					$fc_code = 'it';
				break;

				case 'nl-be':
					$fc_code = 'nl';
				break;

				default:
					$fc_code = $code;
			}
		}

		return $fc_code;
	}


	/**
	 * Change language code from WPML to Datepicker.
	 *
	 * @param  string $code WPML language code.
	 *
	 * @return string       Datepicker language code.
	 * @since 1.11
	 */
	public static function lang_code_wpml_to_datepicker( $code ) {
		switch ( $code ) {
			case 'all':
				$datepicker_code = 'default';
			break;

			case 'en':
				$datepicker_code = 'datepicker-en-GB';
			break;

			case 'pt-pt':
				$datepicker_code = 'datepicker-pt';
			break;

			case 'zh-hans':
				$datepicker_code = 'datepicker-zh-CN';
			break;

			case 'zh-hant':
				$datepicker_code = 'datepicker-zh-TW';
			break;

			default:
				$datepicker_code = 'datepicker-' . $code;
		}

		return $datepicker_code;
	}


	/**
	 * Get current date format, according datepicker language option.
	 *
	 * @return string Date format.
	 * @since 1.11
	 */
	public static function get_current_date_format() {
		$format = 'm/d/Y';
		$datepicker_lang = AWE_function::get_datepicker_lang();

		switch ( $datepicker_lang ) {
			case 'datepicker-zh-CN':
			case 'datepicker-nl-BE':
			case 'datepicker-en-AU':
			case 'datepicker-en-GB':
			case 'datepicker-en-NZ':
			case 'datepicker-fr':
			case 'datepicker-it':
			case 'datepicker-pt-BR':
			case 'datepicker-pt':
			case 'datepicker-es':
			case 'datepicker-ta':
				$format = 'd/m/Y';
			break;

			case 'datepicker-nl':
				$format = 'd-m-Y';
			break;

			case 'datepicker-fr-CH':
			case 'datepicker-de':
			case 'datepicker-it-CH':
			case 'datepicker-pl':
			case 'datepicker-ru':
			case 'datepicker-tr':
				$format = 'd.m.Y';
			break;

			case 'datepicker-zh-TW':
			case 'datepicker-ja':
				$format = 'Y/m/d';
			break;

			case 'datepicker-fr-CA':
				$format = 'Y-m-d';
			break;

			default:
				$format = 'm/d/Y';
		}

		return $format;
	}


	/**
	 * Convert date string from other format to m/d/Y.
	 *
	 * @param  string $date_string Date string.
	 * @param  string $date_format Optional. Date format. If empty, use current date format.
	 *
	 * @return string
	 * @since 1.11
	 */
	public static function convert_date_to_mdY( $date_string, $date_format = null ) {
		if ( ! $date_format ) {
			$date_format = AWE_function::get_current_date_format();
		}
		if ( 'm/d/Y' == $date_format ) {
			return $date_string;
		}
		$d = DateTime::createFromFormat( $date_format, $date_string );
		return $d->format( 'm/d/Y' );
	}


	/**
	 * Replace some value in email.
	 *
	 * @param  string $string   String need replace.
	 * @param  object $order_id Order ID.
	 * @return string
	 * @since 1.11
	 */
	public static function email_str_replace( $string, $order_id ) {
		$new_string = $string;

		$key = array(
			'{site_title}',
			'{order_number}',
			'{order_date}',
		);

		$value = array(
			get_bloginfo( 'name' ),
			$order_id,
			get_the_time( get_option( 'date_format' ), $order_id ),
		);

		$new_string = str_replace( $key, $value, $string );

		return $new_string;
	}


	/**
	 * Get check available page url.
	 *
	 * @return string
	 * @since 2.0
	 */
	public static function get_check_available_page() {
		$page_id = get_option( 'check_avb' );
		if ( function_exists( 'icl_object_id' ) ) {
			$page_id = icl_object_id( $page_id, 'page', true );
		}
		return esc_url( get_permalink( $page_id ) );
	}


	/**
	 * Get check available page id.
	 *
	 * @return string
	 * @since 2.0
	 */
	public static function get_check_available_page_id() {
		$page_id = get_option( 'check_avb' );
		if ( function_exists( 'icl_object_id' ) ) {
			$page_id = icl_object_id( $page_id, 'page', true );
		}
		return $page_id;
	}


	/**
	 * Get list room page id.
	 *
	 * @return string
	 * @since 2.0
	 */
	public static function get_list_room_page_id() {
		$page_id = get_option( 'list_room' );
		if ( function_exists( 'icl_object_id' ) ) {
			$page_id = icl_object_id( $page_id, 'page', true );
		}
		return $page_id;
	}


	/**
	 * Get checkout page id.
	 *
	 * @return string
	 * @since 2.0
	 */
	public static function get_checkout_page_id() {
		$page_id = get_option( 'apb_checkout' );
		if ( function_exists( 'icl_object_id' ) ) {
			$page_id = icl_object_id( $page_id, 'page', true );
		}
		return $page_id;
	}


	/**
	 * Check current page is check available page.
	 *
	 * @return bool
	 * @since 2.0
	 */
	public static function is_check_available_page() {
		$page_id = get_option( 'check_avb' );
		if ( function_exists( 'icl_object_id' ) ) {
			$page_id = icl_object_id( $page_id, 'page', true );
		}
		return get_the_ID() == $page_id;
	}


	/**
	 * Get datepicker language.
	 *
	 * @return string
	 * @since 2.0
	 */
	public static function get_datepicker_lang() {
		$lang = get_option( 'datepicker_lang' );
		if ( function_exists( 'icl_translate' ) ) {
			$lang = AWE_function::lang_code_wpml_to_datepicker( ICL_LANGUAGE_CODE );
			// $format = icl_translate( 'Formats', $format, $format );
		}
		return $lang;
	}


	/**
	 * Get max nights.
	 *
	 * @return int
	 * @since 2.0
	 */
	public static function get_max_night() {
		return absint( get_option( 'max_night' ) );
	}


	/**
	 * Get max adult.
	 *
	 * @return int
	 * @since 2.0
	 */
	public static function get_max_adult() {
		return absint( get_option( 'max_adult' ) );
	}


	/**
	 * Get max child.
	 *
	 * @return int
	 * @since 2.0
	 */
	public static function get_max_child() {
		return absint( get_option( 'max_child' ) );
	}


	/**
	 * Remove old version data.
	 * @return void
	 * @since 2.1
	 */
	public static function reset_old_data() {
		global $wpdb;
		$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}rooms_availability" );
		$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}rooms_booking_unit_options" );
		$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}rooms_pricing" );
		$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}rooms_unit_type" );
		$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}apb_availability" );
		$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}apb_booking_options" );
		$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}apb_pricing" );
		$wpdb->query( "DELETE FROM {$wpdb->prefix}posts WHERE post_type IN ('apb_room_type', 'apb_room', 'shop_order', 'apb_order')" );
	}


	/**
	 * Check if show single calendar.
	 * @return bool
	 */
	public static function show_single_calendar() {
		return absint( get_option( 'apb_show_single_calendar' ) );
	}
}


/**
 * Fire after switched WPML language.
 *
 * @return void
 * @since 1.11
 */
function apb_wpml_switched_lang() {
	if ( ! isset( $_GET['lang'] ) ) {
		return;
	}

	if ( ! defined( ICL_LANGUAGE_CODE ) ) {
		return;
	}
	// Update datepicker language option.
	update_option( 'datepicker_lang', AWE_function::lang_code_wpml_to_datepicker( ICL_LANGUAGE_CODE ) );

	/*$check_page = get_option( 'check_avb' );
	update_option( 'check_avb', icl_object_id( $check_page, 'page', true ) );

	$list_room = get_option( 'list_room' );
	update_option( 'list_room', icl_object_id( $list_room, 'page', true ) );

	$apb_checkout = get_option( 'apb_checkout' );
	update_option( 'apb_checkout', icl_object_id( $apb_checkout, 'page', true ) );*/
}
// add_action( 'init', 'apb_wpml_switched_lang' );


/**
 * Filter option date_format to translate date with WPML.
 *
 * @param  string $format Date format.
 *
 * @return string
 * @since 1.11
 */
function apb_translate_date_format( $format ) {
	if ( function_exists( 'icl_translate' ) ) {
		$format = AWE_function::lang_code_wpml_to_datepicker( ICL_LANGUAGE_CODE );
		// $format = icl_translate( 'Formats', $format, $format );
	}
	return $format;
}
// add_filter( 'option_date_format', 'apb_translate_date_format' );


/**
 * Send mail to user when order is cancelled.
 * @param  int $order_id Order ID.
 * @return void
 * @since 1.11
 */
function apb_send_mail_when_cancelled( $order_id ) {
	$config_mail = get_option( 'apb_mail_cancel' );
	if ( empty( $config_mail['notice_status'] ) ) {
		return;
	}
	$customer = get_post_meta( $order_id, 'info_custom_order', true );
	$email_address = $customer['apb-email'];
	$subject = ! empty( $config_mail['subject'] ) ? $config_mail['subject'] : '[{site_title}] Cancelled order ({order_number})';
	$subject = AWE_function::email_str_replace( $subject, $order_id );

	ob_start();
	do_action( 'apb_mail_cancelled_order', $order_id );
	$message = ob_get_clean();

	$email = new APB_Email();

	if ( isset( $config_mail['notice_status'] ) && 1 == $config_mail['notice_status'] ) {
		$message_user = $email->apb_style_inline( $email->apb_wrap_message( $config_mail['header'], $message ) );
		$email->apb_sendMail( $email_address, $subject, $message_user );
	}
	$email->destroy();
}
add_action( 'apb-cancelled_shop_order', 'apb_send_mail_when_cancelled' );


/**
 * Update available and order status when order is cancelled.
 *
 * @param  int $order_id Order ID.
 * @return void
 * @since 1.11
 */
function apb_update_available_when_cancelled( $order_id ) {
	$from = get_post_meta( $order_id, 'from', true );
	$to = get_post_meta( $order_id, 'to', true );
	$order_data = get_post_meta( $order_id, 'apb_data_order', true );

	if ( ! is_array( $order_data ) ) {
		return;
	}

	foreach ( $order_data as $v ) {
		Boxes_info_booking::update_status( $v['id'], 'apb-cancelled' );
		AWE_function::update_available( $from, $to, $v['order_room_id'], 2 );
		// AWE_Controller::update_day_available( $from, $to, $v['order_room_id'], 2 );
	}
}
add_action( 'apb-cancelled_shop_order', 'apb_update_available_when_cancelled' );
add_action( 'trash_shop_order', 'apb_update_available_when_cancelled' );


/**
 * Update available and order status when order is pending.
 *
 * @param  int $order_id Order ID.
 * @return void
 * @since 2.0
 */
function apb_update_available_when_pending( $order_id ) {
	$from = get_post_meta( $order_id, 'from', true );
	$to = get_post_meta( $order_id, 'to', true );
	$order_data = get_post_meta( $order_id, 'apb_data_order', true );

	if ( ! is_array( $order_data ) ) {
		return;
	}

	foreach ( $order_data as $v ) {
		Boxes_info_booking::update_status( $v['id'], 'apb-pending' );
		AWE_function::update_available( $from, $to, $v['order_room_id'], 3 );
	}
}
add_action( 'apb-pending_shop_order', 'apb_update_available_when_pending' );


/**
 * Update available and order status when order is completed.
 *
 * @param  int $order_id Order ID.
 * @return void
 * @since 2.0
 */
function apb_update_available_when_completed( $order_id ) {
	$from = get_post_meta( $order_id, 'from', true );
	$to = get_post_meta( $order_id, 'to', true );
	$order_data = get_post_meta( $order_id, 'apb_data_order', true );

	if ( ! is_array( $order_data ) ) {
		return;
	}

	foreach ( $order_data as $v ) {
		Boxes_info_booking::update_status( $v['id'], 'apb-completed' );
		AWE_function::update_available( $from, $to, $v['order_room_id'], 0 );
	}
}
add_action( 'apb-completed_shop_order', 'apb_update_available_when_completed' );


/**
 * Add room when import.
 * @param  int    $post_id Room type ID.
 * @param  string $key     Meta key.
 * @param  string $value   Meta value.
 * @return void
 */
function apb_add_room_import( $post_id, $key, $value ) {
	if ( 'apb_room_type' == get_post_type( $post_id ) && 'number_of_rooms' == $key && ! empty( $value ) ) {
		AWE_function::bulk_create_rooms( $post_id, $value );
	}
}
add_action( 'import_post_meta', 'apb_add_room_import', 10, 3 );


/**
 * Remove room when trash room type.
 * @param  int $post_id Room type ID.
 * @return void
 * @since 2.1
 */
function apb_remove_room_when_remove_room_type( $post_id ) {
	$number_rooms = get_post_meta( $post_id, 'number_of_rooms', true );
	AWE_function::bulk_remove_rooms( $post_id, $number_rooms );
}
add_action( 'trash_apb_room_type', 'apb_remove_room_when_remove_room_type' );


/**
 * Create room when untrash room type.
 * @param  WP_Post $post Room type object.
 * @return void
 * @since 2.1
 */
function apb_restore_room_when_restore_room_type( $post ) {
	if ( 'apb_room_type' != $post->post_type ) {
		return;
	}

	$number_rooms = get_post_meta( $post->ID, 'number_of_rooms', true );
	AWE_function::bulk_create_rooms( $post->ID, $number_rooms );
}
add_action( 'trash_to_publish', 'apb_restore_room_when_restore_room_type' );


function apb_wpml_copy_package( $post_id ) {
	if ( 'apb_room_type' != get_post_type( $post_id ) && ! function_exists( 'icl_object_id' ) ) {
		return;
	}

	global $wpdb;
	$lang = wpml_get_default_language();
	$parent_id = icl_object_id( $post_id, 'post', true, $lang );

	if ( $parent_id == $post_id ) {
		return;
	}

	$wpdb->query( "CREATE TEMPORARY TABLE tmp SELECT * FROM {$wpdb->prefix}apb_booking_options WHERE object_id = {$parent_id};" );

	$wpdb->query( "UPDATE tmp SET id = NULL, object_id = {$post_id};" );

	$wpdb->query( "INSERT INTO {$wpdb->prefix}apb_booking_options SELECT * FROM tmp;" );
}
// add_action( 'publish_apb_room_type', 'apb_wpml_copy_package' );
// add_action( 'icl_copy_from_original', 'apb_wpml_copy_package' );

$translate_option = array(
	'apb_email_footer',
	'apb_mail_new_booking',
	'apb_mail_pending',
	'apb_mail_complete',
	'apb_mail_cancel',
);

function apb_wpml_filter_option( $value, $key ) {
	if ( function_exists( 'icl_translate' ) ) {
		$has_translation = null;
		if ( is_array( $value ) ) {
			foreach ( $value as $k => $v ) {
				$value[ $k ] = icl_translate( 'admin_texts_' . $key, '[' . $key . ']' . $k, $v, false, $has_translation, ICL_LANGUAGE_CODE );
			}

			return $value;
		}

		return icl_translate( 'admin_texts_' . $key, $key, $value, false, $has_translation, ICL_LANGUAGE_CODE );
	}

	return $value;
}

foreach ( $translate_option as $key ) {
	add_filter( 'option_' . $key, 'apb_wpml_filter_option', 10, 2 );
}
