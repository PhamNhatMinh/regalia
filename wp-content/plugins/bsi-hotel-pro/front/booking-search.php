<?php
	include(WP_PLUGIN_DIR."/bsi-hotel-pro/includes/conf.class.php");
	include(WP_PLUGIN_DIR."/bsi-hotel-pro/includes/search.class.php");
	include("language.php");
	global $wpdb;
	$bsisearch = new bsiSearch();
	$bsiCore->clearExpiredBookings();
	wp_enqueue_style('jquery-style121', plugins_url().'/bsi-hotel-pro/front/css/jquery.lightbox-0.5.css');
	wp_enqueue_style('jquery-style234', plugins_url().'/bsi-hotel-pro/front/css/colorbox.css');
	wp_enqueue_style('jquery-style333', plugins_url().'/bsi-hotel-pro/front/css/flexslider.css');
	
	wp_enqueue_script( 'custom_script1', plugins_url().'/bsi-hotel-pro/front/js/hotelvalidation.js');
	wp_enqueue_script( 'custom_script2', plugins_url().'/bsi-hotel-pro/front/js/jquery.lightbox-0.5.min.js');
	wp_enqueue_script( 'custom_script3', plugins_url().'/bsi-hotel-pro/front/js/jquery.colorbox.js');
	wp_enqueue_script( 'custom_script4', plugins_url().'/bsi-hotel-pro/front/js/jquery.flexslider.js');
	wp_enqueue_style('style-pro', plugins_url().'/bsi-hotel-pro/front/css/prostyle11.css'); 
?> 
<script type="text/javascript">
            jQuery(window).load(function() {
               jQuery('.flexslider').flexslider({
                    animation: "fade",
                    controlNav: true,
                    directionNav: true
                });
            });
        </script>
        <script>
		function currency_change(ccode)
		{	
			<?php
				
				if(isset($_SESSION['url'])){
					$url = $_SESSION['url'];
				}else{
					$url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
					$_SESSION['url'] = $url;
				}
				if(strpos($url,'?')===false){
			?>
			window.location.href = '<?php echo $url; ?>?currency=' + ccode +'&check_in=checkin';	
			
			<?php
				}else{
			?>
			window.location.href = '<?php echo $url; ?>&currency=' + ccode +'&check_in=checkin';	
			<?php		
				}
			?>
		}	
	
        </script>
    
        
                     <div class="wizard">
                     	<div class="step1 greenStep"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/1.png" alt="" /><p><?php echo SELECT_DATES_TEXT; ?></p></div>
                        <div class="step2 yellowStep"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/2.png" alt="" /><p><?php echo RATES_TEXT; ?></p></div>
                        <div class="step3 grayStep"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/3.png" alt="" /><p><?php echo YOUR_DETAILS_TEXT; ?></p></div>
                        <div class="step4 grayStep"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/4.png" alt="" /><p><?php echo PAYMENT_TEXT; ?></p></div>
                        <div class="step5 grayLastStep"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/5.png" alt="" /><p><?php echo CONFIRM_TEXT; ?></p></div>
                     </div>
                     <div class="progress">
						 <div class="bar bar-success" style="width: 20%;">20% Complete</div>
                         <div class="bar bar-warning" style="width: 20%;"></div>
                     </div> 
    
                     
                      
                            <h2><?php echo SEARCH_INPUT_TEXT; ?> (<a href="<?php the_permalink(); ?>"><?php echo MODIFY_SEARCH_TEXT; ?></a>)</h2>
                            <table class="normal-table">
                                <tbody>
                                    <tr>
                                      <td><strong>
                                        <?php echo CHECK_IN_D_TEXT; ?>
                                        :</strong></td>
                                      <td><?php echo $bsisearch->checkInDate; ?></td>
                                    </tr>
                                    <tr>
                                      <td><strong>
                                        <?php echo CHECK_OUT_D_TEXT; ?>
                                        :</strong></td>
                                      <td><?php echo $bsisearch->checkOutDate; ?></td>
                                    </tr>
                                    <tr>
                                      <td><strong>
                                        <?php echo TOTAL_NIGHTS_TEXT; ?>:</strong></td>
                                      <td><?php echo $bsisearch->nightCount; ?></td>
                                    </tr>
                                    <tr>
                                      <td><strong>
                                        <?php echo ADULT_ROOM_TEXT; ?>
                                        :</strong></td>
                                      <td><?php echo $bsisearch->guestsPerRoom?></td>
                                    </tr>
                                    
                                     <?php if($bsisearch->childPerRoom){ ?>
                                     <tr>
                                     <td><strong><?php echo CHILD_PER_ROOM_TEXT; ?>:</strong></td>
                                     <td><?php echo $bsisearch->childPerRoom; ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            
                             <form name="searchresult" id="searchresult" method="post" action="<?php the_permalink(); ?>" onSubmit="return validateSearchResultForm('<?php echo SELECT_ONE_ROOM_ALERT; ?>');">
                        
                        <div class="wrapper">
                            <div class="htitel">
                                <h2 class="fl" style="border:0; margin:0;"><?php echo SEARCH_RESULT_TEXT; ?></h2>
                                <div class="control-group fr">
                                    <div class="controls">
                                        <?php echo $bsiCore->get_currency_combo2($bsisearch->currency); ?> <br /><br />
                                    </div>
                                </div>
                            </div>
                           
                                    <?php
										$gotSearchResult = false;
										$idgenrator = 0;
										$ik=1;
										foreach($bsisearch->roomType as $room_type){
											foreach($bsisearch->multiCapacity as $capid => $capvalues){
												$room_result = $bsisearch->getAvailableRooms($room_type['rtid'], $room_type['rtname'], $capid);
													
													$sqlroomcheck=$wpdb->get_row("select * from bsi_room where roomtype_id=".$room_type['rtid']." and capacity_id=".$capid,ARRAY_A);
													if(!empty($sqlroomcheck)){
													echo '<script> jQuery(document).ready(function() { ';
													echo '
														jQuery("#iframe_'.str_replace(" ","",$room_type['rtid']).'_'.str_replace(" ","",$capid).$ik.'").colorbox({iframe:true, width: (document.body.offsetWidth) + \'px\', height: "90%"});
														jQuery("#iframe_details_'.str_replace(" ","",$room_type['rtid']).'_'.str_replace(" ","",$capid).$ik.'").colorbox({iframe:true,  width: jQuery(\'#body-div\').width() + \'px\', height: "60%"}); 
														jQuery(".group_'.$room_type['rtid'].'_'.$capid.'").colorbox({rel:\'group_'.$room_type['rtid'].'_'.$capid.'\', maxWidth: jQuery(\'#body-div\').width(), maxHeight:"80%", slideshow:true, slideshowSpeed:5000});
														
															';
													echo '}); </script>';
													echo '<script type="text/javascript">
															jQuery(document).ready(function() {
																jQuery("#mySlides_'.$capid.'_'.$room_type['rtid'].' a").lightBox();
															});
													  </script>';	
				  
	                          ?>
                            <!-- start of search row --> 
                            <div class="container-fluid" style="margin:0; padding:0;">
                                <div class="row-fluid" style="padding: 1% 0">
                                   
                                    <div class="span12">
                                        <div class="span5">
                                            <div class="search-gallery">
                                                <div class="flexslider" style="cursor:pointer; cursor:hand;" >
                                                    <ul class="slides">
                                                    
                                                        <?php echo $bsiCore->roomtype_photos($room_type['rtid'],$capid); ?>        
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span7">
                                            <div class="search-details">
                                                <table cellpadding="5" cellspacing="0" border="0" width="100%"   style="text-align:left; " >
                                                 <?php if($room_result['specail_price_flag']){ $offertag='style="background:url('.plugins_url().'/bsi-hotel-pro/front/images/offer.png) no-repeat left top; padding-left:23px; height:35px;"'; }else{ $offertag='';  }?> 
                                                
                                                    <tr>
                                                        <td width="100%" <?php echo $offertag; ?>>&nbsp; 
                                                            <span  style="font-size:18px;"><strong><?php echo $room_type['rtname']; ?></strong> (<?php echo $capvalues['captitle']; ?>) <?php if($room_result['child_flag']){ ?> <?php echo WITH_CHILD; ?> <?php } ?></strong> </span>  <span style="float:right;">
                                                                <a href="<?php echo plugins_url()."/bsi-hotel-pro/front/" ?>roomtype-details.php?tid=<?php echo $room_type['rtid']; ?>" id='iframe_details_<?php echo str_replace(" ","",$room_type['rtid']).'_'.str_replace(" ","",$capid).$ik; ?>' style="font-weight:bold;" ><?php echo VIEW_ROOM_FACILITIES_TEXT; ?></a></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="100%" valign="top" style="font-size:13px">
                                                            <table  width="100%" class="table table-bordered table2">
                                                                <tr>
                                                                    <td colspan="2"  >
                                                                        <span style=" font-size:14px; font-weight:bold">
                                                                            <a  id='iframe_<?php echo str_replace(" ","",$room_type['rtid']).'_'.str_replace(" ","",$capid).$ik; ?>' href="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/calendar.php?rtype=<?php echo $room_type['rtid']; ?>&cid=<?php echo $capid; ?>" title='<span  style="font-size:16px;"><strong><?php echo $room_type['rtname']; ?></strong> ( <?php echo $capvalues['captitle']; ?> ) </span>' ><?php echo VIEW_NIGHTLY_PRICE_TEXT; ?> &amp; <?php echo CALENDAR_AVAILABILITY_TEXT; ?></a>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                                <tr> 
                                                                    <td><strong><?php echo MAX_OCCUPENCY_TEXT; ?></strong></td>
                                                                    <td><?php echo $capvalues['capval']; ?>
                    <?php echo ADULT_TEXT; ?> <?php if($room_result['child_flag']){ ?> <?php echo AND_TEXT; ?> <?php echo $bsisearch->childPerRoom;?> <?php echo CHILD_TEXT; ?><?php } ?> <?php echo PER_ROOM_TEXT; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td ><strong> <?php echo TOTAL_PRICE_OR_ROOM_TEXT; ?> </strong></td>
                                                                     <?php if($room_result['specail_price_flag']){ ?>
                                                                    <td ><span style="font-weight:bold; "> <del><?php echo $bsiCore->get_currency_symbol($bsisearch->currency).$bsiCore->getExchangemoney($room_result['totalprice'],$bsisearch->currency); ?></del> </span>  <strong><?php echo $bsiCore->get_currency_symbol($bsisearch->currency).$bsiCore->getExchangemoney($room_result['total_specail_price'],$bsisearch->currency); ?></strong> <?php if($room_result['child_flag']){ ?> (included <span style="color:#cc0000; text-decoration:line-through;"><?php echo $bsiCore->get_currency_symbol($bsisearch->currency).$bsiCore->getExchangemoney($room_result['total_child_price'],$bsisearch->currency); ?></span> <?php echo $bsiCore->get_currency_symbol($bsisearch->currency).$bsiCore->getExchangemoney($room_result['total_child_price2'],$bsisearch->currency); ?> <?php echo FOR_TEXT; ?> <?php echo $bsisearch->childPerRoom;?> <?php echo CHILD_TEXT; ?>) <?php }?></td>
                                                                    
                                                                    <?php }else{ ?>
                                                                    <td ><span style="font-weight:bold;"><strong><?php echo $bsiCore->get_currency_symbol($bsisearch->currency).$bsiCore->getExchangemoney($room_result['totalprice'],$bsisearch->currency); ?></strong> <?php if($room_result['child_flag']){ ?> (included <?php echo $bsiCore->get_currency_symbol($bsisearch->currency).$bsiCore->getExchangemoney($room_result['total_child_price'],$bsisearch->currency); ?> <?php echo FOR_TEXT; ?> <?php echo $bsisearch->childPerRoom;?> <?php echo CHILD_TEXT; ?>) <?php }?></td>
                                                                     <?php } ?>
                                                                </tr>
                                                                <tr>
														      <?php
                                                                    if(intval($room_result['roomcnt']) > 0){
                                                                    $gotSearchResult = true;
                                                                    ?>
                                                                
                                                                    <td ><strong><?php echo SELECT_NUMBER_OF_ROOM_TEXT; ?></strong></td>
                                                                    <td >
                                                                        <select name="svars_selectedrooms[]" class="input-mini">
                                                                            <?php echo $room_result['roomdropdown']; ?>
                                                                        </select>
                                                                    </td>
                                                                    <?php }else{ 
																	echo '<script> jQuery(document).ready(function() { ';
																		echo '
																			jQuery("#iframe2_'.str_replace(" ","",$room_type['rtid']).'_'.str_replace(" ","",$capid).$ik.'").colorbox({iframe:true, width: jQuery(\'.container-fluid\').width() + \'px\', height: "80%"});
																			
																				';
																		echo '}); </script>';
			 		                                               ?>
                                                                    
                                                                    <td  colspan="2">
                                                                       <strong>Not Available</strong></span> ( <a style="font-size:13px" id='iframe2_<?php echo str_replace(" ","",$room_type['rtid']).'_'.str_replace(" ","",$capid).$ik; ?>' href="<?=plugins_url();?>/bsi-hotel-pro/front/calendar.php?rtype=<?php echo $room_type['rtid']; ?>&cid=<?php echo $capid; ?>" title='<span  style="font-size:16px;"><strong><?php echo $room_type['rtname']; ?></strong> ( <?php echo $capvalues['captitle']; ?> ) </span>' ><strong><?php echo CHECK_AVILABILITY; ?> </strong></a> )
                                                                    </td>
                                                                   <?php } ?> 
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>  
                            <!-- end of search row --> 
                            <?php } } }
							
							$flag88=0;
								if($gotSearchResult){
									echo '<div id="" style="width:100% !important;"><table cellpadding="5" cellspacing="0" border="0" width="100%" >';
									echo '<tr><td align="right" style="padding-right:30px;"></td></tr>';
									echo '</table></div>';	
								$flag88=1;
								}else{
									echo '<table cellpadding="4" cellspacing="0" width="100%"><tbody><tr><td style="font-size:13px;" align="center"><br /><br />';
									if($bsisearch->searchCode == "SEARCH_ENGINE_TURN_OFF"){
										echo SORRY_ONLINE_BOOKING_CURRENTLY_NOT_AVAILABLE_TEXT;				
									}else if($bsisearch->searchCode == "OUT_BEFORE_IN"){
										echo SORRY_YOU_HAVE_ENTERED_A_INVALID_SEARCHING_CRITERIA_TEXT;				
									}else if($bsisearch->searchCode == "NOT_MINNIMUM_NIGHT"){
										echo MINIMUM_NUMBER_OF_NIGHT_SHOULD_NOT_BE_LESS_THAN_TEXT.' '.$bsiCore->config['conf_min_night_booking'].' '. PLEASE_MODIFY_YOUR_SEARCHING_CRITERIA_TEXT;
									}else if($bsisearch->searchCode == "TIME_ZONE_MISMATCH"){
										$tempdate = date("l F j, Y G:i:s T");
										echo BOOKING_NOT_POSSIBLE_FOR_CHECK_IN_DATE_TEXT.' '.$bsisearch->checkInDate.' '. PLEASE_MODIFY_YOUR_SEARCHING_CRITERIA_TO_HOTELS_DATE_TIME_TEXT.'<br>'. HOTELS_CURRENT_DATE_TIME_TEXT.' '.$tempdate; 
									}else{
										echo SORRY_NO_ROOM_AVAILABLE_AS_YOUR_SEARCHING_CRITERIA_TRY_DIFFERENT_DATE_SLOT;
									}
									echo '<br /><br /><br /></td></tr></tbody></table>';
								}
							
							 ?>
                            
                        
                        </div>
                       
                                <div class="row-fluid" style=" padding: 1% 0">
                                <?php if($flag88){  ?>
                                    <div class="span12">
                                        <div class="back1" style="display: inline-block;
    float: left;
    margin-left: 2.5%;
    width: 30%;">
                                            <button id="registerButton" type="button" onClick="window.location.href='<?php the_permalink(); ?>'" ><?php echo BACK_TEXT; ?></button>
                                        </div>
                                        <div class="continue2" style="display: inline-block;
    float: right;
    margin-right: 2.5%;
    ">
                                            <button id="registerButton" name="registerButton" type="submit" class="conti2" ><?php echo CONTINUE_TEXT; ?></button>
                                        </div>
                                    </div>
                                    <?php }else{ ?>
                                    
                                       <div class="span12">
                                        <div class="back1">
                                            <button id="registerButton" type="button" onClick="window.location.href='<?php the_permalink(); ?>'" ><?php echo BACK_TEXT; ?></button>
                                        </div>
                                        
                                    </div>
                                      <?php } ?> 
                                </div> 
                               </form> 
                          
                    
