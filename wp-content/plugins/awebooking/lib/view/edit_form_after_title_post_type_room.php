<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * AWE custom field data of post type room
 *
 * @version		1.0
 * @package		AweBooking/admin/
 * @author 		AweTeam
 */


?>
<div class="form-item form-type-textfield form-item-label awe-form-new-room">
	<div class="awe-plugin" style="display: inline-block">
			<div class="box-select-room-type-js">
				<div class="box-select-room-wrapper">
					<table class="sticky-header awe-new-room">
						<thead>
							<tr>
								<th>
									<div class="th-wrapper">
										<?php echo _e('Room type', 'awebooking') ?>
										<button type="button" class="close-box-room-type-js awe-button-close">✕</button>
									</div>
								</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="awe-input-rooms-wrapper">
			<div class="form-elements">
				<label><?php _e("Base price",'awebooking') ?> </label>
				<input name="base_price" type="text" class="form-text" size="5" value="<?php echo esc_attr( $room_price ) ?>">
				<?php AWE_function::awe_help('Default pricing of room.') ?>
			</div>

			<div class="form-elements">
				<label><?php _e("Room type",'awebooking') ?> </label>
				 <select name="room_type">
					<?php
						$type_id = isset($_GET['room_type']) ? $_GET['room_type'] : $room_type_id;
					?>
				</select>
			  <?php AWE_function::awe_help('Room type: will have many different types of rooms to facilitate the management.') ?>
			</div>
		</div>

		<fieldset class="rooms-unit-type-defaults form-wrapper">
			<div class="fieldset-wrapper awe-rooms-unit-row">
				<fieldset class="rooms- -type-guest-capacity form-wrapper">
					<legend>
						<span class="fieldset-legend"><?php _e("Sleeping capacity",'awebooking') ?></span>
					</legend>
					<div class="fieldset-wrapper">
						<div class="form-item form-type-textfield form-item-data-min-sleeps">
							<div class="form-elements">
								<input name="min_sleeps"  type="text" class="form-text apb-int" size="5" value="<?php echo esc_attr($room_min_sleeps)  ?>">
							   <?php AWE_function::awe_help('The total number of guests (adults and children) minimum allowed in this room. ') ?>
							</div>
						</div>
						<div class="form-item form-type-textfield form-item-data-max-sleeps">
							<div class="form-elements">
								<input type="text" class="form-text apb-int" name="max_sleeps" size="5" value="<?php echo esc_attr($room_max_sleeps)  ?>">
								 <?php AWE_function::awe_help('The total number of guests (adults and children) maximum allowed in this room. ') ?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="rooms-unit-type-child-capacity form-wrapper">
					<legend>
						<span class="fieldset-legend"><?php _e("Child capacity",'awebooking') ?></span>
					</legend>
					<div class="fieldset-wrapper">
						<div class="form-item form-type-textfield ">
							<div class="form-elements">
								<input type="text" class="form-text apb-int" name="min_children" size="5" value="<?php echo esc_attr($room_min_children)  ?>">
								<?php AWE_function::awe_help('The number minimum of children allowed in this room. ') ?>
							</div>
						</div>
						<div class="form-item form-type-textfield ">
							<div class="form-elements">
								<input type="text" class="form-text apb-int" name="max_children" size="5" value="<?php echo esc_attr($room_max_children)  ?>">
								<?php AWE_function::awe_help('The number maximum of children allowed in this room. ') ?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset  class="rooms-unit-bed-arrangement form-wrapper">
					<legend>
						<span class="fieldset-legend"><?php _e("Bed arrangement",'awebooking') ?></span>
					</legend>
					<div class="fieldset-wrapper">
						<div class="form-item form-type-textfield ">
							<div class="form-elements">
								<input type="text" class="form-text apb-int" name="bed_singles"  size="5" value="<?php echo esc_attr($room_bed_singles)  ?>">
								<?php AWE_function::awe_help('The number of single beds.') ?>
							</div>
						</div>
						<div class="form-item form-type-textfield ">
							<div class="form-elements">
								<input type="text" class="form-text apb-int" name="bed_doubles" size="5" value="<?php echo esc_attr($room_bed_doubles)  ?>">
								<?php AWE_function::awe_help('The number of double beds.') ?>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
		</fieldset>
 </div>
