<?php 
if(isset($_GET['delid'])){
	include("includes/db.conn.php"); 
	include("includes/conf.class.php");
	include("includes/admin.class.php");
	$bsiAdminMain->delete_currency();
	header("location:admin.php?page=currency-manager");	
	exit;
}

include("includes/admin.class.php");


?>
<br />
<br />

<div id="container-inside"> <span style="font-size:16px; font-weight:bold"><?php echo CURRENCY_LIST; ?></span>
<input type="button" value="<?php echo ADD_NEW_CURRENCY; ?>" onClick="window.location.href='admin.php?page=currency-manager-edit&id=0'" style="background: #EFEFEF; float:right"/><br />
<?php 
	if(isset($_SESSION['msg']))
	{
		echo '<font color="#FF0000">'.$_SESSION['msg'].'</font>';
		unset($_SESSION['msg']);
	}
	
?>
<?php
	wp_enqueue_style('jquery-style', plugins_url().'/bsi-hotel-pro/front/css/datepicker.css');
	//wp_enqueue_script( 'custom_script10', plugins_url().'/bsi-hotel-pro/js/bsi_datatables.js');
	wp_enqueue_script( 'custom_script11', plugins_url().'/bsi-hotel-pro/js/DataTables/jquery.dataTables.js');
	wp_enqueue_style('custom-style9', plugins_url().'/bsi-hotel-pro/css/data.table.css');
	
	
?>

 <hr />
 <table class="display datatable" border="0">
  <thead>
   <tr>
    <th width="20%"><?php echo CURRENCY_CODE_LIST; ?></th>
    <th width="15%"><?php echo CURRENCY_SYMBOL_LIST; ?></th>
    <th width="20%"><?php echo EXCHANGE_RATE; ?></th>
    <th width="20%"><?php echo DEFAULT_CURRENCY; ?></th>
    <th>&nbsp;</th>
   </tr>
  </thead>
  <?=$bsiAdminMain->generatecurrency()?>
 </table>

</div>

<script>
 jQuery(document).ready(function() {
	 	var oTable = jQuery('.datatable').dataTable( {
				 "responsive": true,
				"bJQueryUI": true,
				"sScrollX": "",
				"bSortClasses": false,
				"aaSorting": [[0,'asc']],
				"bAutoWidth": true,
				"bInfo": true,
				"sScrollY": "100%",	
				"sScrollX": "100%",
				"bScrollCollapse": true,
				"sPaginationType": "full_numbers",
				"bRetrieve": true,
				"oLanguage": {
								"sSearch": "<?=DT_SEARCH?>:",
								"sInfo": "<?=DT_SINFO1?> _START_ <?=DT_SINFO2?> _END_ <?=DT_SINFO3?> _TOTAL_ <?=DT_SINFO4?>",
								"sInfoEmpty": "<?=DT_INFOEMPTY?>",
								"sZeroRecords": "<?=DT_ZERORECORD?>",
								"sInfoFiltered": "(<?=DT_FILTER1?> _MAX_ <?=DT_FILTER2?>)",
								"sEmptyTable": "<?=DT_EMPTYTABLE?>",
								"sLengthMenu": "<?=DT_LMENU?> _MENU_ <?=DT_SINFO4?>",
								"oPaginate": {
												"sFirst":    "<?=DT_FIRST?>",
												"sPrevious": "<?=DT_PREV?>",
												"sNext":     "<?=DT_NEXT?>",
												"sLast":     "<?=DT_LAST?>"
											  }
							 }
	} );
} );
</script> 


