<?php
	
	
	include(WP_PLUGIN_DIR."/bsi-hotel-pro/includes/conf.class.php");
	include(WP_PLUGIN_DIR."/bsi-hotel-pro/includes/mail.class.php");
   	include("language.php");
	global $wpdb;
    $booking_id = $_POST['bookingid'];
	$emailBody  = '';
	
    $invoiceROWS= $wpdb->get_row("SELECT client_name, client_email, invoice FROM bsi_invoice WHERE booking_id='".$booking_id."'",ARRAY_A);
	$ccArray    = array();
	$bsiMail    = new bsiMail();	
	$emailContent=$bsiMail->loadEmailContent();
	$subject    = $emailContent['subject'];
	$emailBody .= "Dear ".$invoiceROWS['client_name'].",<br><br>";
	$emailBody .= $emailContent['body'];
	$emailBody .= $invoiceROWS['invoice'];
				
	$cardnum        = $_POST['CardNumber'];
	$cc_holder_name = $_POST['cc_holder_name'];
	$CardType       = $_POST['CardType'];
	$cc_exp_dt      = $_POST['cc_exp_dt'];
	$cc_ccv         = $_POST['cc_ccv'];
	$cardnum_enc    = $bsiCore->encryptCard($_POST['CardNumber']);
	$cardno_len=strlen($cardnum)-4;
	$creditcard_no=substr($cardnum,$cardno_len);
	$star='';
	for($i=0;$i<$cardno_len;$i++){ $star.='#';}
	$show_cardno=$star.$creditcard_no;
	
	$payoptions = "Credit Card";
	$table      = '<br /><table  style="font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;" cellpadding="4" cellspacing="1"><tr><td align="left" colspan="2" style="font-weight:bold; font-variant:small-caps; background:#eeeeee;">'.addslashes(INV_PAY_DETAILS).'</td></tr><tr><td align="left" width="30%" style="font-weight:bold; font-variant:small-caps;background:#ffffff;">'.addslashes(INV_PAY_OPTION).'</td><td align="left" style="background:#ffffff;">'.$payoptions.'</td></tr><tr><td align="left" width="30%" style="font-weight:bold; font-variant:small-caps;background:#ffffff;">'.addslashes(CC_NUMBER).'</td><td align="left" style="background:#ffffff;">'.$show_cardno.'</td></tr></table>';
	$updatedInvoice=$invoiceROWS['invoice'].$table;
	$wpdb->query("Update bsi_invoice SET invoice='$updatedInvoice' WHERE booking_id='".$booking_id."'");				
	$wpdb->query("insert into bsi_cc_info(booking_id, cardholder_name, card_type, card_number, expiry_date, ccv2_no) values('".addslashes($_POST['bookingid'])."', '".addslashes($_POST['cc_holder_name'])."', '".addslashes($_POST['CardType'])."', '".$cardnum_enc."', '".addslashes($_POST['cc_exp_dt'])."', '".addslashes($_POST['cc_ccv'])."')");
	
	$emailBody .= $table;
	
	
	$emailBody .= '<br><br>'.addslashes(PP_REGARDS).',<br>'.$bsiCore->config['conf_hotel_name'].'<br>'.$bsiCore->config['conf_hotel_phone'];
	$emailBody .= '<br><br><font style=\"color:#F00; font-size:10px;\">[ '.addslashes(PP_CARRY).' ]</font>';		
		
	$returnMsg = $bsiMail->sendEMail($invoiceROWS['client_email'],$subject, $emailBody);
	if ($returnMsg == true) {
	    $wpdb->query("update bsi_bookings set payment_success=true where booking_id=".$booking_id);
			
		$notifyEmailSubject = "Booking no.".$_POST['bookingid']." - Notification of Room Booking by ".$invoiceROWS['client_name'];				
		$notifynMsg = $bsiMail->sendEMail($bsiCore->config['conf_notification_email'], $notifyEmailSubject, $invoiceROWS['invoice']);
		include('booking-confirm.php');
		die; 
	}else {
		header('Location: booking-failure.php?error_code=25');
		die;
	}		
?>