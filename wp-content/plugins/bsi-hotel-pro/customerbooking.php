<?php

if(isset($_REQUEST['delete'])){
	include("includes/db.conn.php");
	include("includes/conf.class.php");
	include("includes/admin.class.php");
	$bsiAdminMain->booking_cencel_delete(2);
	$client = $_REQUEST['client'];
	header("location:admin.php?page=Customer-booking&client=".$client);
	exit;
}
if(isset($_REQUEST['cancel'])){
	include("includes/db.conn.php");
	include("includes/conf.class.php");	
	include("includes/admin.class.php");
	include("includes/mail.class.php");	
	$bsiAdminMain->booking_cencel_delete(1); 
	$client = $_REQUEST['client']; 
	header("location:admin.php?page=Customer-booking&client=".$client); 
	exit;
}
if(isset($_GET['client'])){
	include("includes/conf.class.php");
	include("includes/admin.class.php");
	$client    = mysql_real_escape_string($_GET['client']);
	$delClient = $client;
	
	$htmlArr   = $bsiAdminMain->fetchClientBookingDetails($client);
	
	$html      = $htmlArr['html'];
}else{
	header("location:customerlookup.php");
	exit;
}

wp_enqueue_script( 'custom_script10', plugins_url().'/bsi-hotel-pro/js/bsi_datatables.js');
wp_enqueue_script( 'custom_script11', plugins_url().'/bsi-hotel-pro/js/DataTables/jquery.dataTables.js');
//wp_enqueue_style('custom-style9', plugins_url().'/bsi-hotel-pro/css/data.table.css');
wp_enqueue_style('custom-style10', plugins_url().'/bsi-hotel-pro/css/jqueryui.css');
?>

<script type="text/javascript"> 
function myPopup2(booking_id){
	var width = 730;
	var height = 650;
	var left = (screen.width - width)/2;
	var top = (screen.height - height)/2;
	var url='<?php echo plugins_url(); ?>/bsi-hotel-pro/print_invoice.php?bid='+booking_id;
	var params = 'width='+width+', height='+height;
	params += ', top='+top+', left='+left;
	params += ', directories=no';
	params += ', location=no';
	params += ', menubar=no';
	params += ', resizable=no';
	params += ', scrollbars=yes';
	params += ', status=no';
	params += ', toolbar=no';
	newwin=window.open(url,'Chat', params);
	if (window.focus) {newwin.focus()}
	return false;
}
function cancel(bid){
	var answer = confirm ('<?php echo CUSTOMER_BOOKING_ARE_YOU_SURE_WANT_TO_CANCEL_BOOKING;?>');  
	if (answer)
		window.location="admin.php?page=Customer-booking&noheader=true&cancel="+bid+"&client="+<?php echo $delClient; ?>;
}
function booking_delete(delid){
	var answer = confirm ('<?php echo CUSTOMER_BOOKING_ARE_YOU_WANT_TO_DELETE_THE_BOOKING;?>');
	if (answer) 
		window.location="admin.php?page=Customer-booking&noheader=true&delete="+delid+"&client="+<?php echo  $delClient; ?>; 
	}
</script>
<p>&nbsp;</p>
<div id="container-inside"> <span style="font-size:16px; font-weight:bold"><?php echo CUSTOMER_BOOKING_LIST_OF;?> <?php echo $htmlArr['clientName']; ?></span>
  <hr />
 <table class="display datatable" border="0">
  <thead>
   <tr>
    <th width="9%" nowrap><?php echo CUSTOMER_BOOKING_ID;?></th>
    <th width="18%" nowrap><?php echo CUSTOMER_BOOKING_NAME;?></th>
    <th width="8%" nowrap="nowrap"><?php echo CUSTOMER_BOOKING_CHECK_IN_DATE;?></th>
    <th width="10%" nowrap><?php echo CUSTOMER_BOOKING_CHECK_OUT_DATE;?></th>
    <th width="10%" nowrap><?php echo CUSTOMER_BOOKING_AMOUNT;?></th>
    <th width="9%" nowrap><?php echo CUSTOMER_BOOKING_DATE;?></th>
    <th width="8%" nowrap="nowrap"><?php echo CUSTOMER_BOOKING_STATUS;?></th>
    <th width="25%">&nbsp;</th>
   </tr>
  </thead>
  <?php echo $html; ?>
 </table>
</div>
<script>
 jQuery(document).ready(function() {
	 	var oTable = jQuery('.datatable').dataTable( {
				 "responsive": true,
				"bJQueryUI": true,
				"sScrollX": "",
				"bSortClasses": false,
				"aaSorting": [[0,'desc']],
				"bAutoWidth": true,
				"bInfo": true,
				"sScrollY": "100%",	
				"sScrollX": "100%",
				"bScrollCollapse": true,
				"sPaginationType": "full_numbers",
				"bRetrieve": true,
				"oLanguage": {
								"sSearch": "Search:",
								"sInfo": "Showing _START_ to _END_ of_TOTAL_ entries",
								"sInfoEmpty": "Showing 0 to 0 of 0 entries",
								"sZeroRecords": "No matching records found",
								"sInfoFiltered": "(filtered from _MAX_total entries)",
								"sEmptyTable": "No data available in table",
								"sLengthMenu": "Show _MENU_ entries",
								"oPaginate": {
												"sFirst":    "First",
												"sPrevious": "Previous",
												"sNext":     "Next",
												"sLast":     "Last"
											  }
							 }
	} );
} );
</script> 