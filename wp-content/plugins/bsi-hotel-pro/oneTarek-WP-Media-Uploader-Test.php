<?php
/*
Plugin Name: oneTarek WP Media Uploader Test
Description: Tetsing the use of WordPress Media Uploader in my plugin settings page.
Author: oneTarek
Author URI: http://onetarek.com
Version: 1.0.0
Source: http://onetarek.com/wordpress/how-to-use-wordpress-media-uploader-in-plugin-or-theme-admin-page/
*/


define ( 'ONETAREK_WPMUT_PLUGIN_URL', plugin_dir_url(__FILE__)); // with forward slash (/).

	function onetarek_wpmut_plugin_menu()
	{
	add_menu_page('oneTarek Media Uploader Test' , 'oneTarek Media Uploader Test', 'manage_options', 'oneTarek_wpmut_plugin_page', 'oneTarek_wpmut_plugin_page');
	}
	add_action('admin_menu', 'onetarek_wpmut_plugin_menu');
	
function onetarek_wpmut_admin_scripts() 
{
 if (isset($_GET['page']) && $_GET['page'] == 'oneTarek_wpmut_plugin_page')
	 {
		 wp_enqueue_script('jquery');
		 wp_enqueue_script('media-upload');
		 wp_enqueue_script('thickbox');
		 wp_register_script('my-upload', ONETAREK_WPMUT_PLUGIN_URL.'onetarek-wpmut-admin-script.js', array('jquery','media-upload','thickbox'));
		 wp_enqueue_script('my-upload');
	 }
}

function onetarek_wpmut_admin_styles()
{
 if (isset($_GET['page']) && $_GET['page'] == 'oneTarek_wpmut_plugin_page')
	 {
		 wp_enqueue_style('thickbox');
	 }
}
add_action('admin_print_scripts', 'onetarek_wpmut_admin_scripts');
add_action('admin_print_styles', 'onetarek_wpmut_admin_styles');
 
 
 function oneTarek_wpmut_plugin_page()
 {
 		echo '<div class="wrap">';
		echo '<div id="icon-options-general" class="icon32"></div><h2>oneTarek\'s WP Media Uploader Test</h2>';
		?>
		<table class="widefat">
			<thead>
				<tr><th colspan="2">Upload Files</th></tr>
			</thead>
			<tr>
				<td width="150"><span class="field_name">Image Location</span></td>
				<td>
				<input type="text"  name="image_location" value="" size="40" />
				<input type="button" class="onetarek-upload-button button" value="Upload Image" />
				<br /><span>Enter the image location or upload an image from your computer.</span>
				</td>
			</tr>

			<tr>
				<td width="150"><span class="field_name">Document Location</span></td>
				<td>
				<input type="text"  name="document_location"  value="" size="40" />
				<input type="button" class="onetarek-upload-button button" value="Upload Image" />
				<br /><span>Enter the document location or upload an doc file from your computer.</span>
				</td>
			</tr>
			
		</table>
		
		<h2>
			Developed by <span style="color:#006600;">Md. Jahidul Islam (oneTarek)</span>
 		</h2>
		If you need any help feel free to contact with me here <a href="http://onetarek.com">oneTarek.com</a><br />
<br />
<br />
<br />
Read full Tutorial and code explanetion here <a  target="_blank" href="http://onetarek.com/wordpress/how-to-use-wordpress-media-uploader-in-plugin-or-theme-admin-page/">
How to use WordPress Media Uploader in Plugin or Theme Admin page
</a>
		
		
		<?php
		echo "</div>";
 
 
 }
?>