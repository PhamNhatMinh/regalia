<?php
include(WP_PLUGIN_DIR."/bsi-hotel-pro/includes/conf.class.php");
include(WP_PLUGIN_DIR."/bsi-hotel-pro/includes/details.class.php");
include("language.php");
$bsibooking = new bsiBookingDetails();
$bsiCore->clearExpiredBookings();
wp_enqueue_style('style-pro', plugins_url().'/bsi-hotel-pro/front/css/prostyle11.css'); 
wp_enqueue_style('jquery-style1', plugins_url().'/bsi-hotel-pro/front/css/jquery.lightbox-0.5.css');
wp_enqueue_style('jquery-style2', plugins_url().'/bsi-hotel-pro/front/css/colorbox.css');
wp_enqueue_style('jquery-style3', plugins_url().'/bsi-hotel-pro/front/css/flexslider.css');

wp_enqueue_script( 'custom_script1', plugins_url().'/bsi-hotel-pro/front/js/hotelvalidation.js');
wp_enqueue_script( 'custom_script2', plugins_url().'/bsi-hotel-pro/front/js/jquery.lightbox-0.5.min.js');
wp_enqueue_script( 'custom_script3', plugins_url().'/bsi-hotel-pro/front/js/jquery.colorbox.js');
wp_enqueue_script( 'custom_script4', plugins_url().'/bsi-hotel-pro/front/js/jquery.flexslider.js');
?>        
        
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery('#btn_exisitng_cust').click(function() {
		//alert(jQuery('#btn_exisitng_cust').val())
		if(jQuery('#btn_exisitng_cust').val() == '1'){
	    jQuery('#exist_wait').html("<img src='<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/ajax-loader.gif' />");
		var querystr = 'actioncode=2&existing_email='+jQuery('#email_addr_existing').val()+'&login_password='+jQuery('#login_password').val(); 
		jQuery.post("<?php echo plugins_url(); ?>/bsi-hotel-pro/front/ajaxreq-processor.php", querystr, function(data){ 						 			
			if(data.errorcode == 0){
				jQuery('#title').html(data.title)
				jQuery('#fname').val(data.first_name)
				jQuery('#lname').val(data.surname)
				jQuery('#str_addr').val(data.street_addr)
				jQuery('#city').val(data.city)
				jQuery('#state').val(data.province)
				jQuery('#zipcode').val(data.zip)
				jQuery('#country').val(data.country)
				jQuery('#phone').val(data.phone)
				jQuery('#fax').val(data.fax)
				jQuery('#email').val(data.email)
				jQuery('#login_c').html('leave blank for unchange password!')
				jQuery('#id_type').val(data.id_type)
				jQuery('#id_number').val(data.id_number)
				jQuery('#exist_wait').html("")
				jQuery('#btn_exisitng_cust').html('Logout')
				jQuery('#btn_exisitng_cust').val('2')
				jQuery('#forgot_pass').html('')
				jQuery("#email_addr_existing").attr("disabled", "disabled"); 
				jQuery("#login_password").attr("disabled", "disabled");  
				jQuery("#password").removeClass("required");
			}else { 
				alert(data.strmsg);
				jQuery('#fname').val('')
				jQuery('#lname').val('')
				jQuery('#str_addr').val('')
				jQuery('#city').val('')
				jQuery('#state').val('')
				jQuery('#zipcode').val('')
				jQuery('#country').val('')
				jQuery('#phone').val('')
				jQuery('#fax').val('')
				jQuery('#email').val('')
				jQuery('#login_c').html('')
				jQuery('#id_type').val('')
				jQuery('#id_number').val('')
				jQuery('#exist_wait').html("")
			}	
		}, "json");
		
		}else if(jQuery('#btn_exisitng_cust').val() == '2'){
			 jQuery('#exist_wait').html("<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/ajax-loader.gif")
				jQuery('#btn_exisitng_cust').html('Login')
				jQuery('#btn_exisitng_cust').val('1')
				jQuery("#email_addr_existing").removeAttr("disabled"); 
				jQuery("#login_password").removeAttr("disabled");  
				jQuery('#email_addr_existing').val('')
				jQuery('#login_password').val('')
			    jQuery('#fname').val('')
				jQuery('#lname').val('')
				jQuery('#str_addr').val('')
				jQuery('#city').val('')
				jQuery('#state').val('')
				jQuery('#zipcode').val('')
				jQuery('#country').val('')
				jQuery('#phone').val('')
				jQuery('#fax').val('')
				jQuery('#email').val('')
				jQuery('#login_c').html('')
				jQuery('#id_type').val('')
				jQuery('#id_number').val('')
				//jQuery('#forgot_pass').html('Forgot Password?')
				jQuery("#password").addClass("required");
				jQuery('#exist_wait').html("")
		}else if(jQuery('#btn_exisitng_cust').val() == '3'){
			jQuery('#exist_wait').html("<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/ajax-loader.gif")
			var querystr = 'actioncode=9&existing_email='+jQuery('#email_addr_existing').val(); 
			jQuery.post("ajaxreq-processor.php", querystr, function(data){ 
			if(data.errorcode == 0){
				alert('Your password has been reset. Please check your email and login!')
				jQuery('#label_pass').show()
				jQuery('#input_pass').show()
				jQuery('#btn_exisitng_cust').val('1')
				jQuery('#btn_exisitng_cust').html('Login')
				jQuery('#exist_wait').html("")
			}else{
				alert(data.strmsg);
				jQuery('#exist_wait').html("")
			}
			}, "json");
		}
	});
	
	jQuery('#forgot_pass').toggle(function() {
				jQuery('#label_pass').hide()
				jQuery('#input_pass').hide()
				jQuery('#btn_exisitng_cust').val('3')
				jQuery('#btn_exisitng_cust').html('Submit')
				jQuery('#forgot_pass').html('Back to Login')
		}, function() {
		        jQuery('#label_pass').show()
				jQuery('#input_pass').show()
				jQuery('#btn_exisitng_cust').val('1')
				jQuery('#btn_exisitng_cust').html('Login')
				jQuery('#exist_wait').html("")	
				//jQuery('#forgot_pass').html('Forgot Password?')
		
	});
});
function myPopup2(booking_id){
		var width = 730;
		var height = 650;
		var left = (screen.width - width)/2;
		var top = (screen.height - height)/2;
		var url='terms-and-services.php?bid='+booking_id;
		var params = 'width='+width+', height='+height;
		params += ', top='+top+', left='+left;
		params += ', directories=no';
		params += ', location=no';
		params += ', menubar=no';
		params += ', resizable=no';
		params += ', scrollbars=yes';
		params += ', status=no';
		params += ', toolbar=no';
		newwin=window.open(url,'Chat', params);
		if (window.focus) {newwin.focus()}
		return false;
   }
</script>
       
           
                        <?php $bookingDetails = $bsibooking->generateBookingDetails(); ?>
                       <!-- <h1 style="padding-top:10px;"><?php echo $bsiCore->config['conf_hotel_name']; ?> </h1>-->
                       
                       
                     <div class="wizard">
                     	<div class="step1 greenStep2"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/1.png" alt="" /><p><?php echo SELECT_DATES_TEXT; ?></p></div>
                        <div class="step2 greenStep"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/2.png" alt="" /><p><?php echo RATES_TEXT; ?></p></div>
                        <div class="step3 yellowStep"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/3.png" alt="" /><p><?php echo YOUR_DETAILS_TEXT; ?></p></div>
                        <div class="step4 grayStep"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/4.png" alt="" /><p><?php echo PAYMENT_TEXT; ?></p></div>
                        <div class="step5 grayLastStep"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/5.png" alt="" /><p><?php echo CONFIRM_TEXT; ?></p></div>
                       
                     </div>
                     <div class="progress">
						<div class="bar bar-success" style="width: 40%;">40% Complete</div>
						 	<div class="bar bar-warning" style="width: 20%;"></div>
                     </div>
                        
                        <div class="wrapper">
                            <h2><?php echo BOOKING_DETAILS_TEXT; ?></h2>
                            
                            <table cellpadding="4" cellspacing="1" class="  ">
                                <tr>
                                    <td  align="center"><strong><?php echo CHECKIN_DATE_TEXT; ?></strong></td>
                                    <td  align="center"><strong><?php echo CHECKOUT_DATE_TEXT; ?></strong></td>
                                    <td  align="center"><strong><?php echo TOTAL_NIGHT_TEXT; ?></strong></td>
                                    <td  align="center"><strong><?php echo TOTAL_ROOMS_TEXT; ?></strong></td>
                                </tr>
                                <tr>
                                    <td align="center" ><?php echo $bsibooking->checkInDate; ?></td>
                                    <td align="center" ><?php echo $bsibooking->checkOutDate; ?></td>
                                    <td align="center" ><?php echo $bsibooking->nightCount; ?></td>
                                    <td align="center" ><?php echo $bsibooking->totalRoomCount; ?></td>
                                </tr>
                                <tr>
                                    <td  align="center"><strong><?php echo NUMBER_OF_ROOM_TEXT; ?></strong></td>
                                    <td  align="center"><strong><?php echo ROOM_TYPE_TEXT; ?></strong></td>
                                    <td  align="center"><strong><?php echo MAXI_OCCUPENCY_TEXT; ?></strong></td>
                                    <td  class="al-r" style="padding-right:5px;"><strong><?php echo GROSS_TOTAL_TEXT; ?></strong></td>
                                </tr>
                                 <?php		
									foreach($bookingDetails as $bookings){		
									$child_title2=($bookings['child_flag2'])? " + ".$bookings['childcount3']." ".CHILD_TEXT." ":"";
										echo '<tr>';
										echo '<td align="center" >'.$bookings['roomno'].'</td>';
										echo '<td align="center" >'.$bookings['roomtype'].' ('.$bookings['capacitytitle'].')</td>';				
										echo '<td align="center" >'.$bookings['capacity'].' '.INV_ADULT.' '.$child_title2.'</td>';
											
										echo '<td class="al-r"  style="padding-right:5px;">'.$bsiCore->get_currency_symbol($_SESSION['sv_currency']).$bsiCore->getExchangemoney($bookings['grosstotal'],$_SESSION['sv_currency']).'</td>';
										echo '</tr>';		
									}
								 ?>                                
                               
                                    <td  class="al-r" colspan="3" ><strong><?php echo SUB_TOTAL_TEXT; ?></strong></td>
                                    <td  class="al-r"  style="padding-right:5px;"><strong> <?php echo $bsiCore->get_currency_symbol($_SESSION['sv_currency']).$bsiCore->getExchangemoney($bsibooking->roomPrices['subtotal'],$_SESSION['sv_currency']); ?></strong></td>
                                </tr>
                                 <?php
									if($bsiCore->config['conf_tax_amount'] > 0 &&  $bsiCore->config['conf_price_with_tax']==0){
										$taxtext=""; 
									?>
                                <tr>
                                    <td class="al-r" colspan="3" ><?php echo TAX_TEXT; ?>(
     <?php echo $bsiCore->config['conf_tax_amount']; ?>
     %)</td>
                                    <td class="al-r"  style="padding-right:5px;"><span id="taxamountdisplay"><?php echo $bsiCore->get_currency_symbol($_SESSION['sv_currency']).$bsiCore->getExchangemoney($bsibooking->roomPrices['totaltax'],$_SESSION['sv_currency']); ?></span></td>
                                </tr>
                                <?php }else{
											$taxtext="(".BD_INC_TAX.")";
										}
										?>
                                
                                <tr>
                                    <td  class="al-r" colspan="3" ><strong><?php echo GRAND_TOTAL_TEXT; ?></strong> <?php echo $taxtext; ?></td>
                                    <td class="al-r"  style="padding-right:5px;"><strong> <span id="grandtotaldisplay"><?php echo $bsiCore->get_currency_symbol($_SESSION['sv_currency']).$bsiCore->getExchangemoney($bsibooking->roomPrices['grandtotal'],$_SESSION['sv_currency']); ?></span></strong></td>
                                </tr>
                                 <?php 
										if($bsiCore->config['conf_enabled_deposit'] && ($bsibooking->depositPlans['deposit_percent'] > 0 && $bsibooking->depositPlans['deposit_percent'] < 100)){
										?>
                                
                                <tr id="advancepaymentdisplay">
                                    <td class="al-r" colspan="3" ><strong></strong> <?php echo ADVANCE_PAYMENT_TEXT; ?>(<span style="font-size:11px;">
     <?php echo $bsibooking->depositPlans['deposit_percent']; ?>
     %<?php echo OF_GRAND_TOTAL_TEXT; ?></span>)</td>
                                    <td class="al-r"  style="padding-right:5px;"><span id="advancepaymentamount"><?php echo $bsiCore->get_currency_symbol($_SESSION['sv_currency']).$bsiCore->getExchangemoney($bsibooking->roomPrices['advanceamount'],$_SESSION['sv_currency']); ?> </span></td>
                                </tr>
                                <?php
                                }
							?>
                            </table>
                            
                        </div>
                        
                        <div class="wrapper">
                            <div class="htitel">
                                <h2 class="fl" style="border:0; margin:0;"><?php echo CUSTOMER_DETAILS_TEXT; ?></h2>
                            </div>
                            <!-- start of search row --> 
                            <div class="container-fluid" style="margin:0; padding:0;">
                                <div class="row-fluid" style="padding: 1% 0">
                                    <div class="span12">
                                        <h3 style="margin-left:2.5%"><?php echo EXISTING_CUSTOMER_TEXT; ?>?</h3>
                                        
                                        <form class="form-horizontal"  method="post" id="form1" style="width: 95%; margin: 0 2.5%">
                                            <div class="control-group">
                                                <label class="control-label" for="ea"><?php echo EMAIL_ADDRESS_TEXT; ?>:</label>
                                                <div class="controls">
                                                    <input type="text" name="email_addr_existing" id="email_addr_existing"  class="input-large" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="pa"><?php echo PASSWORD;?>:</label>
                                                <div class="controls">
                                                    <input type="password" name="login_password" id="login_password"  class="input-large" />
                                                </div>
                                            </div>
                                             <div class="control-group">
                                                <div class="controls" id="exist_wait" >
                                                    
                                                </div>
                                              </div>
                                              
                                            <div class="control-group">
                                                <label class="control-label"></label>
                                                <div class="controls">
                                                    <button id="btn_exisitng_cust" value="1" type="button" style="display:inline-block"><?php echo FETCH_DETAILS_TEXT; ?></button>
                                                    <!--<a href="javascript:;" id="forgot_pass" style="width:150px; display:inline-block; padding-left:10px; cursor:pointer;"><?php echo FORGOT_PASSWORD;?></a>-->
                                                </div>
                                            </div>
                                            
                                           <div class="text-center"><h1><?php echo OR_TEXT; ?></h1></div> 
                                            
                                      <h3 align="left" style="padding-left:5px; color:#999;"><?php echo NEW_CUSTOMER_TEXT; ?>?</h3>    
                                        <input type="hidden" name="allowlang" id="allowlang" value="no" />
                                            <div class="control-group">
                                                <label class="control-label" name="title" for="title"><?php echo TITLE_TEXT?>: </label>
                                                <div class="controls">
                                                    <select id="title" class="input-small">
                                                        <option value="Mr."><?php echo MR_TEXT; ; ?>.</option>
                                                       <option value="Ms."><?php echo MS_TEXT; ?>.</option>
                                                       <option value="Mrs."><?php echo MRS_TEXT; ?>.</option>
                                                       <option value="Miss."><?php echo MISS_TEXT; ?>.</option>
                                                       <option value="Dr."><?php echo DR_TEXT; ?>.</option>
                                                       <option value="Prof."><?php echo PROF_TEXT; ?>.</option>
                                                     </select>
                                                </div>
                                            </div>
                                            
                                            <div class="control-group">
                                                <label class="control-label" for="fn"><?php echo FIRST_NAME_TEXT; ?>:</label>
                                                <div class="controls">
                                                    <input type="text" name="fname" id="fname" class="input-large required">
                                                </div>
                                            </div>
                                            
                                            <div class="control-group">
                                                <label class="control-label" for="ln"><?php echo LAST_NAME_TEXT; ?>:</label>
                                                <div class="controls">
                                                    <input type="text" name="lname" id="lname" class="input-large required">
                                                </div>
                                            </div>
                                            
                                            <div class="control-group">
                                                <label class="control-label" for="ln"><?php echo ADDRESS_TEXT; ?>:</label>
                                                <div class="controls">
                                                    <input type="text" name="str_addr" id="str_addr" class="input-large required">
                                                </div>
                                            </div>                                          
                                            
                                            
                                            <div class="control-group">
                                                <label class="control-label" for="ct"><?php echo CITY_TEXT; ?>:</label>
                                                <div class="controls">
                                                    <input type="text" name="city"  id="city" class="input-large required">
                                                </div>
                                            </div>
                                            
                                            <div class="control-group">
                                                <label class="control-label" for="st"><?php echo STATE_TEXT; ?>:</label>
                                                <div class="controls">
                                                    <input name="state"  id="state" type="text" class="input-large required">
                                                </div>
                                            </div>
                                            
                                            <div class="control-group">
                                                <label class="control-label" for="pc"><?php echo POSTAL_CODE_TEXT; ?>:</label>
                                                <div class="controls">
                                                    <input name="zipcode"  id="zipcode" type="text" class="input-large required number">
                                                </div>
                                            </div>
                                            
                                            <div class="control-group">
                                                <label class="control-label" for="co"><?php echo COUNTRY_TEXT; ?>:</label>
                                                <div class="controls">
                                                    <input name="country"  id="country" type="text" class="input-large required">
                                                </div>
                                            </div>
                                            
                                            <div class="control-group">
                                                <label class="control-label" for="ph"><?php echo PHONE_TEXT; ?>:</label>
                                                <div class="controls">
                                                    <input name="phone"  id="phone" type="text" class="input-large required number">
                                                </div>
                                            </div>
                                            
                                            <div class="control-group">
                                                <label class="control-label" for="fx"><?php echo FAX_TEXT; ?>:</label>
                                                <div class="controls">
                                                    <input name="fax"  id="fax" type="text" class="input-large">
                                                </div>
                                            </div>
                                            
                                            <div class="control-group">
                                                <label class="control-label" for="it"><?php echo ID_TYPE; ?>:</label>
                                                <div class="controls">
                                                    <input name="id_type"  id="id_type" type="text" class="input-large"><span class="help-inline"><?php echo EG_PASSPORT;?></span>
                                                </div>
                                            </div>
                                            
                                            <div class="control-group">
                                                <label class="control-label" for="in"><?php echo ID_NUMBER; ?>:</label>
                                                <div class="controls">
                                                    <input name="id_number"  id="id_number" type="text" class="input-large required"><span class="help-inline"><?php echo EG_PASSPORT_NO;?></span>
                                                </div>
                                            </div>
                                            
                                            <div class="control-group">
                                                <label class="control-label" for="em"><?php echo EMAIL_TEXT; ?>:</label>
                                                <div class="controls">
                                                    <input name="email"  id="email" type="text" class="input-large required email">
                                                </div>
                                            </div>
                                            
                                            <div class="control-group">
                                                <label class="control-label" for="ps"><?php echo PASSWORD;?>:</label>
                                                <div class="controls">
                                                    <input name="password"  id="password" type="password" class="input-large required">
                                                </div>
                                            </div>
                                            
                                            <div class="control-group">
                                                <label class="control-label" for="pb"><?php echo PAYMENT_BY_TEXT; ?>:</label>
                                                <div class="controls">
                                                
                                                <?php
													$paymentGatewayDetails = $bsiCore->loadPaymentGateways();				
													foreach($paymentGatewayDetails as $key => $value){ 	
														echo '<label class="radio"><input type="radio" name="payment_type" id="payment_type_'.$key.'" value="'.$key.'" class="required" /> '.$value['name'].'</label>';
													}
													?>
                                                    <label class="error" generated="true" for="payment_type" style="display:none;"><?php echo FIELD_REQUIRED_ALERT; ?>.</label>                                                    
                                                </div>
                                            </div>
                                            
                                            <div class="control-group">
                                                <label class="control-label" for="ar"><?php echo ADDITIONAL_REQUESTS_TEXT; ?>:</label>
                                                <div class="controls">
                                                    <textarea rows="3" id='ar' name="message" class="input-large"></textarea>
                                                    
                                                    <label class="checkbox">
                                                        <input type="checkbox" name="tos" id="tos" value="" class="required">
                                                        <?php echo I_AGREE_WITH_THE_TEXT; ?> <a target="_blank" href="<?php echo $bsiCore->config['conf_term_and_condition']; ?>"> <?php echo TERMS_AND_CONDITIONS_TEXT; ?>.</a>
                                                    </label>
                                                    
                                                </div>
                                            </div>                                        
                                    </div>
                                </div>
                            </div>  
                            <!-- end of search row --> 
                        </div>
                        <div class="wrapper" style="margin-bottom: 20px">
                            <div class="container-fluid" style="margin:0; padding:0;">
                                <div class="row-fluid" style="padding: 1% 0">
                                    <div class="span12">
                                        <div class="back1" style="display: inline-block;
    float: left;
    margin-left: 2.5%;
    width: 30%;">
                                        <?php
											if(isset($_SESSION['url'])){
												$url = $_SESSION['url'];
											}else{
												$url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
												$_SESSION['url'] = $url;
											}
											if(strpos($url,'?')===false){
												$url = $url.'?check_in=check_in';
											}else{
												$url = $url.'&check_in=check_in';
											}
										?>
                                        
                                            <button id="registerButton" type="button" onClick="window.location.href='<?php echo $url; ?>'" ><?php echo BACK_TEXT; ?></button>
                                        </div>
                                        
                                        <div class="continue1" style="display: inline-block;
    float: right;
    margin-right: 2.5%;
    ">
                                            <button id="registerButton" name="registerButton2" type="submit" class="conti" ><?php echo CONTINUE_TEXT; ?></button>
                                        </div>
                                    </div>
                                </div>  </div>
                        </form>
                    </div>
                    
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#form1").validate();
		
     });
         
</script> 
<script src="<?php echo plugins_url(); ?>/bsi-hotel-pro/js/jquery.validate.js" type="text/javascript"></script>
               