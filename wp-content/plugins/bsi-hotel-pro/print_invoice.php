<?php  

include("includes/db.conn.json.php");
include("includes/conf.class.php");
include("includes/admin.class.php");
global $wpdb;
$lang_row = $wpdb->get_row("SELECT * FROM `bsi_language` where lang_default='1'",ARRAY_A);
	include_once("languages/".$lang_row['lang_file']);
if(isset($_REQUEST['bid'])){
 $r_invoice=$wpdb->get_row("select * from bsi_invoice where booking_id=".$bsiCore->ClearInput($_REQUEST['bid']),ARRAY_A);
 
 $r_booking=$wpdb->get_row("select * from bsi_bookings where booking_id=".$bsiCore->ClearInput($_REQUEST['bid']),ARRAY_A);
 $paymentgateway = $bsiAdminMain->getPayment_Gateway($r_booking['payment_type']);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Print Booking Invoice</title>
<script type="text/javascript">
window.print()
</script>
</head>
<body>
<div align="center">
 <p>&nbsp;</p>
 <table cellpadding="3"  border="0" width="700">
  <tr>
   <td width="400" align="left" valign="top"><span style="font-family:Arial, Helvetica, sans-serif; font-size:24px; font-weight:bold;">
    <?php echo $bsiCore->config['conf_hotel_name']?>
    </span><br />
    <span style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
    <?php echo $bsiCore->config['conf_hotel_streetaddr']?>
    </span><br />
    <span style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
    <?php echo $bsiCore->config['conf_hotel_city']?>
    </span><br />
    <span style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
    <?php echo $bsiCore->config['conf_hotel_state']." ". $bsiCore->config['conf_hotel_zipcode']?>
    </span><br />
    <span style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
   <?php echo $bsiCore->config['conf_hotel_country']?>
    </span><br /></td>
   <td width="200" align="right" valign="top"><span style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><b><?php echo VIEWDETAILS_PHONE;?>:</b>
    <?php echo $bsiCore->config['conf_hotel_phone']?>
    </span><br />
    <span style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><b><?php echo VIEWDETAILS_FAX;?>:</b>
    <?php echo $bsiCore->config['conf_hotel_fax']?>
    </span><br />
    <span style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><b><?php echo VIEWDETAILS_EMAIL;?>:</b>
    <?php echo $bsiCore->config['conf_hotel_email']?>
    </span><br /></td>
  </tr>
 </table>
 <br />
 <?php echo $r_invoice['invoice']?>
</div>
</body>
</html>
