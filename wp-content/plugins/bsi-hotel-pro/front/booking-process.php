<?php
include(WP_PLUGIN_DIR."/bsi-hotel-pro/includes/conf.class.php");
include(WP_PLUGIN_DIR."/bsi-hotel-pro/includes/mail.class.php");
include(WP_PLUGIN_DIR."/bsi-hotel-pro/includes/process.class.php");
include("language.php");  
$bookprs = new BookingProcess();
switch($bookprs->paymentGatewayCode){	
	case "poa":
	
		 $bi = $bookprs->bookingId;
		 $be = $bookprs->clientEmail;
		 $cname = $bookprs->clientName; 
		 $htm = $bookprs->invoiceHtml;
		processPayOnArrival($bi,$be,$cname,$htm);
		break;
		
	case "pp": 	
		$id=$bookprs->bookingId;
		$tm=$bookprs->totalPaymentAmount;
		processPayPal($id,$tm);
		break;	
					
	case "cc":
	
		$payamount=$bookprs->totalPaymentAmount;
		$bookid=$bookprs->bookingId;
		processCreditCard($payamount , $bookid);
		break;
				
	case "an":
		$id=$bookprs->bookingId;
		$tm=$bookprs->totalPaymentAmount;
		processAuthorizeNet($id,$tm);
		break;
		
	case "2co":
		process2Checkout();
		break;
		
			
	case "st":
		$c_mail = $bookprs->clientEmail;
		$total_payment = $bookprs->totalPaymentAmount;
		$bookingId = $bookprs->bookingId;
		processStripe($c_mail,$total_payment,$bookingId);
			
	default:
		processOther();
}
/* PAY ON ARIVAL: MANUAL PAYMENT */	
function processPayOnArrival($bbi,$bbe,$cname,$htm){	

	global $wpdb;
	$bsiCore = new bsiHotelCore;
	$bsiMail = new bsiMail();

	$emailContent=$bsiMail->loadEmailContent();
	$subject    = $emailContent['subject'];
	$wpdb->query("UPDATE bsi_bookings SET payment_success=true WHERE booking_id = ".$bbi);
	$wpdb->query("UPDATE bsi_clients SET existing_client = 1 WHERE email = '".$bbe."'");		
			
	$emailBody  = "Dear "." ".$cname.",<br><br>";
	$emailBody .= $emailContent['body']."<br><br>";
	$emailBody .= $htm;
	$emailBody .= '<br><br>'.PP_REGARDS.',<br>'.$bsiCore->config['conf_hotel_name'].'<br>'.$bsiCore->config['conf_hotel_phone'];
	$emailBody .= '<br><br><font style=\"color:#F00; font-size:10px;\">[ '.PP_CARRY.' ]</font>';	
				
	$returnMsg = $bsiMail->sendEMail($bbe, $subject, $emailBody);
	
	if ($returnMsg == true) {		
		
		$notifyEmailSubject = "Booking No ".$bbi." - Notification of Room Booking by ".$cname;		
		$notifynMsg = $bsiMail->sendEMail($bsiCore->config['conf_notification_email'], $notifyEmailSubject, $htm);
		include('booking-confirm.php');	 
	}else {
		//header('Location:'.$_SESSION['url'].'&error_code=25');  
		
		echo "<script>
		alert(".BOOKING_FAILURE_ERROR_25.")
		</script>";
		die;
	}
	//header('Location: booking-confirm.php?success_code=1');
}
/* PAYPAL PAYMENT */ 
function processPayPal($i , $t){
	
	$bsiCore = new bsiHotelCore;
	echo "<script language=\"JavaScript\">";
	echo "document.write('<form action=\"\" method=\"post\" name=\"formpaypal\">');";
	echo "document.write('<input type=\"hidden\" name=\"amount\"  value=\"".(($bsiCore->config['conf_payment_currency']=='1')? $bsiCore->getExchangemoney($bookprs->totalPaymentAmount,$_SESSION['sv_currency']) : number_format($t, 2))."\">');";
	echo "document.write('<input type=\"hidden\" name=\"invoice\"  value=\"".$i."\">');";
	echo "document.write('</form>');";
	echo "setTimeout(\"document.formpaypal.submit()\",500);";
	echo "</script>";	
}
/* CREDIT CARD PAYMENT */
function processCreditCard($pa , $bi){
	$bsiCore = new bsiHotelCore;
	$paymentAmount = number_format($pa, 2, '.', '');
	
	echo "<script language=\"javascript\">";
	echo "document.write('<form action=\"\" method=\"post\" name=\"form2checkout\">');";
	echo "document.write('<input type=\"hidden\" name=\"x_invoice_num\" value=\"".$bi."\"/>');";
	echo "document.write('<input type=\"hidden\" name=\"total\" value=\"".(($bsiCore->config['conf_payment_currency']=='1')? $bsiCore->getExchangemoney($paymentAmount,$_SESSION['sv_currency']) : $paymentAmount)."\">');"; 
	echo "document.write('</form>');";
	echo "setTimeout(\"document.form2checkout.submit()\",500);";
	echo "</script>";
}

function processAuthorizeNet($bookingId,$totalPaymentAmount){
	$bsiCore = new bsiHotelCore;
	$_SESSION['paymentAmount']=$totalPaymentAmount;
	$_SESSION['bookingId']=$bookingId;
	
	echo "<script language=\"JavaScript\">";
	echo "document.write('<form action=\"\" method=\"post\" name=\"forman\">');";
	echo "document.write('<input type=\"hidden\" name=\"ansubmit2\"  value=\"an\">');";
	echo "document.write('</form>');";
	echo "setTimeout(\"document.forman.submit()\",500);";
	echo "</script>";	
	die;
	
	
	
	include("an_direct_post.php");
	die;
}

/* PAYPAL PAYMENT */ 
/*function process2Checkout(){
	$bookprs = new BookingProcess();
	$bsiCore = new bsiHotelCore;
	$paymentGatewayDetails = $bsiCore->loadPaymentGateways();
	$_SESSION['paymentAmount']=$bookprs->totalPaymentAmount;
	echo "<script language=\"JavaScript\">";
	echo "document.write('<form action=\"https://www.2checkout.com/checkout/spurchase\" method=\"post\" name=\"twocopayment\">');";
	echo "document.write('<input type=\"hidden\" name=\"sid\" value=\"".$paymentGatewayDetails['2co']['account']."\" >');";
	echo "document.write('<input type=\"hidden\" name=\"mode\" value=\"2CO\" >');";
	//echo "document.write('<input type=\"hidden\" name=\"demo\" value=\"N\"/>');";
	echo "document.write('<input type=\"hidden\" name=\"li_0_type\" value=\"product\" >');";
	echo "document.write('<input type=\"hidden\" name=\"li_0_name\" value=\"Booking : ".$bsiCore->config['conf_hotel_name']."\" >');";
	echo "document.write('<input type=\"hidden\" name=\"li_0_price\" value=\"".$bookprs->totalPaymentAmount."\" >');";
	echo "document.write('<input type=\"hidden\" name=\"invoice\"  value=\"".$bookprs->bookingId."\">');";
	echo "document.write('</form>');";
	echo "setTimeout(\"document.twocopayment.submit()\",500);";
	echo "</script>";	
}*/

function processStripe($clientEmail,$totalPaymentAmount,$bookingId){
	$_SESSION['clientEmail']=$clientEmail;
	$_SESSION['paymentAmount']=$totalPaymentAmount;
	$_SESSION['bookingId']=$bookingId;
	//header('Location: stripe-processor.php');
	echo "<script language=\"JavaScript\">";
	echo "document.write('<form action=\"\" method=\"post\" name=\"forman\">');";
	echo "document.write('<input type=\"hidden\" name=\"ansubmit\"  value=\"an\">');";
	echo "document.write('</form>');";
	echo "setTimeout(\"document.forman.submit()\",500);";
	echo "</script>";	
	
	die;
}
/* OTHER PAYMENT */
function processOther(){
	/* not implemented yet */
	header('Location: booking-failure.php?error_code=22');
	die;
}
?>