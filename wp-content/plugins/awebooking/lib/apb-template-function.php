<?php
/**
 * The template for displaying content in the plugin template
 *
 * @author      AweTeam
 * @package 	AweBooking/function
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


/* ============================== Template Page Check Available =====================
 *  Page Check Available layout
 * ======================================================================= */

function form_check_availability() {

	$apb_setting   = get_option( 'apb_setting' );
	$from          = isset( $_GET['from'] ) ? $_GET['from'] : '';
	$to            = isset( $_GET['to'] ) ? $_GET['to'] : '';
	$total_night = AWE_function::get_number_of_nights( $from, $to );
	$room_type_id = isset( $_GET['room_type_id'] ) ? absint( $_GET['room_type_id'] ) : 0;

	if ( ! empty( $from ) && ! empty( $to ) ) {
		$total_day = count( AWE_function::range_date( $from, $to ) );
		$_from = AWE_function::convert_date_to_mdY( $from );
		$_to = AWE_function::convert_date_to_mdY( $to );
	} else {
		$total_day = 0;
	}

	include AWE_function::template_exsits( 'layout/apb-form-check-availability' );
}

/*
 * Layout step for header plugin
 */
function html_step( $active = '' ) {
   include AWE_function::template_exsits( 'layout/apb-step' );
}

function loop_data_check_availability() {
	include AWE_function::template_exsits( 'loop/apb-list-check-availability' );
}

function layout_room_select() {
	$total_cart = count( AWE_function::get_cart() );
	$room_adult  = isset( $_POST['room_adult'] ) ? $_POST['room_adult'] : 1;
	$room_child  = isset( $_POST['room_child'] ) ? $_POST['room_child'] : 0;
	include AWE_function::template_exsits( 'layout/apb-room-select' );
}

function layout_review_book() {
	include AWE_function::template_exsits( 'layout/apb-review_book' );
}

function layout_list_review_book() {
	$i = 1;
	$total_price = 0;
	$apb_cart = AWE_function::get_cart();

	if ( ! empty( $apb_cart ) ) {
		foreach ( $apb_cart as $key => $item ) {
			$room_num   = $i++;
			$room_type_id = wp_get_post_parent_id( $key );
			$room_info  = get_post( $room_type_id );
			$range_date = AWE_function::range_date( $item['from'], $item['to'] );
			$total_night  = count( $range_date ) - 1;
			$room_price = get_post_meta( $room_type_id, 'base_price', true );

			$extra_adult    = get_post_meta( $room_type_id, 'extra_adult', true );
			$extra_child    = get_post_meta( $room_type_id, 'extra_child', true );

			$price = $item['price'];

			$total_price  += $price;

			$room = get_post( $item['room_id'] );

			include AWE_function::template_exsits( 'loop/apb-list-review-book' );
		}
	}
}

function page_checkout() {
	$currency = get_option( 'woocommerce_currency' ) ? get_option( 'woocommerce_currency' ) : '$';
	include AWE_function::template_exsits( 'apb-checkout' );
}

function layout_list_package( $argsData = array() ) {
	$argsData['check'] = isset( $argsData['check'] ) ? $argsData['check'] : true;
	$argsData['count_day'] = isset( $argsData['count_day'] ) ? $argsData['count_day'] : 1;
	$get_option = AWE_function::get_room_option( get_the_ID(), 'apb_room_type' );
	if ( ! empty( $get_option ) ) {
		include AWE_function::template_exsits( 'loop/apb-list-package' );
	}
}

/*
 * Query filter room available
 * Loop and check room ajax.
 */
function loop_content_check_available() {
	$from = wp_kses( $_POST['from'] ,array() );
	$to   = wp_kses( $_POST['to'], array() );
	$year = date( 'Y' );
	$num_args = absint( $_POST['num_args'] );
	$room_adult = absint( $_POST['room_adult'] );
	$room_child = absint( $_POST['room_child'] );

	Apb_show_all_room( $from, $to, $year, 'all', $num_args, $room_adult, $room_child );
}


/**
 * Apb_show_all_room
 * Show all room before check available
 * @param Data $_POST - $_POST['from']|$_POST['to']|$_POST['room_type']|$_POST['num_args']|$_POST['room_adult']|$_POST['room_child']
 */
function Apb_show_all_room( $from, $to, $get_year, $room_type, $num_args, $room_adult, $room_child ) {
	global $wpdb;
	$currency = ( 1 == get_option( 'rooms_checkout_style' ) ) ? get_option( 'woocommerce_currency' ) : get_option( 'woocommerce_currency' );
	$total_day = count(AWE_function::range_date( $from,$to) )-1;
	/*===================================================
	=            Get Room Available Calendar            =
	====================================================*/
	$check = AWE_function::check_room_available( $from, $to );
	$item_ids = array();
	if(!empty( $check) ) {
		 foreach ( $check as $item_id) {
			 $item_ids[] = $item_id->unit_id;
		 }
	}
	/*==========  End of Get Room Available  ===========*/
	$room_by_room_type = get_posts( array(
		'post_type' => 'apb_room_type',
		'posts_per_page' => -1,
		'meta_query' => array( isset( $get_room_type ) ? $get_room_type : '' ),
	) );
	if ( isset( $item_ids ) ) {
		foreach ( $room_by_room_type as $item_id ) {
			if ( ! in_array( $item_id->ID, $item_ids ) ) {
				$empty_room_avb = AWE_function::check_room_available( $from, $to, $item_id->ID );
				if ( empty( $empty_room_avb ) ) {
					$item_ids[] = $item_id->ID;
				}
			}
		}
	} else {
		$item_ids[] = 0;
	}
	$args  = array(
		'post_type'         => 'apb_room_type',
		'post__in'          => ! empty( $item_ids ) ? $item_ids : array( 0 ),
		'posts_per_page'    => -1,
		'meta_query'        => array(
			'relation'      => 'and',
			isset( $get_room_type ) ? $get_room_type : '',
		),
	);

	$list_room_check = new WP_Query( $args );
	$er = '';
	$result_data = null;
	$list_id = array();
	if ( $list_room_check->have_posts() != false && (strtotime( $from) <= strtotime( $to) ) ) {
	while ( $list_room_check->have_posts() ) : $list_room_check->the_post();
		$max_sleeps     = get_post_meta( get_the_ID(), 'max_sleeps', true );
		$min_sleeps     = get_post_meta( get_the_ID(), 'min_sleeps', true );
		$max_children   = get_post_meta( get_the_ID(), 'max_children', true );
		$min_children   = get_post_meta( get_the_ID(), 'min_children', true );
		$min_night      = ( int ) get_post_meta( get_the_ID(), 'min_night', true );
		/*----------  Check by setting: Can book on pending date  ----------*/
		$check_pending = AWE_function::check_room_available( $from, $to, get_the_ID(), true, true, 2 );
		if ( $num_args < count( $room_adult ) ) {
			if ( $room_adult[ $num_args ]['adult'] + $room_child[ $num_args ]['child'] <= $max_sleeps and $room_adult[$num_args]['adult']+$room_child[$num_args]['child'] >= $min_sleeps and $total_day >= $min_night) {
			if ( '' === $max_children && '' === $min_children || $room_child[ $num_args ]['child'] <= $max_children and $room_child[$num_args]['child'] >= $min_children ) {
			  if(get_option( '_booking_pending' ) == 1) {
				/*----------  Check by setting: Can book on pending date  ----------*/
				if(!empty( $check_pending) ) {
					$list_id[] = get_the_ID();
					$result_data = 1;
				}
			  }else{
				 $list_id[] = get_the_ID();
				 $result_data = 1;
			  }
			}
			}else{
				$er = 1;
			}
		}
	endwhile;

	 wp_reset_postdata();
	 $paged = isset( $_POST['paged'] ) ? $_POST['paged'] : 1;
	 if(!empty( $list_id) ) {
		$room = new WP_Query( array(
					  'post_type'         => 'apb_room_type',
					  'post__in'          => $list_id,
					  'posts_per_page'    => get_option( 'limit-page' ),
					  'paged'             => $paged,
					  'orderby' => 'date',
					  'order'   => 'DESC',
				  ) );
		 while ( $room->have_posts() ): $room->the_post();
			$room_price     = get_post_meta(get_the_ID(),"base_price",true);
			$room_desc      = get_post_meta(get_the_ID(),"room_desc",true);

			$extra_child    = get_post_meta(get_the_ID(),"extra_child",true);
			$extra_adult    = get_post_meta(get_the_ID(),"extra_adult",true);
			$extra_sale     = get_post_meta(get_the_ID(),"extra_sale",true);
			$info_price_day = AWE_function::get_pricing_of_days( $from,$to,get_the_ID(),1);
		   include AWE_function::template_exsits("loop/apb-content-check-available-all-room");
		 endwhile;
	 }

	 if( $result_data == null) {
		 do_action( 'apb_notice_available' );
	 }
	 if( $er == "" && isset( $room) ) {
		check_avb_pagination( $room);
	 }
	 wp_reset_postdata();
	}else{
		do_action( 'apb_notice_available' );
	}
}
function body_check_available() {
	if(isset( $_GET['action'] ) && $_GET['action'] == 'filter_available' ) {
	  $roomtype = isset( $_GET['roomtype'] ) ? wp_kses( $_GET['roomtype'], '' ) : "all";
	  query_room_filter(wp_kses( $_GET['from'],""),wp_kses( $_GET['to'],""),$roomtype,wp_kses( $_GET['room_adult'],""),wp_kses( $_GET['room_child'],"") );
	}else{
	   include AWE_function::template_exsits("layout/apb-check-calendar");
	}

}

function query_room_filter( $from,$to,$room_type,$room_adult,$room_child) {

	$check = AWE_function::check_room_available( $from, $to);
	   $item_ids = array();
		if(!empty( $check) ) {
			 foreach ( $check as $item_id) {
				 $item_ids[] = $item_id->unit_id;
			 }
		}

	  $room_by_room_type = get_posts(
	   array(
	   'post_type' => 'apb_room_type',
	   isset( $get_room_type) ? $get_room_type : "",
		)
	  );

	if(isset( $item_ids) ) {
		 foreach ( $room_by_room_type as $item_id) {
			   if(!in_array( $item_id->ID, $item_ids) ) {
				  $empty_room_avb = AWE_function::check_room_available( $from, $to,$item_id->ID);
				   if(empty( $empty_room_avb) ) {
						$item_ids[] = $item_id->ID;
				   }
			   }
		   }
	} else {
		$item_ids[] = 0;
	}

	$paged = isset( $_POST['paged'] ) ? $_POST['paged'] : 1;
	$args = array(
		'post_type'        => 'apb_room_type',
		'post__in'         => $item_ids,
		'posts_per_page'   => get_option( 'limit-page' ),
		'paged' => $paged,
		isset( $get_room_type) ? $get_room_type : '',
	);
	$currency = (get_option( 'rooms_checkout_style' ) == 1) ? get_option( 'woocommerce_currency' ) : get_option( 'woocommerce_currency' );

	$room  = new WP_Query( $args );

	while ( $room->have_posts() ) : $room->the_post();

		$room_price     = get_post_meta( get_the_ID(), 'base_price', true );
		$room_desc      = get_post_meta( get_the_ID(), 'room_desc', true );
		$max_sleeps     = get_post_meta( get_the_ID(), 'max_sleeps', true );
		$min_sleeps     = get_post_meta( get_the_ID(), 'min_sleeps', true );
		$max_children   = get_post_meta( get_the_ID(), 'max_children', true );
		$min_children   = get_post_meta( get_the_ID(), 'min_children', true );

		$extra_child    = get_post_meta( get_the_ID(), 'extra_child', true );
		$extra_adult    = get_post_meta( get_the_ID(), 'extra_adult', true );

		if ( $room_adult[0] + $room_child[0] <= $max_sleeps && $room_adult[0] + $room_child[0] >= $min_sleeps ) {
			if ( $room_child[0] <= $max_children && $room_child[0] >= $min_children ) {
				$info_price_day = AWE_function::get_pricing_of_days( $from, $to, get_the_ID(), 1 );
				include AWE_function::template_exsits( 'loop/apb-content-check-available-all-room' );
			}
		} else {
			do_action( 'apb_notice_available' );
			break;
		}
	endwhile;
	check_avb_pagination( $room );

}

function check_avb_pagination( $room) {
	include AWE_function::template_exsits( 'loop/apb-check_pagination' );
}

function layout_loading() {
	include AWE_function::template_exsits( 'layout/apb-loading' );
}

/*----------  Loop price by show all room  ----------*/
/**
 * Display room price.
 * @param  string  $room_price Room price.
 * @param  integer $show       Show regular price, total price or both
 *     1 - Regular price
 *     2 - Total price
 *     3 - Both
 * @return void
 */
function loop_price( $room_price = '', $show = 1 ) {

	$currency = ( 1 == get_option( 'rooms_checkout_style' ) ) ? get_option( 'woocommerce_currency' ) : get_option( 'woocommerce_currency' );
	if ( isset( $_GET['action'] ) && $_GET['action'] == 'filter_available' ) {
		$from = AWE_function::convert_date_to_mdY( $_GET['from'] );
		$to   = AWE_function::convert_date_to_mdY( $_GET['to'] );
		$total_date = count( AWE_function::range_date( $from, $to ) ) -1;
		$apb_total_price = AWE_function::get_total_price( AWE_function::get_pricing_of_days( $from, $to, get_the_ID() ) );
		$total_price_all = $apb_total_price;
		$info_price_day = AWE_function::get_pricing_of_days( $from, $to, get_the_ID(), 1 );
	} elseif ( isset( $_REQUEST['from'] ) && isset( $_REQUEST['to'] ) ) {
		$from = AWE_function::convert_date_to_mdY( $_REQUEST['from'] );
		$to   = AWE_function::convert_date_to_mdY( $_REQUEST['to'] );
		$total_date = count( AWE_function::range_date( $from, $to ) ) -1;
		$apb_total_price = AWE_function::get_total_price( AWE_function::get_pricing_of_days( $from, $to, get_the_ID() ) );
		$info_price_day = AWE_function::get_pricing_of_days( $from, $to, get_the_ID(), 1 );

		/*----------  Get total price by day and price sale and price extra  ----------*/
		$extra_adult    =  get_post_meta( get_the_ID(), 'extra_adult', true );
		$extra_child    =  get_post_meta( get_the_ID(), 'extra_child', true );
		$extra_sale     =  get_post_meta( get_the_ID(), 'extra_sale', true );
		$total_price_all = $apb_total_price;
		if ( ! empty( $extra_adult ) ) {
			$total_date = count( AWE_function::range_date( $from, $to ) ) -1;
			foreach ( $extra_adult as $item_extra_adult ) {
				if ( $_REQUEST['adult'] == $item_extra_adult['number'] ) {
					$total_price_all += $total_date * $item_extra_adult['price'];
				}
			}
		}
		if ( ! empty( $extra_child ) ) {
			$total_date = count( AWE_function::range_date( $from, $to ) ) -1;
			foreach ( $extra_child as $item_extra_child ) {
				if ( $_REQUEST['child'] == $item_extra_child['number'] ) {
					$total_price_all += $total_date * $item_extra_child['price'];
				}
			}
		}

		if ( ! empty( $extra_sale ) ) {
			$total_date = count( AWE_function::range_date( $from, $to ) );
			$data_extra_sale = AWE_function::apb_get_extra_sale( $extra_sale, $total_date, $from );
			if ( ! empty( $data_extra_sale ) ) {
				if ( 'sub' == $data_extra_sale['sale_type'] ) {
					$total_price_all = $total_price_all - $data_extra_sale['amount'];
				}
				if ( 'decrease' == $data_extra_sale['sale_type'] ) {
					$total_price_all = $total_price_all - $data_extra_sale['amount'] / 100 * $total_price_all;
				}
			}
		}
	} else {
		$apb_total_price = 0;
		$total_price_all = $apb_total_price;
		// $info_price_day = AWE_function::get_pricing_of_days( $from,$to,get_the_ID(),1);
	}
	include AWE_function::template_exsits( 'loop/apb-price' );
}


/**
 * Loop button book ajax all room.
 * @return void
 * @deprecated 2.0
 */
function loop_book() {
	_deprecated_function( __FUNCTION__, '2.0' );
	include AWE_function::template_exsits( 'loop/apb-book' );
}

/**
 * Loop button book ajax only room type.
 * @param  array $args Arguments.
 * @return void
 * @deprecated 2.0
 */
function loop_book_room_type( $args ) {
	_deprecated_function( __FUNCTION__, '2.0' );
	include AWE_function::template_exsits( 'room_type/loop/apb-book' );
}

/**
 * Element result data from ajax select room.
 */
function fill_content_js() {
	if ( isset( $_GET['action'] ) && 'filter_available' == $_GET['action'] ) {
		echo apply_filters( 'apb_fill_list_room_before', '<div class="room-select-js">' );
		do_action( 'layout_room_select' );
		echo apply_filters( 'apb_fill_list_room_after', '</div>' );
	} else {
		echo apply_filters( 'apb_fill_list_room', '<div class="room-select-js"></div>' );
	}
}

/**
 * General field check number of people.
 */
function general_field_check_people( $type = 'multi' ) {
	$total_num = isset( $_GET['room_num'] ) ? absint( $_GET['room_num'] ) : 1;
	$room_adult = isset( $_GET['room_adult'] ) ? wp_kses_post( wp_unslash( $_GET['room_adult'] ) ) : array( 1 );
	$room_child = isset( $_GET['room_child'] ) ? wp_kses_post( wp_unslash( $_GET['room_child'] ) ) : array( 0 );

	echo wp_kses_post( apply_filters( 'apb_general_field_check_people_before', '<div class="list-room">' ) );
	for ( $i = 1; $i <= $total_num; $i++ ) {
		include AWE_function::template_exsits( 'loop/apb-general-field-check-people' );
	}
	echo wp_kses_post( apply_filters( 'apb_general_field_check_people_after', '</div>' ) );
}

/**
 * Element result data from ajax select room.
 */
function get_content_select_room() {
	$_room = 1;
	$total_cart = count( AWE_function::get_cart() );
	$_i = 0;
	if ( is_array( AWE_function::get_cart() ) ) {
		foreach ( AWE_function::get_cart() as $key_item => $item_cart ) {
			if ( isset( $item_cart['room_id'] ) ) {
				$t_i = $_i++;
				$room_info = get_post( $item_cart['room_id'] );
				if ( isset( $_POST['room_adult'][ $t_i ]['adult'] ) ) {
					$total_day = count( AWE_function::range_date( $item_cart['from'], $item_cart['to'] ) ) - 1;
					include AWE_function::template_exsits( 'loop/apb-content-room-select' );
				}
			}
		}
	}
}

/**
 * Before content to javascript.
 */
function apb_room_content_before() {
	include AWE_function::template_exsits( 'room/apb-room_content_before' );
}


/**
 * After content to javascript.
 */
function apb_room_content_after() {
	include AWE_function::template_exsits( 'room/apb-room_content_after' );
}

/**
 * Room selected content.
 * @return void
 */
function loop_content_room_select() {
	$currency = get_option( 'woocommerce_currency' ) ? get_option( 'woocommerce_currency' ) : '$';
	$_room = 1;

	$cart = AWE_function::get_cart();
	// var_dump($cart);
	$total_cart = count( $cart );
	$_i = 0;
	if ( is_array( $cart ) ) {
		foreach ( $cart as $key_item => $item_cart ) {
			if ( isset( $item_cart['room_id'] ) ) {
				$t_i = $_i++;
				$room_info = get_post( $item_cart['room_id'] );
				if ( isset( $_POST['room_adult'][ $t_i ]['adult'] ) ) {
					$total_day = count( AWE_function::range_date( $item_cart['from'], $item_cart['to'] ) ) - 1;
					if ( isset( $_POST['key'] ) && $_POST['key'] == $key_item) {
						?>
						<!-- CURRENT -->
						<div class="apb-room-seleted_current apb-bg_blue">
							<h6><?php printf( esc_html__( 'You are changing room %s', 'awebooking' ), absint( $_POST['num_args'] ) + 1 ); ?></h6>
							<span>
								<?php printf( esc_html__( '%s Adult', 'awebooking' ), $_POST['room_adult'][$_POST['num_args']]['adult'] ); ?>,
								<?php printf( esc_html__( '%s Child', 'awebooking' ), $_POST['room_child'][$_POST['num_args']]['child'] ); ?>
							</span>
						</div>
						<!-- CURRENT -->
						<?php
					} else {
						include AWE_function::template_exsits( 'loop/apb-content-room-select' );
					}
				}
			}
		}
	}
}

function apb_notice_available() {
	include AWE_function::template_exsits( 'loop/apb-notice_check_available' );
}

function checkout_class_js() {
	if ( get_option( 'rooms_checkout_style' ) == 1 ) {
		echo ' add-to-checkout-js';
	} elseif ( get_option( 'rooms_checkout_style' ) == 2 ) {
		echo 'add-to-checkout-js';
	}
}

/**
 * Template Post Type room.
 * @param array $argsData Args.
 */
function loop_single_package( $argsData = array( 'count_day' => 1 ) ) {
	$currency = get_option( 'woocommerce_currency' ) ? get_option( 'woocommerce_currency' ) : '$';
	include AWE_function::template_exsits( 'room/loop/apb-room-single-package' );
}

function loop_price_single( $room_price = '' ) {
	$currency = get_option( 'woocommerce_currency' ) ? get_option( 'woocommerce_currency' ) : '$';
	include AWE_function::template_exsits( 'room/loop/apb-room-single-price' );
}

/**
 * Form check available of post type room
 */
function form_check_available_single() {
	include AWE_function::template_exsits( 'room/apb-form-check-available-single' );
}

function loop_single_book() {
	_deprecated_function( __FUNCTION__, '2.0' );
	include AWE_function::template_exsits( 'room/loop/apb-room-single-book' );
}

function loop_content_list_room() {
	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$room  = new WP_Query( array(
		'post_type'       => 'apb_room_type',
		'posts_per_page'  => get_option( 'limit-page' ),
		'paged'           => $paged,
	) );
	while ( $room->have_posts() ) : $room->the_post();
		$room_price    = get_post_meta( get_the_ID(), 'base_price', true );
		$room_desc     = get_post_meta( get_the_ID(), 'room_desc', true );
		$max_sleeps    = get_post_meta( get_the_ID(), 'max_sleeps', true );
		$min_sleeps    = get_post_meta( get_the_ID(), 'min_sleeps', true );
		$max_children  = get_post_meta( get_the_ID(), 'max_children', true );
		$min_children  = get_post_meta( get_the_ID(), 'min_children', true );
		include AWE_function::template_exsits( 'room/apb-content-list-room' );
	endwhile;
	list_room_pagination( $room );

}

function list_room_pagination( $room ) {
	include AWE_function::template_exsits( 'room/apb-list-room-pagination' );
}

function single_message() {
	_deprecated_function( __FUNCTION__, '2.0' );
	include AWE_function::template_exsits( 'room/apb-room-single-message' );
}

function fill_content_message_single() {
	_deprecated_function( __FUNCTION__, '2.0' );
	echo apply_filters( 'apb_fill_content_message_single', '<div class="apb-notice-body-js panel-body"></div>' );
}

function single_gallery() {
	$room_gallery = get_post_meta( get_the_ID(), 'apb_gallery', true );
	include AWE_function::template_exsits( 'loop/apb-single-gallery' );
}


/* ============================== Template Post Type room_type ==============
 *  Post type room_type layout
 * ======================================================================= */

/**
 * Form check available for post type room_type.
 *
 * @return void
 */
function form_check_available_room_type() {
	_deprecated_function( __FUNCTION__, '2.0' );
	global $wp_query;
	$room_type = AWE_function::get_room_type( isset( $wp_query->query_vars['rt_id'] ) ? $wp_query->query_vars['rt_id'] : 0 );
	?>
	<form class="apb-checkavailable-submit" action="<?php echo esc_url( home_url( '/' ) ) ?>" method="GET">
		<input type="hidden" name="page_id" value="<?php echo absint( AWE_function::get_check_available_page_id() ) ?>" >
		<input type="hidden" name="action" value="filter_available">
		<input type="hidden" name="roomtype" value="<?php echo esc_attr( $room_type[0]->type ) ?>">
		<script>
			var apb_form_type = "shortcode";
		</script>
		<?php include AWE_function::template_exsits( 'room_type/apb-form-check-available-room-type' ); ?>
	</form>
	<?php

}

/**
 * @deprecated 2.0
 */
function apb_loop_content_list_room_type() {
	_deprecated_function( __FUNCTION__, '2.0' );
	$currency       = get_option( 'woocommerce_currency' ) ? get_option( 'woocommerce_currency' ) : '$';
	$list_room_type = AWE_function::get_room_type();

	foreach ( $list_room_type as $room_type ) :
		$info_room_type = get_post( $room_type->object_id );
		$room_desc      = get_post_meta( $room_type->object_id, 'room_desc', true );
		$data           = unserialize( $room_type->data );

		include AWE_function::template_exsits( 'room_type/apb-content-list-room_type' );

	endforeach;
}

function loop_package_room_type( $argsData = array() ) {
	_deprecated_function( __FUNCTION__, '2.0' );
	$argsData['check'] = isset( $argsData['check'] ) ? $argsData['check'] : true;
	$argsData['count_day'] = isset( $argsData['count_day'] ) ? $argsData['count_day'] : 1;
	$get_option = AWE_function::get_room_option( $argsData['roomType_Id'], "room_type");
	if(!empty( $get_option) ) {
	  include AWE_function::template_exsits("room_type/loop/apb-room_type-package");
	}
}
function room_type_single_gallery() {
	_deprecated_function( __FUNCTION__, '2.0' );
	$room_gallery =  get_post_meta(get_the_ID(),"apb_gallery",true);
	include AWE_function::template_exsits("loop/apb-single-gallery");
}
function loop_package_room_type_single( $room_type_id = 0) {
	_deprecated_function( __FUNCTION__, '2.0' );
	$currency     = (get_option( 'rooms_checkout_style' ) == 1) ? get_option( 'woocommerce_currency' ) : get_option( 'woocommerce_currency' );
	$room_gallery =  get_post_meta(get_the_ID(),"apb_gallery",true);
	include AWE_function::template_exsits("room_type/loop/apb-room_type-single-package");
}
/* ============================== Template Check out =====================
 *  Checkout layout
 * ======================================================================= */

function loop_item_cart_info() {
	$currency = get_option( 'woocommerce_currency' ) ? get_option( 'woocommerce_currency' ) : '$';
	$i = 1;
	$total_price = 0;
	$apb_cart = AWE_function::get_cart();

	if ( ! empty( $apb_cart ) ) {
		foreach ( $apb_cart as $item ) {
			$room_num     = $i++;
			$room_id = absint( $item['room_id'] );
			$room = get_post( $room_id );
			$room_type_id = wp_get_post_parent_id( $item['room_id'] );
			$room_type    = get_post( $room_type_id );
			$range_date   = AWE_function::range_date( $item['from'], $item['to'] );
			$total_day    = count( $range_date ) - 1;
			$room_price   = get_post_meta( $room_type->ID, 'base_price', true );

			$extra_adult  = get_post_meta( $room_type->ID, 'extra_adult', true );
			$extra_child  = get_post_meta( $room_type->ID, 'extra_child', true );

			$price = $item['price'];
			$total_price += $price;
			include AWE_function::template_exsits( 'checkout/loop/apb-item_cart_info' );
		}
	}
}


/*================================================================
=            Display layout room current booking detail          =
=================================================================*/

function apb_gen_room_select() {
	ob_start();
	$total_num = isset( $_POST['number'] ) ? absint( $_POST['number'] ) : 3;
	$type = 'multi';
	for ( $i = 1; $i <= $total_num; $i++ ) {
		include AWE_function::template_exsits( 'loop/apb-general-field-check-people' );
	}
	$html = ob_get_clean();
	echo $html;
	die;
	return false;
}
add_action( 'wp_ajax_apb_gen_room_select', 'apb_gen_room_select' );
add_action( 'wp_ajax_nopriv_apb_gen_room_select', 'apb_gen_room_select' );

/*=====  End of Display layout room current booking  ===========*/



/*================================================
=            Render wraper Awebooking            =
================================================*/

function Apb_renderBefore() {
	ob_start();
}

function Apb_renderAfter() {
   printf( '<div id="%s" class="awebooking">%s</div>', apply_filters( 'awe_element_id', 'awe-plugin-booking' ), ob_get_clean() );
}
/*=====  End of Render wraper Awebooking  ======*/



/*==========================================
=            Template shortcode            =
==========================================*/

function apb_shortcode_check_available( $apb_setting = '' ) {
	if ( empty( $apb_setting ) ) {
		$apb_setting = array(
			'departure'     => 1,
			'night'         => 1,
			'room_type'     => 1,
			'mullti_room'   => 1,
			'style'         => 1,
			'tmp_file'      => '',
		);
	}
	if ( empty( $apb_setting['tmp_file'] ) ) {
		include AWE_function::template_exsits( 'shortcode/apb_check_available/apb-shortcode-check-availability' );
	} else {
		include AWE_function::template_exsits( 'shortcode/apb_check_available/' . $apb_setting['tmp_file'] );
	}
}

function apb_get_day_advance() {
	echo '<script>
		 var apb_day_book = "' . esc_js( get_option( 'rooms_booking_start_date' ) ) . '";
	</script>';
}

function apb_shorcode_rooms( $apb_attr = '' ) {
	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$room = new WP_Query( array(
		'post_type'       => 'apb_room_type',
		'posts_per_page'  => get_option( 'limit-page' ),
		'paged'           => $paged,
	) );

	while ( $room->have_posts() ) : $room->the_post();
		$room_price    = get_post_meta( get_the_ID(), 'base_price', true );
		$room_desc     = get_post_meta( get_the_ID(), 'room_desc', true );
		$max_sleeps    = get_post_meta( get_the_ID(), 'max_sleeps', true );
		$min_sleeps    = get_post_meta( get_the_ID(), 'min_sleeps', true );
		$max_children  = get_post_meta( get_the_ID(), 'max_children', true );
		$min_children  = get_post_meta( get_the_ID(), 'min_children', true );
		include AWE_function::template_exsits( 'shortcode/apb_rooms/apb-shortcode-display-rooms' );
	endwhile;
	list_room_pagination( $room );
}

/*=====  End of Template shortcode  ======*/


/*=====================================
=            Template mail            =
=====================================*/

/**
 * Mail for admin when new order.
 *
 * @return void
 * @since 1.11
 */
function apb_mail_new_booking() {
	$i = 1;
	$total_price = 0;
	$apb_cart = AWE_function::get_cart();

	if ( ! empty( $apb_cart ) ) {
		foreach ( $apb_cart as $item ) {
			$room_num     = $i++;
			$room_id = absint( $item['room_id'] );
			$room_type_id = wp_get_post_parent_id( $room_id );
			$room    = get_post( $room_id );
			$range_date   = AWE_function::range_date( $item['from'], $item['to'] );
			$total_day    = count( $range_date ) - 1;
			$room_price   = get_post_meta( $room_type_id, 'base_price', true );

			$extra_adult  = get_post_meta( $room_type_id, 'extra_adult', true );
			$extra_child  = get_post_meta( $room_type_id, 'extra_child', true );

			$price = $item['price'];

			$total_price += $price;
			include AWE_function::template_exsits( 'emails/apb-customer-new-booking' );
		}
	}
}

/**
 * Mail when order is created.
 *
 * @return void
 * @since 1.11
 */
function apb_mail_pending_order() {
	$i = 1;
	$total_price = 0;
	$apb_cart = AWE_function::get_cart();

	if ( ! empty( $apb_cart ) ) {
		foreach ( $apb_cart as $item ) :
			$room_num     = $i++;
			$room_id = absint( $item['room_id'] );
			$room_type_id = wp_get_post_parent_id( $room_id );
			$room = get_post( $room_id );
			$range_date   = AWE_function::range_date( $item['from'], $item['to'] );
			$total_day    = count( $range_date ) - 1;
			$room_price   = get_post_meta( $room_type_id, 'base_price', true );

			$extra_adult  = get_post_meta( $room_type_id, 'extra_adult', true );
			$extra_child  = get_post_meta( $room_type_id, 'extra_child', true );

			$price = $item['price'];
			$total_price += $price;
			include AWE_function::template_exsits( 'emails/apb-customer-pending-order' );
		endforeach;
	}
}

/**
 * Mail when order is completed.
 *
 * @param  int $order_id Order ID.
 * @return void
 * @since 1.0
 */
function apb_mail_complete_order( $order_id = 0 ) {
	$order_data = get_post_meta( $order_id, 'apb_data_order', true );
	$i = 1;
	include AWE_function::template_exsits( 'emails/apb-customer-completed-order' );
}

/**
 * Mail when order is cancelled.
 *
 * @param  int $order_id Order Id.
 * @return void
 * @since 1.0
 */
function apb_mail_cancelled_order( $order_id = 0 ) {
	$order_data = get_post_meta( $order_id, 'apb_data_order', true );
	$i = 1;
	include AWE_function::template_exsits( 'emails/apb-customer-cancel-order' );
}

/*=====  End of Template mail  ======*/

