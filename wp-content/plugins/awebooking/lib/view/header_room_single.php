<?php
/**
 * Header Create and edit room type and manage room
 *
 * @version		1.0
 * @package		AweBooking/admin/
 * @author 		AweTeam
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( isset( $_GET['room_id'] ) ) {
	$post_info = get_post( absint( $_GET['room_id'] ) );
}

if ( isset( $_GET['action'] ) ) {
	switch ( $_GET['action'] ) {
		case 'avb':
			$curent_page = __( 'Manage Availability', 'awebooking' );
			break;

		case 'pricing':
			$curent_page = __( 'Manage Pricing', 'awebooking' );
			break;
	}
}
?>
<div class="breadcrumb">
	<a href="<?php echo esc_url( admin_url( 'edit.php?post_type=apb_room_type' ) ) ?>"><?php esc_html_e( 'Rooms' ) ?></a> »
	<a href="#"><?php echo $curent_page ?></a>
	<?php if ( isset( $post_info ) ) : ?> »
		<a href="#"><?php echo esc_html( $post_info->post_title ); ?></a>
	<?php endif; ?>
</div>

<?php if(isset($post_info)): ?>
	<div class="awe-brand-header">
		<h1 class="page-title"><?php echo $post_info->post_title ?></h1>
		<ul class="tabs primary">
				<li>
						<a href="<?php echo get_permalink($post_info->ID) ?>"><?php _e('View','awebooking') ?></a>
				</li>
				<li>
						<a href="<?php echo admin_url() ?>post.php?post=<?php echo $post_info->ID ?>&action=edit"><?php _e('Edit','awebooking') ?></a>
				</li>
				<li>
						<a <?php if($_GET['action'] == "avb") echo 'class="active"'; ?> href="<?php echo $this->awe_get_link("avb","&room_id=". $_GET['room_id']); ?>"><?php _e('Manage Availability','awebooking') ?></a>
				</li>
				<li>
						<a <?php if($_GET['action'] == "pricing") echo 'class="active"'; ?> href="<?php echo $this->awe_get_link("pricing","&room_id=". $_GET['room_id']); ?>"><?php _e('Manage Pricing','awebooking') ?></a>
				</li>
		</ul>
	</div>
<?php
	else:
				echo '<h1 class="page-title">'.$curent_page.'</h1>';
	endif;
?>
