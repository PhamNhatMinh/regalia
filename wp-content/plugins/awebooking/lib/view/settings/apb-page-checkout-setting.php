<?php
/**
 *  Tempalte setting form check available
 *
 * @version   1.0
 * @package   AweBooking/admin/
 * @author    AweTeam
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<div class="form-elements">
	<p><?php esc_html_e( 'Checkout presentation style', 'awebooking' ); ?></p>
	<div class="form-radios">
		<div class="form-elements">
			<input id="rooms_checkout_style_1" type="radio" <?php checked( 1, get_option( 'rooms_checkout_style' ) ); ?> value="1" name="rooms_checkout_style">
			<label for="rooms_checkout_style_1"><?php esc_html_e( 'WooCommerce checkout', 'awebooking' ) ?> </label><br/>
			<span class="description"><?php esc_html_e( 'Checkout and payment with plugin woocommerce. With checkout woocommerce function, you have to install page checkout of woocommerce', 'awebooking' ); ?></span>
		</div>
		<div class="form-elements">
			<input id="rooms_checkout_style_2" type="radio" <?php checked( 2, get_option( 'rooms_checkout_style' ) ); ?> value="2" name="rooms_checkout_style">
			<label for="rooms_checkout_style_2"><?php esc_html_e( 'Contact form 7 checkout', 'awebooking' ); ?> </label><br/>
			<span class="description"><?php esc_html_e( 'Checkout and send mail info with plugin contact form 7', 'awebooking' ); ?></span>
		</div>

	</div>
</div>

<div class="form-elements">
	<p><?php esc_html_e( 'Shortcode Contact form 7', 'awebooking' ) ?></p>
	<div class="form-radios">
		<div class="form-elements">
			<textarea rows="4" cols="50" name="shortcode_form"><?php echo esc_textarea( wp_unslash( get_option( 'shortcode_form' ) ) ); ?></textarea>
			<br/>
			<span class="description">
				<?php esc_html_e( 'Add shorcode from contact form 7.', 'awebooking' ); ?>
				<?php esc_html_e( 'Required [email* apb-email] and [text* apb-name] to plugin works right.', 'awebooking' ); ?>
			</span>
		</div>
	</div>
</div>
