<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 *  Manage List room type
 *
 * @version		1.0
 * @package		AweBooking/admin/
 * @author 		AweTeam
 */


?>
<div id="awe-booking-wraper" class="awe-booking-wraper">
<div class="awe-plugin">
	<div class="clearfix" id="branding">
		<div class="awe-brand-header">
			<h1 class="page-title"><?php _e('Room management','awebooking') ?></h1>
			<ul class="tabs primary">
				<li><a class="active" href=""><?php _e('Room Type','awebooking') ?></a></li>
				<li><a href="<?php echo admin_url('edit.php?post_type=apb_room_type_type') ?>"><?php _e('Room Type Intro','awebooking') ?></a></li>
			</ul>
		</div>
	</div>
	<div id="page">
		<div class="tabs-secondary clearfix"></div>

		<div class="clearfix" id="content">

			<div class="element-invisible"><a id="main-content"></a></div>
			<ul class="action-links">
				<li><a href="<?php echo admin_url('edit.php?post_type=apb_room_type&page=room-type.php&action=add_room_type') ?>"><i class="fa fa-plus"></i> <?php _e('Add Room Type','awebooking') ?></a></li>
			</ul>
			 <?php
			 echo AWE_function::apb_getStateFlash('error_delete')."<br/>";
			 unset($_SESSION['error_delete']);
			?>
			<div class="region region-content">
				<div class="block block-system" id="block-system-main">
					<div class="content">
						<form accept-charset="UTF-8" id="rooms-unit-type-overview-form" method="post" action="">
							<div>
								<table class="sticky-header" >
									<thead>
										<tr>
											<th><?php _e('Label','awebooking') ?></th>

											<th colspan="6"><?php _e('Action','awebooking') ?></th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div id="footer"> </div>
	</div>
</div>
</div>

