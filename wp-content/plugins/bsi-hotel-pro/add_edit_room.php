<?php 
if(isset($_POST['submitRoom'])){  
	include("includes/conf.class.php");
	include("includes/admin.class.php");
	$bsiAdminMain->add_edit_room();
	header("location:admin.php?page=Room-Manager");	
	exit;
}

include("includes/conf.class.php");
include("includes/admin.class.php");
global $wpdb;
if(isset($_GET['rid']) && $_GET['rid'] != ""){
	$rid = $bsiCore->ClearInput($_GET['rid']);
	$cid = $bsiCore->ClearInput($_GET['cid']);
	if($rid != 0 && $cid != 0){
		$row = $wpdb->get_row($bsiAdminMain->getRoomsql($rid, $cid),ARRAY_A);
		$rowrt = $wpdb->get_row($bsiAdminMain->getRoomtypesql($row['roomtype_id']),ARRAY_A);
		$rowca = $wpdb->get_row($bsiAdminMain->getCapacitysql($row['capacity_id']),ARRAY_A);
		$roomtypeCombo = $rowrt['type_name'].'<input type="hidden" name="roomtype_id" value="'.$row['roomtype_id'].'" />';
		$capacityCombo = $rowca['title'].'<input type="hidden" name="capacity_id" value="'.$row['capacity_id'].'" />';
	}else{
		$row = NULL;
		$roomtypeCombo = $bsiAdminMain->generateRoomtypecombo();
		$capacityCombo = $bsiAdminMain->generateCapacitycombo();
	}
}else{
	header("location:admin.php?page=Room-Manager");
	exit;
}
?>  
      <p>&nbsp;</p>
      <div id="container-inside">
      <span style="font-size:16px; font-weight:bold"><?php echo ROOM_ADD_AND_EDIT;?></span>
<input type="button" value="<?php echo BACK_TO_ROOM_LIST;?>" onClick="window.location.href='admin.php?page=Room-Manager'" style="background: #EFEFEF; float:right"/>
     <hr style="margin-top:10px;" />
        <form action="<?php echo admin_url('admin.php?page=aad-new-room&noheader=true'); ?>" method="post" id="form1"> 
          <table cellpadding="5" cellspacing="2" border="0"> 
            <tr>
              <td><strong><?php echo NUMBER_OF_ROOM;?>:</strong></td> 
              <td valign="middle"><input type="text" name="no_of_room" id="no_of_room" class="required digits" value="<?php echo $row['NoOfRoom']?>" style="width:50px;" /> &nbsp;&nbsp;EXAMPLE: 1, 2</td><input type="hidden" name="pre_room_cnt" value="<?php echo $row['NoOfRoom']?>" />
            </tr>
            <tr>
              <td><strong><?php echo ROOM_TYPE_ADD_EDIT;?>:</strong></td>
              <td><?php echo $roomtypeCombo;?></td>
            </tr>
            <tr>
              <td><strong><?php echo NO_OF_ADULT;?>:</strong></td>
              <td><?php echo $capacityCombo;?></td>
            </tr>
             <tr>
              <td><strong><?php echo MAX_CHILD_PER_ROOM;?>:</strong></td>
              <td><input type="text" name="child_per_room" value="<?php echo $row['no_of_child']; ?>" style="width:40px;"/> (leave blank if None!)</td>
            </tr>
            <!--<tr>
              <td></td>
              <td><a onclick="return false;" title="Upload image" class="thickbox" id="add_image" href="media-upload.php?type=image&amp;TB_iframe=true&amp;width=640&amp;height=105">Upload Image</a></td>
            </tr>-->
            <tr>
              <td></td>
              <td><input type="submit" value="<?php echo SUBMIT;?>" name="submitRoom" style="background:#e5f9bb; cursor:pointer; cursor:hand;" /></td>
            </tr>
          </table>
        </form>
      </div>
<script type="text/javascript">
	jQuery().ready(function() {
		jQuery("#form1").validate();	
     });        
</script>