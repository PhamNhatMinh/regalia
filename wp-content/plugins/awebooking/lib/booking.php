<?php
/**
 * APB_Booking Autoloader.
 *
 * @class 		APB_Booking
 * @version		1.0
 * @package		AweBooking/Classes/
 * @author 		AweTeam
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

final class APB_Booking {

	static private $instance = NULL;

	static public function getInstance() {
		self::$instance = new APB_Booking();
		self::table_exists();
		self::lib_function();
		self::autoload();
		self::scripts();
		self::post_type();
		self::manage_admin();
		self::controller();
		self::process_ajax();
		self::apb_add_to_cart();
		self::template_func();
		self::apb_wc_setting();
		// self::apb_duplicate();
		self::apb_shortcode();
		self::apb_meta_boxes();
		self::apb_widget();
		self::apb_emails();
		self::apb_import_export();
		self::apb_emogrifier();
		return self::$instance;
	}

	static public function table_exists() {
		include 'apb-database-exits.php';
		return new database_exits();
	}

	static public function lib_function() {
		include 'apb-lib-function.php';
	}
	static public function controller() {
		include 'apb-controller.php';
		return new AWE_Controller();
	}
	static public function manage_admin() {
		include 'apb-backend.php';
		return new AWE_backend();
	}
	static public function post_type() {
		include 'apb-post-type.php';
		return new AWE_Post_type();
	}
	static public function scripts() {
		include 'apb-scripts.php';
	}
	static public function process_ajax() {
		include 'apb-process-ajax.php';
		return new Process_ajax();
	}
	static public function template_func() {
		include 'apb-template.php';
		return new AWE_Template();
	}
	static public function apb_wc_setting() {
		include 'apb-wc-setting.php';
		return new Apb_Wc_Setting();
	}
	static public function apb_shortcode() {
		include 'apb-shortcode.php';
		return new Apb_shortcode();
	}
	static public function apb_meta_boxes() {
		include 'apb-meta-boxes.php';
		return new Apb_meta_boxes();
	}
	static public function apb_duplicate() {
		_deprecated_function( __FUNCTION__, '2.0' );
		include 'apb-duplicate-room.php';
		return new Apb_Admin_Duplicate();
	}
	static public function autoload() {
		include 'apb-autoload.php';
		return new Apb_autoload();
	}
	static public function apb_widget() {
		include 'apb-widget.php';
	}
	static public function apb_import_export() {
		include 'apb-import-export.php';
	}
	static public function apb_add_to_cart() {
		include 'apb-class-add-cart.php';
	}
	static public function apb_emails() {
		include 'apb-class-email.php';
	}
	static public function apb_emogrifier() {
		include 'apb-class-emogrifier.php';
	}
}

function begin_plugin() {
	return APB_Booking::getInstance();
}
begin_plugin();
