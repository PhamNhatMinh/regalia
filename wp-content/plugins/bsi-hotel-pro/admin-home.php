<?php 
include("includes/conf.class.php");	
include("includes/admin.class.php");
//includeing the js files
//wp_enqueue_script( 'custom_script10', plugins_url().'/bsi-hotel-pro/js/bsi_datatables.js');

wp_enqueue_script( 'custom_script11', plugins_url().'/bsi-hotel-pro/js/DataTables/jquery.dataTables.js');
wp_enqueue_style('custom-style9', plugins_url().'/bsi-hotel-pro/css/data.table.css');
wp_enqueue_style('custom-style10', plugins_url().'/bsi-hotel-pro/css/jqueryui.css');

?>    <br />
<br />
  
        <div id="container-inside">
        <span style="font-size:16px; font-weight:bold"><?=LAST_10_BOOKING?></span>
     	 <hr />
          <table class="display datatable" border="0" id="test5">
         <?=$bsiAdminMain->homewidget(1)?>
        </table>
         <br />
         
         <span style="font-size:16px; font-weight:bold"><?=TODAY_CHECK_IN?></span>
      	 <hr />
          <table class="display datatable" border="0">
         <?=$bsiAdminMain->homewidget(2)?>
        </table>
         <br />
         
         <span style="font-size:16px; font-weight:bold"><?=TODAY_CHECK_OUT?></span>
         <hr />
          <table class="display datatable" border="0">
        <?=$bsiAdminMain->homewidget(3)?>
        </table>
         <br />
         
        </div>
<!--<script type="text/javascript" src="js/DataTables/jquery.dataTables.js"></script>
-->
<script>
jQuery(document).ready(function() {
	 	var oTable = jQuery('.datatable').dataTable( {
				 "responsive": true,
				"bJQueryUI": true,
				"sScrollX": "",
				"bSortClasses": false,
				"aaSorting": [[1,'asc']],
				"bAutoWidth": true,
				"bInfo": true,
				"sScrollY": "100%",	
				"sScrollX": "100%",
				"bScrollCollapse": true,
				"sPaginationType": "full_numbers",
				"bRetrieve": true,
				"oLanguage": {
								"sSearch": "Search:",
								"sInfo": "Showing _START_ to _END_ of _TOTAL_ entries",
								"sInfoEmpty": "Showing 0 to 0 of 0 entries",
								"sZeroRecords": "No matching records found",
								"sInfoFiltered": "(filtered from _MAX_ total entries)",
								"sEmptyTable": "No data available in table",
								"sLengthMenu": "Show _MENU_ entries",
								"oPaginate": {
												"sFirst":    "First",
												"sPrevious": "Previous",
												"sNext":     "Next",
												"sLast":     "Last"
											  }
							 }
	} );
} );
</script> 
