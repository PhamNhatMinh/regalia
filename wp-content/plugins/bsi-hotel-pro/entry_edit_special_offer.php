<?php 

if(isset($_POST['sbt_details'])){
	include("includes/conf.class.php");
    include("includes/admin.class.php");
	$bsiAdminMain->add_edit_spe_off();
	header("location:admin.php?page=special-offer");
	exit;
}


include("includes/conf.class.php");
include("includes/admin.class.php");
 if(isset($_GET['id']))
 {
   $id=$_GET['id'];
   if($id!='0')
   {
	  $row1 = $bsiAdminMain->edit_view1();
   }else{
	   $row1 =null;
   }
   
 }
wp_enqueue_style('jquery-style', plugins_url().'/bsi-hotel-pro/front/css/datepicker.css');
	
	wp_enqueue_script( 'custom_script10', plugins_url().'/bsi-hotel-pro/js/bsi_datatables.js');
		wp_enqueue_script( 'custom_script11', plugins_url().'/bsi-hotel-pro/js/DataTables/jquery.dataTables.js');
		wp_enqueue_style('custom-style9', plugins_url().'/bsi-hotel-pro/css/data.table.css');
		wp_enqueue_style('custom-style10', plugins_url().'/bsi-hotel-pro/css/jqueryui.css');
?>
<div id="container-inside">
<span style="font-size:16px; font-weight:bold"><?php echo ADD_EDIT_SPECIAL_OFFER; ?></span>
<hr />
<div id="container-inside">
  <form  id="form1" ame="form1" action="<?php echo admin_url('admin.php?page=entry-edit-special-offer&noheader=true'); ?>" method="post">
    <input type="hidden" value="<?php echo $id; ?>" name="id"  />
    <table cellpadding="5" cellspacing="2" border="0">
     <tr>
        <td valign="middle"><strong><?php echo OFFER_NAME_TEXT; ?>:</strong></td>
        <td><input type="text" id="offer_name" name="offer_name"  class="required" size="35" value="<?php echo $row1['offer_title'];?>" /></td> 
      </tr>
      <tr>
        <td valign="middle"><strong><?php echo START_DATE;?>:</strong></td>
        <td><input type="text" id="txtFromDate" name="fromDate" class="required" size="10" value="<?php echo $row1['start_date'];?>" />
          <a id="datepickerImage" href="javascript:;"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/images/month.png" height="16px" width="16px" style=" margin-bottom:-4px;" border="0" /></a></td>
      </tr>
      <tr>
        <td valign="middle"><strong><?php echo END_DATE;?>:</strong></td>
        <td><input type="text" id="txtToDate" name="toDate" class="required" size="10" value="<?php echo $row1['end_date'];?>"/>
          <a id="datepickerImage1" href="javascript:;"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/images/month.png" height="18px" width="18px" style=" margin-bottom:-4px;" border="0" /></a></td>
      </tr>
      <tr>
        <td valign="middle"><strong><?php echo ROOM_TYPE;?>:</strong></td>
        <td><?php echo $select_rtype=$bsiAdminMain->getRoomtype2($row1['room_type']); ?></td>
      </tr>
      <tr>
        <td valign="middle"><strong><?php echo PRICE_DEDUCTED;?>:</strong></td>
        <td><input type="text" id="pr_de" name="pr_de"  class="required number"  style="width:40px;" value="<?php echo $row1['price_deduc'];?>" />
          <?php echo "%";?></td>
      </tr>
      <tr>
        <td valign="middle"><strong><?php echo MINIMUM_STAY_OPTIONAL;?>:</strong></td>
        <td><input type="text" id="min_sty" name="min_sty"  size="10" value="<?php echo $row1['min_stay'];?>" />
          <?php echo NIGHTS;?> <span style="color:#900">(<?php echo LEAVE_BLANK_IF_NO_MINIMUM_NIGHT_RESTRICTION; ?>)</span></td>
      </tr>
      <tr>
        <td></td>
        <td><input type="submit" value="<?php echo SPECIAL_SUBMIT; ?>" name="sbt_details"  id="sbt_details"  style="background:#e5f9bb; cursor:pointer; cursor:hand;"   /></td>
      </tr>
    </table>
  </form>
</div>

<script type="text/javascript" charset="">
   jQuery(document).ready(function() {
   jQuery("#priceplanaddeit").validate();
   jQuery('#roomtype_id').change(function() {
         if(jQuery('#roomtype_id').val() != 0){
			var querystr = 'actioncode=3&roomtype_id='+jQuery('#roomtype_id').val();		
			jQuery.post("admin_ajax_processor.php", querystr, function(data){												 
				if(data.errorcode == 0){
					 jQuery('#default_capacity').html(data.strhtml)
				}else{
				    jQuery('#default_capacity').html("<span style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px;\">'<?php  echo NOT_FOUND;?>'</span>")
				}
			}, "json");
		} else {
		 jQuery('#default_capacity').html("<span style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px;\">'<?php echo PLEASE_SELECT_ROOMTYPE_FROM_DROPDOWN_ALERT; ?>'</span>")
		}
	});
	
	if(jQuery('#roomtype').val() == 0){
		jQuery('#default_capacity').html("<span style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px;\">'<?php echo PLEASE_SELECT_ROOMTYPE_FROM_DROPDOWN_ALERT; ?>'</span>")
	}
});
 
jQuery(document).ready(function(){
	jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ "en" ] );
	jQuery.datepicker.setDefaults({ dateFormat: '<?=$bsiCore->config['conf_dateformat']?>' });
    jQuery("#txtFromDate").datepicker({
        minDate: 0,
        maxDate: "+<?php echo $bsiCore->config['conf_maximum_global_years']; ?>D",
        numberOfMonths: 2,
        onSelect: function(selected) {
        var date = jQuery(this).datepicker('getDate');
        if(date){
            date.setDate(date.getDate() + <?=$bsiCore->config['conf_min_night_booking']?>);
        }
          jQuery("#txtToDate").datepicker("option","minDate", date)
        }
    });
    jQuery("#txtToDate").datepicker({ 
        minDate: 0,
        maxDate:"+<?php echo $bsiCore->config['conf_maximum_global_years']; ?>D",
        numberOfMonths: 2,
        onSelect: function(selected) {
           jQuery("#txtFromDate").datepicker("option","maxDate", selected)
        }
    });
	
	jQuery("#txtFromDate").datepicker();
	jQuery("#datepickerImage").click(function() { 
		jQuery("#txtFromDate").datepicker("show");
	});
	
	jQuery("#txtToDate").datepicker();
	jQuery("#datepickerImage1").click(function() { 
		jQuery("#txtToDate").datepicker("show");
	});    
});
</script> 
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#form1").validate();
     });  
</script> 
