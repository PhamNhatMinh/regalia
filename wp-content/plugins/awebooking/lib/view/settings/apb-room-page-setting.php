<?php
/**
 *  Tempalte setting page setting
 *
 * @version		1.0
 * @package		AweBooking/admin/
 * @author 		AweTeam
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="form-elements">
	<p><?php esc_html_e( 'Check Availability', 'awebooking' ) ?></p>
	<?php AWE_function::select_page( 'check_avb', AWE_function::get_check_available_page_id() ); ?>
	<span class="description">
		<?php esc_html_e( 'Selected page to display check availability form.', 'awebooking' ) ?>
	</span>
</div>

<div class="form-elements">
	<p><?php esc_html_e( 'List Room', 'awebooking' ) ?></p>
	<?php AWE_function::select_page( 'list_room', AWE_function::get_list_room_page_id() ); ?>
	<span class="description">
		<?php esc_html_e( 'Selected page to display all rooms.', 'awebooking' ) ?>
	</span>
</div>

<div class="form-elements">
	<p><?php esc_html_e( 'Checkout', 'awebooking' ); ?></p>
	<?php AWE_function::select_page( 'apb_checkout', AWE_function::get_checkout_page_id() ); ?>
	<span class="description">
		<?php esc_html_e( 'Selected page to display checkout form.', 'awebooking' ) ?>
	</span>
</div>

<div class="form-elements">
	<a class="apb-btn" href="<?php echo esc_url( admin_url( 'edit.php?post_type=apb_room_type&page=rooms.php&action=intallpage' ) ); ?>"><?php esc_html_e( 'Install Pages', 'awebooking' ) ?> </a>

	<span class="description">
		<?php esc_html_e( 'Install page default.', 'awebooking' ) ?>
	</span>
</div>

