<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * AWE Duplicate Room
 *
 * @class 		Apb_Admin_Duplicate
 * @version		1.0
 * @package		AweBooking/Classes/
 * @author 		AweTeam
 * @deprecated 2.0
 */
class Apb_Admin_Duplicate extends AWE_function{
    /**
	 * Constructor
	 */
	public function __construct() {
        add_action( 'admin_action_duplicate_room', array( $this, 'duplicate_room_action' ) );
		add_filter( 'post_row_actions', array( $this, 'dupe_link' ), 10, 2 );
		add_filter( 'page_row_actions', array( $this, 'dupe_link' ), 10, 2 );

	}
     /**
	 * Show the "Duplicate" link in admin products list
	 * @param  array   $actions
	 * @param  WP_Post $post Post object
	 * @return array
	 */
	public function dupe_link( $actions, $post ) {

		if ( $post->post_type != 'apb_room_type' ) {
			return $actions;
		}
		$actions['duplicate'] = '<a href="' . wp_nonce_url( admin_url( 'edit.php?post_type=apb_room_type&action=duplicate_room&amp;post=' . esc_attr($post->ID) ), 'apb-duplicate-room_' . esc_attr($post->ID) ) . '" title="' . __( 'Make a duplicate from this room', 'awebooking' )
			. '" rel="permalink">' .  __( 'Duplicate', 'awebooking' ) . '</a>';

		return $actions;
	}
        /**
	 * Duplicate a room action.
	 */
	public function duplicate_room_action() {

		if ( empty( $_REQUEST['post'] ) ) {
			wp_die( __( 'No room to duplicate has been supplied!','awebooking' ) );
		}

		// Get the original page
		$id = isset( $_REQUEST['post'] ) ? absint( $_REQUEST['post'] ) : '';

		check_admin_referer( 'apb-duplicate-room_' . $id );

		$post = $this->get_room_to_duplicate( $id );

		// Copy the page and insert it
		if ( ! empty( $post ) ) {
			$new_id = $this->duplicate_room( $post );

			// If you have written a plugin which uses non-WP database tables to save
			// information about a page you can hook this action to dupe that data.
			do_action( 'apb_duplicate_room', $new_id, $post );

			// Redirect to the edit screen for the new draft page
			wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_id ) );
			exit;
		} else {
			wp_die( __( 'Room creation failed, could not find original room:', 'awebooking' ) . ' ' . $id );
		}
	}


        /**
	 * Get a room from the database to duplicate
	 *
	 * @param mixed $id
	 * @return WP_Post|bool
	 * @todo Returning false? Need to check for it in...
	 * @see duplicate_product
	 */
	private function get_room_to_duplicate( $id ) {
		global $wpdb;

		$id = absint( $id );

		if ( ! $id ) {
			return false;
		}

		$post = $wpdb->get_results( "SELECT * FROM $wpdb->posts WHERE ID=$id" );

		if ( isset( $post->post_type ) && $post->post_type == "revision" ) {
			$id   = $post->post_parent;
			$post = $wpdb->get_results( "SELECT * FROM $wpdb->posts WHERE ID=$id" );
		}

		return $post[0];
	}

        /**
	 * Function to create the duplicate of the room.
	 *
	 * @param mixed $post
	 * @param int $parent (default: 0)
	 * @param string $post_status (default: '')
	 * @return int
	 */
	public function duplicate_room( $post, $parent = 0, $post_status = '' ) {
		global $wpdb;

		$new_post_author    = wp_get_current_user();
		$new_post_date      = current_time( 'mysql' );
		$new_post_date_gmt  = get_gmt_from_date( $new_post_date );

		if ( $parent > 0 ) {
			$post_parent        = $parent;
			$post_status        = $post_status ? $post_status : 'publish';
			$suffix             = '';
		} else {
			$post_parent        = $post->post_parent;
			$post_status        = $post_status ? $post_status : 'draft';
			$suffix             = ' ' . __( '(Copy)', 'awebooking' );
		}

		// Insert the new template in the post table
		$wpdb->insert(
			$wpdb->posts,
			array(
				'post_author'               => $new_post_author->ID,
				'post_date'                 => $new_post_date,
				'post_date_gmt'             => $new_post_date_gmt,
				'post_content'              => $post->post_content,
				'post_content_filtered'     => $post->post_content_filtered,
				'post_title'                => $post->post_title . $suffix,
				'post_excerpt'              => $post->post_excerpt,
				'post_status'               => $post_status,
				'post_type'                 => $post->post_type,
				'comment_status'            => $post->comment_status,
				'ping_status'               => $post->ping_status,
				'post_password'             => $post->post_password,
				'to_ping'                   => $post->to_ping,
				'pinged'                    => $post->pinged,
				'post_modified'             => $new_post_date,
				'post_modified_gmt'         => $new_post_date_gmt,
				'post_parent'               => $post_parent,
				'menu_order'                => $post->menu_order,
				'post_mime_type'            => $post->post_mime_type
			)
		);

		$new_post_id = $wpdb->insert_id;

		// Copy the meta information
		$this->duplicate_post_meta( $post->ID, $new_post_id );

		return $new_post_id;
	}

        /**
	 * Copy the meta information of a post to another post
	 *
	 * @param mixed $id
	 * @param mixed $new_id
	 */
	private function duplicate_post_meta( $id, $new_id ) {
		global $wpdb;

		$post_meta_infos = $wpdb->get_results( $wpdb->prepare( "SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=%d AND meta_key NOT IN ( 'total_sales' );", absint( $id ) ) );
                $data_option = AWE_function::get_room_option($id,'apb_room_type');

		if ( count( $post_meta_infos ) != 0 ) {
            /*
             * Insert package
             */
             if(!empty($data_option)){
                 foreach ($data_option as $item_option){
                      $wpdb->insert(
                      $wpdb->prefix . 'apb_booking_options',
                      array(
                          "entity_type" 	=> 'apb_room_type',
                          "object_id" 		=> $new_id,
                          "total" 			=> 1,
                          "option_name" 		=> $item_option->option_name,
                          "option_operation" 	=>$item_option->option_operation,
                          "option_value" 		=> $item_option->option_value,
                          "option_desc" 		=> $item_option->option_desc,
                          'status'			 => 'draf'
                        ),
                       array('%s','%d','%d','%s','%s','%d','%s')
                   );
                 }
             }

             /*
              * Insert meta
              */
			$sql_query_sel = array();
			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";

			foreach ( $post_meta_infos as $meta_info ) {
				$sql_query_sel[]= $wpdb->prepare( "SELECT %d, %s, %s", $new_id, $meta_info->meta_key, $meta_info->meta_value );
			}

			$sql_query.= implode( " UNION ALL ", $sql_query_sel );
			$wpdb->query($sql_query);
		}
	}
}


