<?php
/**
 * The template for displaying check out cf7
 *
 * Override this template by copying it to your theme
 *
 * @author  AweTeam
 * @package AweBooking/Templates
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<section class="section-checkout">
	<?php do_action( 'layout_loading' ); ?>
	<div class="container">
		<div class="checkout">
			<div class="row">
				<div class="col-md-6">
					<div class="checkout_head">
						<h3><?php esc_html_e( 'Booking Checkout', 'awebooking' ); ?></h3>
					</div>
					<?php echo do_shortcode( apply_filters( 'the_content', stripslashes( get_option( 'shortcode_form' ) ) ) ); ?>
				</div>
			</div>
		</div>
	</div>
</section>
