 <?php 
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 *  Detail room available
 *
 * @version		1.0
 * @package		AweBooking/admin/
 * @author 		AweTeam
 */
$_date_curent = date_create(date('Y-m-d'));
date_add($_date_curent, date_interval_create_from_date_string((isset($_GET['mon']) ? $_GET['mon'] : '0').' month'));
$new_date = date_format($_date_curent, 'Y-m-d');

$month_curent = isset($_GET['mon']) ? $_GET['mon']+2 : 2;
$month_sub = isset($_GET['mon']) ? $_GET['mon']-2 : '-2';

if(date('m',strtotime($new_date)) == 12){
	$_year = date('Y',strtotime($new_date))+1;
}else{
	 $_year = date('Y',strtotime($new_date));
}

$btn_status_url = explode('&apb_stauts',$_SERVER['REQUEST_URI']);
?>
<div id="awe-booking-wraper" class="awe-booking-wraper">
<div class="awe-plugin">
   <div class="clearfix" id="branding">       
	<?php do_action("header_room_single"); ?>
   </div>
	<div id="page">
		<div class="tabs-secondary clearfix"></div>

		<div class="clearfix" id="content">
			<div class="element-invisible"><a id="main-content"></a></div>

			<div class="region region-content">
				<div class="update-pricing">
					<div class="awe-form-item">
						<input id="awe_datepicker_start" name="rooms_start_date" placeholder="From Date" name="" type="text" class="form-text date-start-js">
					</div>
					<div class="awe-form-item">
						<input id="awe_datepicker_end" name="rooms_end_date" placeholder="To Date" type="text" class="form-text date-end-js">
					</div>
					 <div class="awe-form-item">
						<label><?php _e("Sun", 'awebooking') ?></label> <input name="day_options[]" value="Sunday" type="checkbox" class="form-text get_day_js">
						<label><?php _e("Mon", 'awebooking') ?></label> <input name="day_options[]" value="Monday" type="checkbox" class="form-text get_day_js">
						<label><?php _e("Tue", 'awebooking') ?></label> <input name="day_options[]" value="Tuesday" type="checkbox" class="form-text get_day_js">
						<label><?php _e("Wed", 'awebooking') ?></label> <input name="day_options[]" value="Wednesday" type="checkbox" class="form-text get_day_js">
						<label><?php _e("Thu", 'awebooking') ?></label> <input name="day_options[]" value="Thursday" type="checkbox" class="form-text get_day_js">
						<label><?php _e("Fri", 'awebooking') ?></label> <input name="day_options[]" value="Friday" type="checkbox" class="form-text get_day_js">
						<label><?php _e("Sat", 'awebooking') ?></label> <input name="day_options[]" value="Saturday" type="checkbox" class="form-text get_day_js">
					</div>
					<div class="awe-form-item">
						<select class="form-select" name="unit_state">
							<option value="1"><?php _e("Unavailable", 'awebooking') ?></option>
							<option value="2"><?php _e("Available", 'awebooking') ?></option>
						</select>
					</div>
					<div class="awe-button-margin">
						<button type="button" class="awe-avb-js button"><?php _e("Update Availability", 'awebooking') ?></button>
						<div class="btn-update-status-js" style="display:none;">
							<p class="description">(*) <?php _e( 'Displaying orders can be affected by changing  room status in the date range that you selected. You should change room status only after deleting orders in the selected date range.', 'awebooking' ); ?></p><br/>
							<button data-value="yes" class="button apb-update-status-js"><?php _e( 'Yes', 'awebooking' ); ?></button> 
							<button data-value="no" class="button apb-update-status-js"><?php _e( 'No', 'awebooking' ); ?></button>
							<span class="spinner" style="display: none;"></span>
						</div>
					</div>

				</div>
				<div class="awe-block-calendar block block-system">
					<div class="button-status">
						<a href="<?php echo esc_url($btn_status_url[0].'&apb_stauts=apb-all'); ?>" class="button btn-status-js"> <?php _e( 'All Status', 'awebooking' ); ?></button>
						<a href="<?php echo esc_url($btn_status_url[0].'&apb_stauts=apb-available');?>" style="background: <?php echo get_option('color-available') ?>; color: white;"  class="button btn-status-js"><?php _e( 'Available', 'awebooking' ); ?></a>
						<a href="<?php echo esc_url($btn_status_url[0].'&apb_stauts=apb-unavailable'); ?>" style="background: <?php echo get_option('color-unavailable') ?>; color: white;" class="button btn-status-js"><?php _e( 'Unavailable', 'awebooking' ); ?></a>
						<a href="<?php echo esc_url($btn_status_url[0].'&apb_stauts=apb-pending'); ?>" style="background: <?php echo get_option('color-pending') ?>; color: white;"  class="button btn-status-js"><?php _e( 'Pending', 'awebooking' ); ?></a>
						<a href="<?php echo esc_url($btn_status_url[0].'&apb_stauts=apb-completed'); ?>" style="background: <?php echo get_option('color-complate') ?>; color: white;" class="button btn-status-js"><?php _e( 'Completed', 'awebooking' ); ?></a>
					</div>
					<div class="month">
					<?php 
						$next = $this->awe_get_link("avb","&mon=".$month_curent ."&year=".(date('Y', strtotime($new_date)))."&room_id=".$_GET['room_id']); 
						$prew = $this->awe_get_link("avb","&mon=".$month_sub ."&year=".(date('Y', strtotime($new_date)))."&room_id=".$_GET['room_id']); 
					?>
						<a href="<?php echo $prew ?>" class="button btn-left" type="button"><?php _e('Prev','awebooking') ?></a>
						<div id="calendar"> </div>  
					</div>
					<div class="month">
						<a href="<?php echo $next ?>" class="button btn-right" type="button"><?php _e('Next','awebooking') ?></a>
						<div id="calendar2"> </div> 
					</div>
				</div>

				<script>
					var awe_date_curent_1   = '<?php echo date('Y', strtotime($new_date)); ?>-<?php echo date('m', strtotime($new_date));?>';
					var awe_date_curent_2   = '<?php echo $_year; ?>-<?php echo date('m',strtotime(date('Y', strtotime($new_date))."-".(date('m', strtotime($new_date))+1)));?>';
					var awe_start_date      = '<?php echo date('m', strtotime($new_date));?>/01/<?php echo date('Y', strtotime($new_date)); ?>';
					var awe_end_date        = '<?php echo date('m',strtotime(date('Y', strtotime($new_date))."-".(date('m', strtotime($new_date))+1)));?>/01/<?php echo $_year ?>';
					var room_id             = <?php echo wp_kses($_GET['room_id'], '') ?>;
					var room_status         = '<?php echo isset($_GET['apb_stauts']) ? $_GET['apb_stauts'] : "apb-all"; ?>';
				</script>                           
			</div>
		</div>
	</div>
	<div class="box-select-room-type-js">

		<div class="table-update-status-single">

			<div class="box-header"><?php _e('Event Management','awebooking') ?></div>

			<div class="box-content">
				<table class="box-update-status-single">
					<tbody>
						<tr class="odd">
							<td>
								<div>
									<?php 
									   $post_info = isset($_GET['room_id']) ? get_post($_GET['room_id']) : 0;
									?>
									<h2><?php echo $post_info->post_title ?></h2>
									<div class="event-details-js"></div>
										 <input type="hidden" name="popup_rooms_start_date">
										 <input type="hidden" name="popup_rooms_end_date">
										<div class="form-elements">
											<label><?php _e('Change the state for this event to:','awebooking') ?>  </label>
											<select class="form-select" name="popup_unit_state">
												 <option>-- <?php _e('Select','awebooking') ?> --</option>
												 <option value="1"><?php _e('Unavailable','awebooking') ?></option>
												<option value="2"><?php _e('Available','awebooking') ?></option>
												<!-- <option value="3"><?php _e('Pending','awebooking') ?></option>
												<option value="0"><?php _e('Complete','awebooking') ?></option> -->
											</select>
											
									   
											<div class="btn-popup-update-status-js" style="display:none;">
												<p class="description">(*) <?php _e( 'Displaying orders can be affected by changing  room status in the date range that you selected. You should change room status only after deleting orders in the selected date range.', 'awebooking' ); ?></p><br/>
												<button data-value="yes" class="button apb-popup-update-status-js"><?php _e( 'Yes', 'awebooking' ); ?></button> 
												<button data-value="no" class="button apb-popup-update-status-js"><?php _e( 'No', 'awebooking' ); ?></button>
												<span class="description change_status_notice_js"></span>
											</div>
											
										</div>
										<p><a class="apb-add-booking" href="#"><?php _e( 'Created Booking', 'awebooking' ); ?></a> <i class="apb-text-loading" style="display:none;"><?php _e( 'Loading...', 'awebooking' ); ?></i></p>
										<p class="notice-check-avb-single-js" style="display:none;"></p>
										<div class="options_group add-booking-single-form-js" style="display:none;">
											
										   <p class="awe-form-inline">
												<label for="edit-label"><?php _e("Customer",'awebooking') ?></label><br/>
												<select class="form-select awe-select" name="custommer">
												  <?php foreach(get_users() as $user): ?>
														 <option  value="<?php echo esc_attr($user->data->ID); ?>"><?php echo $user->data->user_login; ?></option>
												  <?php endforeach; ?>
										   </select>
											</p>
											<p class="awe-form-inline">
												<label for="edit-label"><?php _e("Adult",'awebooking') ?></label><br/>
											   <?php
												/*
												 * General selected 
												 */
												   AWE_function::apb_get_option_to_selected(
														   array(
															   'name'      => 'room_adult',
															   'count_num' => 10,
															   'data'      => array('class' => 'form-select awe-select'),
															   'select'  =>  ""
															   )
														   );
												  ?>  
											</p>
											 <p class="awe-form-inline">
												<label for="edit-label"><?php _e("Child",'awebooking') ?></label><br/>
												 <?php
												/*
												 * General selected 
												 */
												   AWE_function::apb_get_option_to_selected(
														   array(
															   'name'      => 'room_child',
															   'start_num' => 0,
															   'count_num' => 10,
															   'data'      => array('class' => 'form-select awe-select'),
															   'select'  =>  ""
															   )
														   );
												  ?>
											</p>
											<p class="awe-form-inline">
												 <label for="edit-label"><?php _e("Order Status",'awebooking') ?></label><br/>
												 <select class="form-select ajax-processed awe-select" name="apb_order_status">
												   <?php 
												   $apb_get_order_statuses = AWE_function::apb_get_order_statuses();
												   foreach ($apb_get_order_statuses as $key_status => $val_status): ?>
												   <option value="<?php echo $key_status ?>"><?php echo $val_status ?></option>
												  <?php endforeach; ?>
											   </select>
											</p>
											 <p class="awe-form-inline apb-single-avb-package-js">
												 
											 </p>
											 <input type="hidden" name="room_type" value="<?php echo esc_attr(get_post_meta(wp_kses($_GET['room_id'], ''),"room_type_id",true));?>">
											 <p class="awe-form-inline"><button id="add-booking-js" class="button"><?php _e( 'Add Booking', 'awebooking' ); ?></button></p>
										</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="box-footer">
				<button type="button" class="close-box-room-type-js button"><?php _e('Close','awebooking') ?></button>
			</div>
		</div>
	</div>
	<div class="box-event-info-js">
		<table class="sticky-header" >
			<thead>
				<tr>
					<th><?php _e('Booking Details','awebooking') ?></th>
				</tr>
			</thead>
			<tbody>
				<tr class="odd">
					<td>
						<span class="spinner is-active" style="display: none;"></span>
						<div class="content-event-js"> </div>
					</td>
				</tr>
		   
			</tbody>
			 <tfoot>
				<tr>
					<th  colspan="2"> 
						<button type="button" class="close-box-room-type-js button"><?php _e('Close','awebooking') ?></button>
					</th>
				</tr>
			</tfoot>
		</table>
	</div>
	
</div>
</div>



