<?php

/**
 * Description of boxes_apb_order_data
 *
 * @author Letrungha
 */
class boxes_apb_order_data {
     public static function output( $post ) {
       $custom_info = get_post_meta($post->ID, "info_custom_order",true);
       if(!empty($custom_info)){
	       	foreach ($custom_info as $key => $val){
	           echo "<strong>".str_replace("-"," ",  ucwords($key))."</strong> : ".$val."<br/>";
	       }
       }   
     }
}
