<?php
/**
 * The template for displaying loop price of room.
 *
 * Override this template by copying it to your theme
 *
 * @author  AweTeam
 * @package AweBooking/Templates
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( isset( $total_date ) ) {
	$price_total_day = AWE_function::get_total_day_price( $info_price_day );
	$total_day = AWE_function::get_number_day_price( $info_price_day );
	$price_average = $price_total_day / ( $total_day );
} else {
	$price_average = $room_price;
}
if ( 1 == $show || 2 == $show ) {
	printf(
		'<span class="apb-room_price">
			<span class="apb-room_amount apb-price-%s">%s</span> / %s
			<input class="room-price-base-%s" data-base="%s" value="%s" type="hidden">
		</span>',
		// esc_html__( 'From', 'awebooking' ),
		get_the_ID(),
		AWE_function::apb_price( $price_average ),
		esc_html__( 'Night', 'awebooking' ),
		get_the_ID(),
		$price_average,
		$price_average
	);
}

if ( 1 == $show || 3 == $show ) {
	if ( ! empty( $total_price_all ) ) {
		if ( ! isset( $price_total_day ) ) {
			$price_total_day = $total_price_all;
		}
		printf(
			'<span class="apb-room_price apb-total-price">
				%s <span class="apb-room_amount apb-total-all-price-%s" data-value="%s">%s</span>
			</span>',
			esc_html__( 'Total:', 'awebooking' ),
			get_the_ID(),
			// $price_total_day,
			// AWE_function::apb_price( $price_total_day )
			$total_price_all,
			AWE_function::apb_price( $total_price_all )
		);

		/**
		 * Total price of day.
		 * Not change
		 */
		$apb_currency_pos = get_option( 'woocommerce_currency_pos' ) && in_array( get_option( 'woocommerce_currency_pos' ), array( 'left', 'right', 'left_space', 'right_space' ) ) ? get_option( 'woocommerce_currency_pos' ) : 'left';
		$apb_decimals = get_option( 'woocommerce_price_num_decimals' ) ? absint( get_option( 'woocommerce_price_num_decimals' ) ) : 2;
		$apb_decimal_sep = get_option( 'woocommerce_price_decimal_sep' ) ? get_option( 'woocommerce_price_decimal_sep' ) : '.';
		$apb_thousand_sep = get_option( 'woocommerce_price_thousand_sep' ) ? get_option( 'woocommerce_price_thousand_sep' ) : ',';

		printf(
			'<input class="total-price-room-%s" value="%s" data-currency-pos="%s" data-decimals="%s" data-decimal-sep="%s" data-thousand-sep="%s" data-currency="%s" type="hidden">',
			absint( get_the_ID() ),
			( float ) $total_price_all,
			esc_attr( $apb_currency_pos ),
			absint( $apb_decimals ),
			esc_attr( $apb_decimal_sep ),
			esc_attr( $apb_thousand_sep ),
			esc_attr( AWE_function::get_currency( $currency ) )
		);
	}
}
