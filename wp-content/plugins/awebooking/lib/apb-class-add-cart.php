<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * AWE Add To Cart Class
 *
 * @class 		APB_Cart
 * @version		1.0
 * @package		AweBooking/Classes/
 * @author 		AweTeam
 */

class APB_Cart {

	public function __construct() {
		/*========================================
		 =            Add room to cart           =
		 ========================================*/
		add_action("wp_ajax_apb_add_to_cart", array( $this, "apb_add_to_cart"));
		add_action("wp_ajax_apbRoomType_addToCart", array( $this, "apbRoomType_addToCart"));

		add_action("wp_ajax_nopriv_apb_add_to_cart", array( $this, "apb_add_to_cart"));
		add_action("wp_ajax_nopriv_apbRoomType_addToCart", array( $this, "apbRoomType_addToCart"));
		/*=====  End of Add room to cart  ======*/

		add_action("wp_ajax_user_check_available", array( $this, "user_check_available"));
		add_action("wp_ajax_nopriv_user_check_available", array( $this, "user_check_available"));

		add_action("wp_ajax_apb_ResultIDRoom", array( $this, "apb_ResultIDRoom"));
		add_action("wp_ajax_nopriv_apb_ResultIDRoom", array( $this, "apb_ResultIDRoom"));
	}

	/**
	 * Add room to cart session.
	 *
	 * @deprecated 2.0
	 */
	public function apb_add_to_cart() {
		_deprecated_function( __FUNCTION__, '2.0' );
		$from           = $_POST['from'];
		$to             = $_POST['to'];
		$room_id        = wp_kses( $_POST['room_id'], '');
		$room_info      = get_post( $room_id);
		$max_sleeps     = get_post_meta( $room_info->ID, "max_sleeps", true);
		$min_sleeps     = get_post_meta( $room_info->ID, "min_sleeps", true);
		$max_children   = get_post_meta( $room_info->ID, "max_children", true);
		$min_children   = get_post_meta( $room_info->ID, "min_children", true);

		$key = substr(md5(rand(0, 9)), 0, 5);

		$num = $_POST['num_people'];

		$html = '';

		// Check number of adult + child of room available.
		if ( $_POST['room_adult'][ $num ]['adult'] + $_POST['room_child'][ $num ]['child'] <= $max_sleeps && $_POST['room_adult'][ $num ]['adult'] + $_POST['room_child'][ $num ]['child'] >= $min_sleeps ) {

			if ( '' === $max_children && '' === $min_children || $_POST['room_child'][ $num ]['child'] <= $max_children && $_POST['room_child'][ $num ]['child'] >= $min_children ) {

				// Get Price of option extra price.
				$extra_adult    =  get_post_meta( $room_id, 'extra_adult', true );
				$extra_child    =  get_post_meta( $room_id, 'extra_child', true );
				$extra_sale     =  get_post_meta( $room_id, 'extra_sale', true );
				$count_date = count( AWE_function::range_date( $from, $to ) );

				$_price = $_POST['price'];
				if ( ! empty( $extra_adult ) ) {
					$total_date = $count_date - 1;
					foreach ( $extra_adult as $item_extra_adult ) {
						if ( $_POST['room_adult'][ $num ]['adult'] == $item_extra_adult['number'] ) {
							$_price += $total_date * $item_extra_adult['price'];
						}
					}
				}
				if(!empty( $extra_child)){
					$total_date = $count_date - 1;
					foreach ( $extra_child as $item_extra_child) {
					   if( $_POST['room_child'][ $num]['child']  == $item_extra_child['number']){
						 $_price += $total_date*$item_extra_child['price'];
					   }
					}
				}
				if ( ! empty( $extra_sale ) ) {
					$total_date = $count_date;
					$data_extra_sale = AWE_function::apb_get_extra_sale( $extra_sale, $total_date, $from );
					if ( ! empty( $data_extra_sale ) ) {
						if( $data_extra_sale['sale_type'] == 'sub'){
							 $_price = $_price - $data_extra_sale['amount'];
						}
						if( $data_extra_sale['sale_type'] == 'decrease'){
							 $_price = $_price - $data_extra_sale['amount'] / 100 * $_price;
						}
						$sale_info = $data_extra_sale;
					}
				}

				if ( isset( $_POST['key_cart_change'] ) ) {
					$_SESSION['apb_cart'][ $_POST['key_cart_change']] = array(
						"room_id"        => $room_id,
						"price"          => $_POST['price'],
						// "price"          => $_price,
						"from"           => $from,
						"to"             => $to,
						"adult"          => $_POST['room_adult'][ $num]['adult'],
						"child"          => $_POST['room_child'][ $num]['child'],
						"package_data"   => isset( $_POST['package_data']) ? $_POST['package_data'] : array(),
						"sale_info"     => isset( $sale_info) ? $sale_info : "",
						"check"			=> 'apb_room_type'
					);
				} else {
					$_SESSION['apb_cart'][ $key."-".$_POST['room_id']] = array(
						"room_id"       => $room_id,
						"from"          => $from,
						"to"            => $to,
						"price"          => $_POST['price'],
						// "price"         => $_price,
						"adult"         => $_POST['room_adult'][ $num]['adult'],
						"child"         => $_POST['room_child'][ $num]['child'],
						"package_data"  => isset( $_POST['package_data']) ? $_POST['package_data' ] : array(),
						"sale_info"     => isset( $sale_info) ? $sale_info : "",
						"check"			=> 'apb_room_type'
					);
				}
			}
		}
		ob_start();
		do_action( 'layout_room_select' );
		$html .= ob_get_clean();
		echo $html;

		die;
		return false;
	}

	/**
	 * Add to cart by room type.
	 * @return void
	 * @deprecated 2.0
	 */
	public function apbRoomType_addToCart() {
		_deprecated_function( __FUNCTION__, '2.0' );
		$args_rooms = explode( ',', $_POST['args_rooms'] );

		$num = $_POST['num_people'];
		$key = substr( md5( rand( 0, 9 ) ), 0, 5 );
		for ( $i = 0; $i <= $_POST['booking_num'] - 1; $i++ ) {

			$extra_adult    =  get_post_meta( $args_rooms[ $i ], 'extra_adult', true );
			$extra_child    =  get_post_meta( $args_rooms[ $i ], 'extra_child', true );
			$extra_sale     =  get_post_meta( $args_rooms[ $i ], 'extra_sale', true );

			if ( isset( $_POST['price'] ) ) {
				$_price =  $_POST['price'] * $this->ApbCart_GetPrice( $_POST['from'], $_POST['to'], $args_rooms[ $i ], false );
			} else {
				$_price =  $this->ApbCart_GetPrice( $_POST['from'], $_POST['to'], $args_rooms[ $i ], true );
			}

			/*----------  Plus price by adult  ----------*/
			if ( ! empty( $extra_adult ) ) {
				$total_date = count( AWE_function::range_date( $_POST['from'], $_POST['to'] ) ) - 1;
				foreach ( $extra_adult as $item_extra_adult ) {
					if ( $_POST['room_adult'][ $num ]['adult']  == $item_extra_adult['number'] ) {
						$_price += $total_date * $item_extra_adult['price'];
					}
				}
			}

			/*----------  Plus price by child  ----------*/
			if ( ! empty( $extra_child ) ) {
				$total_date = count( AWE_function::range_date( $_POST['from'], $_POST['to'] ) ) - 1;
				foreach ( $extra_child as $item_extra_child ) {
					if ( $_POST['room_child'][ $num ]['child']  == $item_extra_child['number'] ) {
						$_price += $total_date * $item_extra_child['price'];
					}
				}
			}

			/*----------  Plus price by sale  ----------*/
			if(!empty( $extra_sale)){
				$total_date  = count(AWE_function::range_date( $_POST['from'],$_POST['to']));
				$data_extra_sale = AWE_function::apb_get_extra_sale( $extra_sale,$total_date,$_POST['from']);
					if(!empty( $data_extra_sale)){
						if( $data_extra_sale['sale_type'] == 'sub'){
							 $_price =  $_price-$data_extra_sale['amount'];
						}
						if( $data_extra_sale['sale_type'] == 'decrease'){
							 $_price = $_price-$data_extra_sale['amount']/100*$_price;
						}
						$sale_info = $data_extra_sale;
					}
			}
			/*----------  Plus price package  ----------*/
			if ( isset( $_POST['package_data'] ) ) {
				global $wpdb;
				foreach ( $_POST['package_data'] as $item_package ) {
					$sql = $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}apb_booking_options where id = %d", $item_package['package_id'] );
					$data_package = $wpdb->get_results( $sql);
					$_price += $data_package[0]->option_value*$item_package['total'];
				}
			}
			$_SESSION['apb_cart'][ $key . '-' . $args_rooms[ $i ] ] = array(
				"room_id"       => $args_rooms[ $i ],
				"from"          => $_POST['from'],
				"to"            => $_POST['to'],
				"price"         => $_price,
				"adult"         => $_POST['room_adult'][ $num ]['adult'],
				"child"         => $_POST['room_child'][ $num ]['child'],
				"package_data"  => isset( $_POST['package_data']) ? $_POST['package_data' ] : array(),
				"sale_info"     => isset( $sale_info ) ? $sale_info : "",
				"roomtype_id"	=> $_POST['roomtype_id'],
				"check"			=> 'room_type'
			);
		}
		$html = '';
		ob_start();
		do_action("layout_room_select");
		$html .= ob_get_clean();
		echo $html;
		die;
		return false;
	}

	/**
	 *	ApbCart_GetPrice
	 *  @param string $from
	 *  @param string $to
	 *  @param int $room_id
	 */
	public function ApbCart_GetPrice( $from, $to, $room_id, $byDay = true ) {
		$price = 0;
		if ( $byDay ) {
			$args_price = AWE_function::get_pricing_of_days( $from, $to, $room_id );
			foreach ( $args_price as $itemPrice ) {
				foreach ( $itemPrice as $value ) {
					$price = $value;
				}
			}
		} else {
			$price = count( AWE_function::range_date( $from, $to ) ) - 1;
		}

		return $price;
	}

	public function user_check_available() {
		ob_start();

		if ( $_POST['control'] == "check") {
			if( get_option('rooms_checkout_style') == 1){
				if(class_exists('WooCommerce')){
					WC()->cart->empty_cart();
				}
			}
			if( get_option('rooms_checkout_style') == 2){
				 $_SESSION['apb_cart'] = array();
			}
		}

		if ( $_POST['control'] == "change") {
			if(get_option('rooms_checkout_style') == 1){
				if(class_exists('WooCommerce')){
					global $woocommerce;
					foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {

						if ( $cart_item['product_id'] == $_POST['room_id']) {

							WC()->cart->remove_cart_item( $cart_item_key);
						}
					}
				}
			}
			// if(get_option('rooms_checkout_style') == 2){
			//      unset( $_SESSION['apb_cart'][ $_POST['key']]);
			// }
		}
		do_action( 'loop_data_check_availability' );
		$html = ob_get_clean();
		echo $html;
		die;
		return false;
	}

	public function apb_ResultIDRoom() {
		$args_rooms = explode( ',', $_POST['args_rooms'] );
		$room_id = array();
		for ( $i = 0; $i <= $_POST['booking_num'] - 1; $i++ ) {
			$room_id[] = $args_rooms[ $i ];
		}
		echo json_encode( $room_id );
		die;
		return false;
	}
}
new APB_Cart();
