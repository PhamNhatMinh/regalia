<?php 

include("includes/conf.class.php");
include("includes/admin.class.php")
?>    
<?php
	wp_enqueue_style('jquery-style', plugins_url().'/bsi-hotel-pro/front/css/datepicker.css');
	
	//wp_enqueue_script( 'custom_script10', plugins_url().'/bsi-hotel-pro/js/bsi_datatables.js');
		wp_enqueue_script( 'custom_script11', plugins_url().'/bsi-hotel-pro/js/DataTables/jquery.dataTables.js');
		wp_enqueue_style('custom-style9', plugins_url().'/bsi-hotel-pro/css/data.table.css');
		wp_enqueue_style('custom-style10', plugins_url().'/bsi-hotel-pro/css/jqueryui.css');
?>
 <script type="text/javascript">
	jQuery(document).ready(function(){
		disableInput("#submit");
		jQuery('#book_type').change(function(){
			if(jQuery('#book_type').val() != ""){
				enableInput("#submit");			
			}else{
				disableInput("#submit");
			}
		});
		//Enabling Disabling Function
		function disableInput(id){
			jQuery(id).attr('disabled', 'disabled');
		}
		function enableInput(id){
			jQuery(id).removeAttr('disabled');	
		}
	});
</script>
      <p>&nbsp;</p>
      <div id="container-inside">
      <span style="font-size:16px; font-weight:bold"><?php echo VIEW_BOOKING_LIST;?></span>
      <hr />
        <form action="<?php echo admin_url('admin.php?page=View-Booking-List'); ?>" method="post" id="form1">
          <table cellpadding="5" cellspacing="2" border="0">
            <tr>
            <td valign="middle"><strong><?php echo VIEW_BOOKINGS_SELECT_TYPE;?></strong>:</td>
            <td><select name="book_type" id="book_type"><option value="" selected="selected">---<?php echo SELECT_BOOKING_TYPE;?>---</option><option value="1"><?php echo ACTIVE_BOOKING;?></option><option value="2" ><?php echo BOOKING_HISTORY;?></option></select> </td>
            
            </tr>
            
           <tr><td><strong><?php echo VB_DATE_RANGE_OPTIONAL;?></strong></td><td><input id="txtFromDate" name="fromDate" style="width:68px" type="text" readonly="readonly" />
      <span style="padding-left:0px;"><a id="datepickerImage" href="javascript:;"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/images/month.png" height="16px" width="16px" style=" margin-bottom:-4px;" border="0" /></a></span>&nbsp;&nbsp;&nbsp;&nbsp; <strong><?=VB_TO?></strong>&nbsp;&nbsp;&nbsp;<input id="txtToDate" name="toDate" style="width:68px" type="text" readonly="readonly"/>
      <span style="padding-left:0px;"><a id="datepickerImage1" href="javascript:;"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/images/month.png" height="18px" width="18px" style=" margin-bottom:-4px;" border="0" /></a></span>&nbsp;&nbsp;&nbsp;&nbsp;<strong><?=VB_BY?>    </strong>&nbsp;&nbsp;&nbsp;&nbsp;<select name="shortby"><option value="booking_time" selected="selected"><?=VIEW_ACTIVE_BOOKING_DATE?></option><option value="start_date"><?=CUSTOMER_BOOKING_CHECK_IN_DATE?></option><option value="end_date"><?=CUSTOMER_BOOKING_CHECK_OUT_DATE?></option></select></td></tr>
           <tr><td></td><td><input type="submit" value="<?php echo "SUBMIT";?>" name="submit" id="submit" style="background:#e5f9bb; cursor:pointer; cursor:hand;"/></td></tr>
          </table>
        </form>
        <br /><br />
         <span style="font-size:16px; font-weight:bold"><?php echo LAST_10_BOOKING;?></span>
     	 <hr />
          <table class="display datatable" border="0" id="test5">
         <?php echo $bsiAdminMain->homewidget(1); ?>
        </table>
         <br />
         
         <span style="font-size:16px; font-weight:bold"><?php echo TODAY_CHECK_IN;?></span>
      	 <hr />
          <table class="display datatable" border="0">
         <?php echo $bsiAdminMain->homewidget(2); ?>
        </table>
         <br />
         
         <span style="font-size:16px; font-weight:bold"><?php echo TODAY_CHECK_OUT;?></span>
         <hr />
          <table class="display datatable" border="0">
        <?php echo $bsiAdminMain->homewidget(3); ?>
        </table>
         <br />
      </div>
      
      
         
<script type="text/javascript">
	jQuery().ready(function() {
		jQuery("#form1").validate();
		
     });
         
</script>      

<script type="text/javascript">
jQuery(document).ready(function(){
 jQuery.datepicker.setDefaults({ dateFormat: '<?php echo $bsiCore->config['conf_dateformat']?>' });
    jQuery("#txtFromDate").datepicker({
        maxDate: "+365D",
        numberOfMonths: 2,
        onSelect: function(selected) {
    	var date = jQuery(this).datepicker('getDate');
         if(date){
            date.setDate(date.getDate());
          }
          jQuery("#txtToDate").datepicker("option","minDate", date)
        } 
    });
 
    jQuery("#txtToDate").datepicker({ 
        maxDate:"+365D",
        numberOfMonths: 2,
        onSelect: function(selected) {
           jQuery("#txtFromDate").datepicker("option","maxDate", selected)
        }
    });  
 jQuery("#datepickerImage").click(function() { 
    jQuery("#txtFromDate").datepicker("show");
  });
 jQuery("#datepickerImage1").click(function() { 
    jQuery("#txtToDate").datepicker("show");
  });
});
</script>

<script>
 jQuery(document).ready(function() {
	 	var oTable = jQuery('.datatable').dataTable( {
			    "responsive": true,
				"bJQueryUI": true,
				"sScrollX": "",
				"bSortClasses": false,
				"aaSorting": [[0,'asc']],
				"bAutoWidth": true,
				"bInfo": true,
				"sScrollY": "100%",	
				"sScrollX": "100%",
				"bScrollCollapse": true,
				"sPaginationType": "full_numbers",
				"bRetrieve": true,
				"oLanguage": {
								"sSearch": "Search:",
								"sInfo": "Showing _START_ to _END_ of _TOTAL_ entries",
								"sInfoEmpty": "Showing 0 to 0 of 0 entries",
								"sZeroRecords": "No matching records found",
								"sInfoFiltered": "(filtered from _MAX_ total entries)",
								"sEmptyTable": "No data available in table",
								"sLengthMenu": "Show _MENU_ entries",
								"oPaginate": {
												"sFirst":    "First",
												"sPrevious": "Previous",
												"sNext":     "Next",
												"sLast":     "Last"
											  }
							 }
	} ); 
} );
</script> 