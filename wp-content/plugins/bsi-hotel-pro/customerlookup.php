<?php 

if(isset($_REQUEST['update'])){
	
	header("location:admin.php?page=Customerlookup"); 
	exit;
}
include("includes/conf.class.php");
include("includes/admin.class.php");
$html = $bsiAdminMain->getCustomerHtml();
//wp_enqueue_script( 'custom_script10', plugins_url().'/bsi-hotel-pro/js/bsi_datatables.js');
wp_enqueue_script( 'custom_script11', plugins_url().'/bsi-hotel-pro/js/DataTables/jquery.dataTables.js');
wp_enqueue_style('custom-style9', plugins_url().'/bsi-hotel-pro/css/data.table.css');
wp_enqueue_style('custom-style10', plugins_url().'/bsi-hotel-pro/css/jqueryui.css');
 ?>  
      <p>&nbsp;</p>
      <div id="container-inside">
       <span style="font-size:16px; font-weight:bold"><?=CUSTOMERLOOKUP_CUSTOMER_LIST?></span>
       <hr />
        <table class="display datatable" border="0">
          <thead>
	<tr><th width="18%" nowrap="nowrap"><?=CUSTOMERLOOKUP_GUEST_NAME?></th><th width="27%" nowrap="nowrap"><?=CUSTOMER_STREET_ADDRESS?></th><th width="15%" nowrap="nowrap"><?=CUSTOMERLOOKUP_PHONE_NUMBER?></th><th width="25%" nowrap="nowrap"><?=CUSTOMERLOOKUP_EMAIL_ID?></th><th width="15%" nowrap="nowrap">&nbsp;</th></tr>
 </thead>
 <tbody id="getcustomerHtml">
 	<?php echo $html?>
 </tbody>
        </table>
      </div>
 <script>
jQuery(document).ready(function() {
	 	var oTable = jQuery('.datatable').dataTable( {
			     "responsive": true,
				"bJQueryUI": true,
				"sScrollX": "",
				"bSortClasses": false,
				"aaSorting": [[0,'desc']],
				"bAutoWidth": true,
				"bInfo": true,
				"sScrollY": "100%",	
				"sScrollX": "100%",
				"bScrollCollapse": true,
				"sPaginationType": "full_numbers",
				"bRetrieve": true,
				"oLanguage": {
								"sSearch": "Search:",
								"sInfo": "Showing _START_ to _END_ of_TOTAL_ entries",
								"sInfoEmpty": "Showing 0 to 0 of 0 entries",
								"sZeroRecords": "No matching records found",
								"sInfoFiltered": "(filtered from _MAX_total entries)",
								"sEmptyTable": "No data available in table",
								"sLengthMenu": "Show _MENU_Entries",
								"oPaginate": {
												"sFirst":    "First",
												"sPrevious": "Previous",
												"sNext":     "Next",
												"sLast":     "Last"
											  }
							 }
	} );
} );
</script> 
