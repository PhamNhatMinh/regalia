<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 *  Tempalte setting form check available
 *
 * @version		1.0
 * @package		AweBooking/admin/
 * @author 		AweTeam
 */

$field_status = get_option( 'apb_setting' );
?>
<p>This setting only is applied on Awe check availability page.</p>
<div class="form-elements">
	<p><?php esc_html_e( 'Number of units to display', 'awebooking' ); ?></p>
	<select name="limit-page">
		<?php for ( $i = 0; $i <= 20; $i++ ) : ?>
			<option <?php selected( get_option( 'limit-page' ), $i ); ?> value="<?php echo absint( $i ); ?>"><?php echo absint( $i ); ?></option>
		<?php endfor; ?>
	</select>
	<span class="description">
		<?php esc_html_e( 'Maximum number of rooms in check availability result page.', 'awebooking' ); ?>
	</span>
</div>

<label><?php esc_html_e( 'Enable or disable field of form check available.', 'awebooking' ); ?></label>
<div class="form-elements">
	<label></label>
	<div class="form-radios">
		<input class="apb_departure" name="apb_setting[field_status][status_departure_page]" <?php checked( $field_status['field_status']['status_departure_page'], 1, true ); ?> type="checkbox" value="1">
		<?php esc_html_e( 'Enable Departure Date', 'awebooking' ); ?>
	</div>

</div>
<div class="form-elements">
	<div class="form-radios">
		<input class="apb_nightnum" name="apb_setting[field_status][status_night_number_page]" <?php checked( $field_status['field_status']['status_night_number_page'], 1, true ); ?> type="checkbox" value="1">
		<?php esc_html_e( 'Enable Night Number', 'awebooking' ); ?>
	</div>
</div>
<div class="form-elements">
	<div class="form-radios">
		<input name="apb_setting[field_status][status_multi_room_page]" <?php checked( $field_status['field_status']['status_multi_room_page'], 1, true ); ?> type="checkbox" value="1">
		<?php esc_html_e( 'Enable Select Multiple Room', 'awebooking' ); ?>
	</div>
</div>





