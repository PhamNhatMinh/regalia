<?php
/*
Plugin Name: Online Hotel Booking System Pro (Wordpress Plugin) (shared on wplocker.com)
Plugin URI: http://www.bestsoftinc.com
Description: Wordpress plugin of online hotel booking system Pro
Version: 1.0
Author: Best Soft Inc
Author URI: http://www.bestsoftinc.com
*/




function db_install() {
   global $wpdb;
   $wpdb->query("CREATE TABLE IF NOT EXISTS `bsi_advance_payment` (
  `month_num` int(11) NOT NULL AUTO_INCREMENT,
  `month` varchar(255) NOT NULL,
  `deposit_percent` decimal(10,2) NOT NULL,
  PRIMARY KEY (`month_num`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;
");
$wpdb->query("INSERT INTO `bsi_advance_payment` (`month_num`, `month`, `deposit_percent`) VALUES
(1, 'January', '0.00'),
(2, 'February', '0.00'),
(3, 'March', '0.00'),
(4, 'April', '0.00'),
(5, 'May', '50.00'),
(6, 'June', '50.00'),
(7, 'July', '0.00'),
(8, 'August', '0.00'),
(9, 'September', '0.00'),
(10, 'October', '0.00'),
(11, 'November', '0.00'),
(12, 'December', '0.00');
");	
$wpdb->query("CREATE TABLE IF NOT EXISTS `bsi_bookings` (
  `booking_id` int(10) unsigned NOT NULL,
  `booking_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` date NOT NULL DEFAULT '0000-00-00',
  `end_date` date NOT NULL DEFAULT '0000-00-00',
  `client_id` int(10) unsigned DEFAULT NULL,
  `child_count` int(2) NOT NULL DEFAULT '0',
  `extra_guest_count` int(2) NOT NULL DEFAULT '0',
  `discount_coupon` varchar(50) DEFAULT NULL,
  `total_cost` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `payment_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `payment_type` varchar(255) NOT NULL,
  `payment_success` tinyint(1) NOT NULL DEFAULT '0',
  `payment_txnid` varchar(100) DEFAULT NULL,
  `paypal_email` varchar(500) DEFAULT NULL,
  `special_id` int(10) unsigned NOT NULL DEFAULT '0',
  `special_requests` text,
  `is_block` tinyint(4) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `block_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`booking_id`),
  KEY `start_date` (`start_date`),
  KEY `end_date` (`end_date`),
  KEY `booking_time` (`discount_coupon`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

$wpdb->query("CREATE TABLE IF NOT EXISTS `bsi_capacity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `capacity` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;");

$wpdb->query("CREATE TABLE IF NOT EXISTS `bsi_cc_info` (
  `booking_id` varchar(100) CHARACTER SET latin1 NOT NULL,
  `cardholder_name` varchar(255) NOT NULL,
  `card_type` varchar(50) NOT NULL,
  `card_number` blob NOT NULL,
  `expiry_date` varchar(10) CHARACTER SET latin1 NOT NULL,
  `ccv2_no` int(4) NOT NULL,
  PRIMARY KEY (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$wpdb->query("CREATE TABLE IF NOT EXISTS `bsi_clients` (
  `client_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(64) DEFAULT NULL,
  `surname` varchar(64) DEFAULT NULL,
  `title` varchar(16) DEFAULT NULL,
  `street_addr` text,
  `city` varchar(64) DEFAULT NULL,
  `province` varchar(128) DEFAULT NULL,
  `zip` varchar(64) DEFAULT NULL,
  `country` varchar(64) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `fax` varchar(64) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `id_type` varchar(255) NOT NULL,
  `id_number` varchar(50) NOT NULL,
  `additional_comments` text,
  `ip` varchar(32) DEFAULT NULL,
  `existing_client` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`client_id`),
  KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;");

$wpdb->query("CREATE TABLE IF NOT EXISTS `bsi_configure` (
  `conf_id` int(11) NOT NULL AUTO_INCREMENT,
  `conf_key` varchar(100) NOT NULL,
  `conf_value` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`conf_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='bsi hotel configurations' AUTO_INCREMENT=36 ;
");

$wpdb->query("INSERT INTO `bsi_configure` (`conf_id`, `conf_key`, `conf_value`) VALUES
(1, 'conf_hotel_name', 'Hotel Booking System Pro'),
(2, 'conf_hotel_streetaddr', '99 xxxxx Road'),
(3, 'conf_hotel_city', 'Your City'),
(4, 'conf_hotel_state', 'Your State'),
(5, 'conf_hotel_country', 'USA'),
(6, 'conf_hotel_zipcode', '11211'),
(7, 'conf_hotel_phone', '+18778888972'),
(8, 'conf_hotel_fax', '+18778888972'),
(9, 'conf_hotel_email', 'sales@bestsoftinc.com'),
(20, 'conf_tax_amount', '9'),
(21, 'conf_dateformat', 'mm/dd/yy'),
(22, 'conf_booking_exptime', '1000'),
(25, 'conf_enabled_deposit', '1'),
(26, 'conf_hotel_timezone', 'Asia/Calcutta'),
(27, 'conf_booking_turn_off', '0'),
(28, 'conf_min_night_booking', '1'),
(30, 'conf_notification_email', 'sales@bestsoftinc.com'),
(31, 'conf_price_with_tax', '0'),
(32, 'conf_maximum_global_years', '730'),
(33, 'conf_payment_currency', '0'),
(34, 'conf_invoice_currency', '0'),
(35, 'conf_term_and_condition', ''),
(36, 'conf_currency_update_time', '1404278603');");

$wpdb->query("CREATE TABLE IF NOT EXISTS `bsi_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_code` varchar(10) NOT NULL,
  `currency_symbl` varchar(10) NOT NULL,
  `exchange_rate` decimal(20,6) NOT NULL,
  `default_c` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8  ;
");

$wpdb->query("INSERT INTO `bsi_currency` (`id`, `currency_code`, `currency_symbl`, `exchange_rate`, `default_c`) VALUES
(1, 'USD', '$', 1.000000, 1);");

$wpdb->query("CREATE TABLE IF NOT EXISTS `bsi_email_contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_name` varchar(500) NOT NULL,
  `email_subject` varchar(500) NOT NULL,
  `email_text` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;");
$wpdb->query("INSERT INTO `bsi_email_contents` (`id`, `email_name`, `email_subject`, `email_text`) VALUES
(1, 'Confirmation Email', 'Confirmation of your successfull booking in our hotel', '<p><strong>Text can be chnage in admin panel</strong></p>\r'),
(2, 'Cancellation Email ', 'Cancellation Email subject', '<p><strong>Text can be chnage in admin panel</strong></p>\r\ncvxcv');");


$wpdb->query("CREATE TABLE IF NOT EXISTS `bsi_gallery` (
  `pic_id` int(11) NOT NULL AUTO_INCREMENT,
  `roomtype_id` int(11) NOT NULL,
  `capacity_id` int(11) NOT NULL,
  `img_path` varchar(255) NOT NULL,
  PRIMARY KEY (`pic_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

$wpdb->query("CREATE TABLE IF NOT EXISTS `bsi_invoice` (
  `booking_id` int(10) NOT NULL,
  `client_name` varchar(500) NOT NULL,
  `client_email` varchar(500) NOT NULL,
  `invoice` longtext NOT NULL,
  PRIMARY KEY (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$wpdb->query("CREATE TABLE IF NOT EXISTS `bsi_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_title` varchar(255) NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `lang_file` varchar(255) NOT NULL,
  `lang_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;
");

$wpdb->query("INSERT INTO `bsi_language` (`id`, `lang_title`, `lang_code`, `lang_file`, `lang_default`) VALUES
(1, 'English', 'en', 'english.php', 1),
(2, 'French', 'fr', 'french.php', 0),
(3, 'German', 'de', 'german.php', 0),
(4, 'Greek', 'el', 'greek.php', 0),
(5, 'Spanish', 'es', 'espanol.php', 0),
(6, 'Italian', 'it', 'italian.php', 0),
(7, 'Dutch', 'nl', 'dutch.php', 0),
(8, 'Polish', 'pl', 'polish.php', 0),
(9, 'Portuguese', 'pt', 'portuguese.php', 0),
(10, 'Russian', 'ru', 'russian.php', 0),
(11, 'Turkish', 'tr', 'turkish.php', 0),
(12, 'Thai', 'th', 'thai.php', 0),
(13, 'Chinese', 'zh-CN', 'chinese.php', 0),
(14, 'Indonesian', 'id', 'indonesian.php', 0),
(15, 'Romanian', 'ro', 'romanian.php', 0),
(17, 'Japanese', 'ja', 'japanese.php', 0);
");

$wpdb->query("CREATE TABLE IF NOT EXISTS `bsi_payment_gateway` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gateway_name` varchar(255) NOT NULL,
  `gateway_code` varchar(50) NOT NULL,
  `account` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
");
$wpdb->query("INSERT INTO `bsi_payment_gateway` (`id`, `gateway_name`, `gateway_code`, `account`, `enabled`) VALUES
(1, 'PayPal', 'pp', 'phpdev_1330251667_biz@aol.com', 1),
(2, 'Manual : Pay on Arrival', 'poa', 'null', 1),
(3, 'Credit Card (offline)', 'cc', 'null', 1),
(4, 'Stripe', 'st', 'sk_test_HTGxkUdVdAGd3t0dFqFomE40#pk_test_c6KEy6DebkozRP0V9ysSDwVq#1', 1);
");

$wpdb->query("CREATE TABLE IF NOT EXISTS `bsi_priceplan` (
  `plan_id` int(10) NOT NULL AUTO_INCREMENT,
  `roomtype_id` int(10) DEFAULT NULL,
  `capacity_id` int(11) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `sun` decimal(10,2) DEFAULT '0.00',
  `mon` decimal(10,2) DEFAULT '0.00',
  `tue` decimal(10,2) DEFAULT '0.00',
  `wed` decimal(10,2) DEFAULT '0.00',
  `thu` decimal(10,2) DEFAULT '0.00',
  `fri` decimal(10,2) DEFAULT '0.00',
  `sat` decimal(10,2) DEFAULT '0.00',
  `default_plan` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`plan_id`),
  KEY `priceplan` (`roomtype_id`,`capacity_id`,`start_date`,`end_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8  ;
");

$wpdb->query("CREATE TABLE IF NOT EXISTS `bsi_reservation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bookings_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `room_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8  ;

");

$wpdb->query("CREATE TABLE IF NOT EXISTS `bsi_room` (
  `room_ID` int(10) NOT NULL AUTO_INCREMENT,
  `roomtype_id` int(10) DEFAULT NULL,
  `room_no` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `capacity_id` int(10) DEFAULT NULL,
  `no_of_child` int(11) NOT NULL DEFAULT '0',
  `extra_bed` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`room_ID`),
  KEY `roomtype_id` (`roomtype_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8  ;
");

$wpdb->query("CREATE TABLE IF NOT EXISTS `bsi_roomtype` (
  `roomtype_ID` int(10) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(500) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`roomtype_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8  ;
");

$wpdb->query("CREATE TABLE IF NOT EXISTS `bsi_special_offer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `offer_title` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `room_type` varchar(255) CHARACTER SET latin1 NOT NULL,
  `price_deduc` decimal(10,2) NOT NULL,
  `min_stay` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8  ;
");

}

register_activation_hook(__FILE__,'db_install');
register_deactivation_hook(__FILE__ , 'bsi_uninstall');
add_action( 'admin_enqueue_scripts', 'admin_f' ); 
add_action( 'init', 'wp29r01_date_picker' );
add_action( 'admin_init', 'language' );
function admin_f()
{
	wp_enqueue_script( 'jquery-ui-datepicker', array('jquery','jquery-ui-core'));
	wp_enqueue_style('jquery-style', plugins_url().'/bsi-hotel-pro/front/css/datepicker.css');
	wp_enqueue_style('custom-style7', plugins_url().'/bsi-hotel-pro/css/style.css');
	wp_enqueue_style('custom-style8', plugins_url().'/bsi-hotel-pro/css/jquery.validate.css');
	wp_enqueue_script( 'jquery-validate', plugins_url().'/bsi-hotel-pro/js/jquery.validate.js'); 
}
function wp29r01_date_picker() { 	
    wp_enqueue_script( 'jquery-ui-datepicker',  plugins_url().'/bsi-hotel-pro/js/bootstrap.min.js');
	wp_enqueue_script( 'jquery-ui-datepicker',  plugins_url().'/bsi-hotel-pro/js/bootstrap-datepicker.js');
	wp_enqueue_script( 'jquery-ui-datepicker', array('jquery','jquery-ui-core'));
	
	wp_enqueue_style('jquery-style', plugins_url().'/bsi-hotel-pro/front/css/datepicker.css');
	
	wp_enqueue_style('jquery-bootstrap-style', plugins_url().'/bsi-hotel-pro/front/css/bootstrap.css');
	wp_enqueue_style('jquery-bootstrap-responsive-style', plugins_url().'/bsi-hotel-pro/front/css/bootstrap-responsive.css');
	
	
	
	}
function language(){
	global $wpdb;
	
	$lang_row = $wpdb->get_row("SELECT * FROM `bsi_language` where lang_default='1'",ARRAY_A);
	include_once("languages/".$lang_row['lang_file']);
	
}
add_action('admin_menu','menu_creator');
if(!session_id()){
	add_action('init','session_start');
}
add_shortcode("bsi_hotel_pro","dis_front_func");
function menu_creator()
{
	$icon_url= plugins_url().'/bsi-hotel-pro/images/plugin.ico';
	// starting of the 1st menu
	add_menu_page('Hotel Home','Hotel Home','manage_options','hotel_home', 'admin_home', $icon_url);
	add_menu_page('Bsi Hotel','Hotel Manager','manage_options','hotel_manager', 'defaul', $icon_url);
	add_submenu_page( "hotel_manager", "Hotel Details", "Hotel Details", "manage_options", "hotel-details", 'hotel_details'); // FOR THE MAIN MENU
	
	add_submenu_page( "hotel_manager", "Room Manager", "Room Manager", "manage_options", "Room-Manager", 'room_list'); // FOR THE SUB MENU
	add_submenu_page('Room-Manager', 'Add New Room', 'Add New Room','manage_options', 'aad-new-room', 'add_room'); // FOR THE INNER PAGE
	//add_links_page( $page_title, $menu_title, $capability, $menu_slug, $function);
	
	
//  testing menu	
//add_submenu_page( "hotel_manager", "Room upload", "Room upload", "manage_options", "roomupload", 'room_upload'); // FOR THE SUB MENUadd_pages_page('Hotel Maneger', 'add new room', 'manage_options', 'add-room', 'add_room'); // FOR THE INNER PAGE
	
	add_submenu_page("hotel_manager", "room type manager", "Room Type Manager", "manage_options", "room-type-manager", 'room_type');
	add_submenu_page('Hotel Maneger', 'add new room type', 'add new room type', 'manage_options', 'aad-new-room-type', 'add_room_type'); // FOR THE INNER PAGE
	
	add_submenu_page("hotel_manager", "capacity manager", "Capacity Manager", "manage_options", "capacity-manager", 'capacity_manager');
	
	add_submenu_page("hotel_manager", "Photo Gallery", "Photo Gallery", "manage_options", "photo-gallery", 'photo_gallery');
	
	add_submenu_page('Hotel Maneger', 'Add Photo', 'dd Photo', 'manage_options', 'add-photo', 'photo_gallery_add');
    add_submenu_page('Hotel Maneger', 'add capacity', 'add capacity', 'manage_options', 'aad-edit-capacity', 'add_capacity'); // FOR THE INNER PAGE
	// ending of the 1st menu
	
	//2n menu starting menu
	
	add_menu_page('Bsi Hotel2','Price Manager','manage_options','price_manager', 'defaul', $icon_url);
	add_submenu_page( "price_manager", "price plan", "Price Plan", "manage_options", "price-plan", 'price_plan'); // FOR THE MAIN MENU
	add_submenu_page('price-plan', 'Add Price Plan', 'Add Price Plan', 'manage_options', 'add-price-plan', 'add_price'); // FOR THE INNER PAGE
	add_submenu_page( "price_manager", "Special Offer", "Special Offer", "manage_options", "special-offer", 'special_offer');
	
	add_submenu_page('price-plan', 'Add Edit Special Offer', 'Add Edit Special Offer', 'manage_options', 'entry-edit-special-offer', 'edit_special_offer'); 
	add_submenu_page( "price_manager", "advance payment", "Advance Payment", "manage_options", "advance-payment", 'advance_payment');
	
	
	add_menu_page('Bsi Hotel3','Booking Manager','manage_options','booking_maneger', 'booking_list', $icon_url);
	add_submenu_page("booking_maneger", "booking list", "Booking list", "manage_options", "booking-list", 'booking_list'); // FOR THE MAIN MENU
	add_submenu_page('booking-list', 'View Booking List ', 'View Booking List ', 'manage_options', 'View-Booking-List', 'view_booking_list'); // FOR THE INNER PAGE
// FOR THE INNER PAGE
	add_submenu_page('Customerlookup', 'customer booking', 'customer booking', 'manage_options', 'Customer-booking', 'Customer_booking'); // FOR THE INNER PAGE
	add_submenu_page('Customer-booking', 'customer booking edit', 'customer booking edit', 'manage_options', 'Customer-booking-edit', 'Customer_booking_edit'); // FOR THE INNER PAGE
	add_submenu_page('booking-list', 'View Details', 'View Details', 'manage_options', 'view-details', 'view_details'); // FOR THE INNER PAGE
	add_submenu_page("booking_maneger", "Customer Lookup", "Customer Lookup", "manage_options", "Customerlookup", 'Customer_Lookup'); // FOR THE MAIN MENU 
	add_submenu_page("booking_maneger", "Calender View", "Calender View", "manage_options", "Calender-View", 'Calender_View'); // FOR THE MAIN MENU
	add_submenu_page("booking_maneger", "admin block room", "Admin Block Room", "manage_options", "admin-block-room", 'admin_block_room'); // FOR THE MAIN MENU
	add_submenu_page('admin-block-room', 'block room', 'block room', 'manage_options', 'block-room', 'block_room'); // FOR THE INNER PAGE
	add_submenu_page('Customerlookup', 'customer lookup edit', 'customer lookup edit', 'manage_options', 'customer-lookup-edit', 'customer_lookup_edit'); // FOR THE INNER PAGE
	add_menu_page('Currency Manager','Currency Manager','manage_options','currency-manager', 'currency_manager', $icon_url);
	add_menu_page('Language Manager','Language Manager','manage_options','language-manager', 'language_manager', $icon_url);
	
	
	add_submenu_page('Currency Manager', 'Currency Manager edit', 'Currency Manager edit', 'manage_options', 'currency-manager-edit', 'currency_Manager_edit');
	
	add_submenu_page('Language Manager', 'Language Manager Edit', 'Language Manager Edit', 'manage_options', 'language-manager-edit', 'language_manager_edit');
	
	
	add_menu_page('Bsi Hotel4','Hotel Setting','manage_options','Setting', 'defaul', $icon_url);
	add_submenu_page( "Setting", "Global Setting", "Global Setting", "manage_options", "global-setting", 'global_setting'); // FOR THE MAIN MENU
	add_submenu_page( "Setting", "Payment Getway", "Payment Getway", "manage_options", "Payment-Getway", 'Payment_Getway'); // FOR THE MAIN MENU
	add_submenu_page( "Setting", "email content", "Email Content", "manage_options", "email-content", 'email_content'); // FOR THE MAIN MENU
	//removing the extra main menu from the menu list
	remove_submenu_page('booking_maneger','booking_maneger');
	remove_submenu_page('page','aad-new-room');
	remove_submenu_page('price_manager','price_manager');
	remove_submenu_page('hotel_manager','hotel_manager');
	remove_submenu_page('Setting','Setting');
	remove_submenu_page('Setting','Setting');
} 
function Customer_booking_edit()
{
	
	include('customerlookupEdit.php');	
}
function bsi_uninstall()
{
	global $wpdb;
	$sql="DROP TABLE `bsi_advance_payment`, `bsi_bookings`, `bsi_capacity`, `bsi_cc_info`, `bsi_clients`, `bsi_configure`, `bsi_currency`, `bsi_email_contents`, `bsi_gallery`, `bsi_invoice`, `bsi_language`, `bsi_payment_gateway`, `bsi_priceplan`, `bsi_reservation`, `bsi_room`, `bsi_roomtype`, `bsi_special_offer`;";
	$wpdb->query($sql);
}
function view_details()
{
	include('viewdetails.php');	
}
function admin_home(){
	include('admin-home.php');
}

function defaul()
{
	echo "<h1>Select a Submenu</h1>";
}
function hotel_details()
{
	if(!isset($_SESSION['url']))
	{
 		$_SESSION['url']=admin_url();
		
	}
	include('admin_hotel_details.php');
}
function room_list()
{
	
include('room_list.php');
//echo  plugins_url()."/bsi-hotel-pro/js/DataTables/jquery.dataTables.js";
}
function customer_lookup_edit()
{
	include('customerlookupEdit.php');	
}
function booking_details()
{
	include('booking_details.php');	
}
function add_room()
{	
	
	include('add_edit_room.php');
}
function room_type()
{
	include('roomtype.php');
}
function capacity_manager()
{
	include("admin_capacity.php");
}
function add_edit_capacity()
{
	include('add_edit_capacity.php');
}
function add_capacity()
{
	
	include('add_edit_capacity.php');
}
function photo_gallery(){
	include('gallery_list.php');
}
function photo_gallery_add(){
	include('add_gallery.php');
}
function price_plan()
{
	
	include('priceplan.php');
}
function add_price()
{
	include("add_edit_priceplan.php");
		
}
function currency_Manager_edit(){
	include("add_edit_currency.php");
}

function language_manager_edit(){
	include("add_edit_language.php");
}
function advance_payment(){
	include("advance_payment.php");
}
function add_room_type()
{
	include('add_edit_roomtype.php');
}
function room_upload()
{
	
	
}
function currency_manager(){
	include('currency_list.php');
}
function booking_list(){
	include('view_bookings.php');
}
function view_booking_list(){
	include('view_active_or_archieve_bookings.php');
}
function Customer_Lookup(){

	include("customerlookup.php");
}
function Calender_View()
{
	include('calendar_view.php');	
}
function admin_block_room()
{
	include("admin_block_room.php");
}
function block_room()
{
	include("block_room.php");
}
function global_setting()
{
	include('global_setting.php');	
}
function edit_special_offer(){
	include('entry_edit_special_offer.php');
}
function language_manager(){
	include('manage_langauge.php');
}
function Payment_Getway()
{
	
	include('payment_gateway.php');
}
function email_content()
{
	
	include("email_content.php");
		
}

function Customer_booking()
{
	include('customerbooking.php');	
}
function special_offer(){
	include("view_special_offer.php");
}
function dis_front_func()
{
	
	if(!isset($_REQUEST['check_in'])&& !isset($_REQUEST['check_in'])&& !isset($_REQUEST['registerButton'])&& !isset($_REQUEST['registerButton2']) && !isset($_REQUEST['success_code'])&& !isset($_REQUEST['error_code'])&& !isset($_REQUEST['amount']) && !isset($_REQUEST['x_invoice_num']) && !isset($_REQUEST['CardNumber'])&&!isset($_REQUEST['action'])  && !isset($_REQUEST['ansubmit'])&& !isset($_REQUEST['ansubmit2']))
	{	
	
	$dir=ABSPATH."wp-content/plugins/bsi-hotel-pro/front/index.php";
	
	include($dir); 
		
	}
	
	if(isset($_REQUEST['check_in']))
	{
		
		$dir=ABSPATH."wp-content/plugins/bsi-hotel-pro/front/booking-search.php";
		include($dir);
		
	}
	
	
	if(isset($_REQUEST['registerButton']))
	{
		$dir=ABSPATH."wp-content/plugins/bsi-hotel-pro/front/booking_details.php";
		include($dir);
	
	}
	
	if(isset($_REQUEST['registerButton2']))
	{				
		$dir=ABSPATH."wp-content/plugins/bsi-hotel-pro/front/booking-process.php";
		include($dir);
	}
	
	if(isset($_REQUEST['success_code']))
	{
		
		unset($_SESSION['success_code']); 
		$dir=ABSPATH."wp-content/plugins/bsi-hotel-pro/front/booking-confirm.php";
		include($dir);	
		
	}
	if(isset($_REQUEST['error_code']))
	{
		
		unset($_SESSION['success_code']); 
		$dir=ABSPATH."wp-content/plugins/bsi-hotel-pro/front/booking-confirm.php";
		include($dir);
	}
	if(isset($_REQUEST['amount']))
	{
		$dir=ABSPATH."wp-content/plugins/bsi-hotel-pro/front/paypal.php";
		include($dir);
	}
	
	if(isset($_REQUEST['x_invoice_num']))
	{
		$dir=ABSPATH."wp-content/plugins/bsi-hotel-pro/front/offlinecc-payment.php";
		include($dir);	
	}
	
	if(isset($_REQUEST['CardNumber']))
	{
		$dir=ABSPATH."wp-content/plugins/bsi-hotel-pro/front/cc_process.php";
		include($dir);
	}
	
	if(isset($_REQUEST['action']))
	{
		
		$dir=ABSPATH."wp-content/plugins/bsi-hotel-pro/front/paypal.php";
		include($dir);
	}
	if(isset($_REQUEST['ansubmit'])){
		
		$dir=ABSPATH."wp-content/plugins/bsi-hotel-pro/front/stripe-processor.php";
		include($dir);
	}
	
	if(isset($_REQUEST['ansubmit2'])){
		
		$dir=ABSPATH."wp-content/plugins/bsi-hotel-pro/front/an_direct_post.php";
		include($dir); 
	}
	
	
}
function arka_func()
{ 
	include('a.php');
	
}
?>