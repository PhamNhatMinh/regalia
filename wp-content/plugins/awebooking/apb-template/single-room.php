<?php
/**
 * The template for displaying single room
 *
 * Override this template by copying it to your theme
 *
 * @author  AweTeam
 * @package AweBooking/Templates
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_header();

$_date_curent = date_create( date( 'Y-m-d' ) );
date_add( $_date_curent, date_interval_create_from_date_string( ( isset( $_GET['apb_mon'] ) ? $_GET['apb_mon'] : '0' ) . ' month' ) );
$new_date = date_format( $_date_curent, 'Y-m-d' );
$month_current = isset( $_GET['apb_mon'] ) ? $_GET['apb_mon'] + 2 : 2;
$month_sub = isset( $_GET['apb_mon'] ) ? $_GET['apb_mon'] - 2 : '-2';
if ( date( 'm', strtotime( $new_date ) ) == 12 ) {
	$_year = date( 'Y', strtotime( $new_date ) ) + 1;
} else {
	$_year = date( 'Y', strtotime( $new_date ) );
}
do_action( 'apb_renderBefore' );
?>
<div class="apb-container">
	<?php
	/**
	 * Layout_loading hook.
	 */
	do_action( 'layout_loading' );
	?>
	<div class="apb-product_detail room-detail">
		<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<div class="apb-layout">
					<div class="apb-content-area">
						<?php
						/**
						 * Hook: apb_single_gallery.
						 */
						do_action( 'apb_single_gallery' );
						?>
					</div>

					<div class="apb-widget-area">
						<div class="room-detail_book">
							<div class="room-detail_total">
								<h6><?php echo esc_html( ucfirst( get_the_title() ) ); ?></h6>
								<?php loop_price_single( get_post_meta( get_the_ID(), 'base_price', true ) ); ?>
							</div>

							<?php
							form_check_availability();

							/**
							 * Hook: apb_form_check_available_single.
							 */
							// do_action( 'apb_form_check_available_single' );
							?>
						</div>
					</div>
				</div>

				<?php // do_action( 'apb_single_message' ); ?>

				<div class="apb-product_tab">

					<ul class="apb-product_tab-header">
						<li>
							<a href="#information" data-toggle="tab"><?php esc_html_e( 'Information', 'awebooking' ) ?></a>
						</li>

						<?php
						$package = AWE_function::get_room_option( get_the_ID(), 'apb_room_type' );
						if ( ! empty( $package ) ) :
							?>
							<li>
								<a href="#package" data-toggle="tab"><?php esc_html_e( 'Optional extras', 'awebooking' ) ?></a>
							</li>
						<?php endif; ?>

						<?php if ( AWE_function::show_single_calendar() ) : ?>
							<li class="active">
								<a href="#date-available" data-toggle="tab"><?php esc_html_e( 'Date Available', 'awebooking' ) ?></a>
							</li>
						<?php endif; ?>
					</ul>

					<div class="apb-product_tab-panel tab-content">
						<div class="tab-pane fade" id="information">
							<?php the_content() ?>
						</div>

						<div class="tab-pane fade" id="package">
							<?php
							/**
							 * Hook : apb_loop_single_package
							 * Get list package for room.
							 * @hooked loop_single_package
							 */
							do_action( 'apb_loop_single_package' );
							?>
						</div>

						<?php if ( AWE_function::show_single_calendar() ) : ?>
							<div class="tab-pane fade  active in" id="date-available">
								<?php
								$next = add_query_arg(
									array(
										'apb_mon'	=> $month_current,
										'apb_year'	=> date( 'Y', strtotime( $new_date ) ),
										'room_id'	=> get_the_ID(),
									),
									get_permalink()
								);
								$prev = add_query_arg(
									array(
										'apb_mon'	=> $month_sub,
										'apb_year'	=> date( 'Y', strtotime( $new_date ) ),
										'room_id'	=> get_the_ID(),
									),
									get_permalink()
								);
								?>
								<script>
									var awe_date_curent_1  = '<?php echo date( 'Y', strtotime( $new_date ) ); ?>-<?php echo date( 'm', strtotime( $new_date ) ); ?>';
									var awe_date_curent_2  = '<?php echo $_year; ?>-<?php echo date( 'm', strtotime( date( 'Y', strtotime( $new_date ) ) . '-' . ( date( 'm', strtotime( $new_date ) ) + 1 ) ) ); ?>';
									var room_id = <?php the_ID() ?>
								</script>
								<div class="apb-month">
									<a href="<?php echo esc_html( $prev ); ?>#date-available" class="apb-fc-nav apb-fc-prev" type="button"><?php esc_html_e( 'Prev', 'awebooking' ); ?></a>
									 <div id="calendar"> </div>
								</div>
								<div class="apb-month">
								   <a href="<?php echo esc_html( $next ); ?>#date-available" class="apb-fc-nav apb-fc-next" type="button"><?php esc_html_e( 'Next', 'awebooking' ); ?></a>
								   <div id="calendar2"> </div>
								</div>
							</div>
						<?php endif; ?>
					</div>

				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</div>
<!-- END / PAGE WRAP -->
<?php
do_action( 'apb_renderAfter' );
get_footer();
?>
