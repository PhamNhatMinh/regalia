<?php 

if(isset($_GET['delimg'])){
	include("includes/conf.class.php");
	include("includes/admin.class.php");
	$bsiAdminMain->deletegallery();
	header("location:admin.php?page=photo-gallery");
	exit;
}
include("includes/admin.class.php");

?>
<?php
	wp_enqueue_style('jquery-style', plugins_url().'/bsi-hotel-pro/front/css/datepicker.css');
   //wp_enqueue_script( 'custom_script10', plugins_url().'/bsi-hotel-pro/js/bsi_datatables.js');
	wp_enqueue_script( 'custom_script26', plugins_url().'/bsi-hotel-pro/js/fancybox/jquery.fancybox-1.3.4.js');
	wp_enqueue_script( 'custom_script11', plugins_url().'/bsi-hotel-pro/js/DataTables/jquery.dataTables.js');
	wp_enqueue_style('custom-style9', plugins_url().'/bsi-hotel-pro/css/data.table.css');
	wp_enqueue_style('custom-style10', plugins_url().'/bsi-hotel-pro/css/jqueryui.css');
	wp_enqueue_style('custom-style11', plugins_url().'/bsi-hotel-pro/css/jqueryui.css');
	wp_enqueue_style('custom-style55', plugins_url().'/bsi-hotel-pro/css/gallery.css');
	wp_enqueue_style('custom-style15', plugins_url().'/bsi-hotel-pro/js/fancybox/jquery.fancybox-1.3.4.css');
?>
<br />

<div id="container-inside" style="width:90%"> <span style="font-size:16px; font-weight:bold"><?php echo HOTEL_PHOTO_GALLERY_TEXT; ?></span>
  <input type="button" value="<?php echo ADD_PHOTO_TEXT; ?>" onClick="window.location.href='admin.php?page=add-photo'" style="background: #EFEFEF; float:right"/>
  
    <?php if(isset($_SESSION['hotelgal'])){echo $bsiAdminMain->getroomtypewithcapacity($_SESSION['hotelgal']);}else{echo $bsiAdminMain->getroomtypewithcapacity();}?>
  </select>
  <hr />
  <div class="indent gallery" id="gallery"></div>
</div>
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery('#hotelid').change(function(){
		if(jQuery('#hotelid').val() != "0"){
			getImage();
		}if(jQuery('#hotelid').val() == "0"){
			jQuery('#gallery').html('<?php echo PLEASE_SELECT_A_ROOM_TYPE_ALERT; ?>');
		}
	});
	
	if(jQuery('#hotelid').val() != ""){
		getImage();
	}
	
	function getImage(){
		var querystr='actioncode=6&type_capacity_id='+jQuery('#hotelid').val();
		
		jQuery.post("<?php echo plugins_url(); ?>/bsi-hotel-pro/admin_ajax_processor.php", querystr, function(data){						 
			if(data.errorcode == 0){
				//alert(data.viewcontent)
				jQuery('#gallery').html(data.viewcontent);
			}else{
				jQuery('#gallery').html(data.strmsg);
			}
		}, "json");	
	}
});	

function deleteImage(delimg){
	var roomtype_with_capacity = document.getElementById('hotelid').value;
	window.location.href='admin.php?page=photo-gallery&delimg='+delimg+'&noheader=true';	
}
</script> 

