<?php
//include(ABSPATH."wp-content/plugins/bsi-hotel-pro/front/languages/english.php");
	include(WP_PLUGIN_DIR."/bsi-hotel-pro/includes/conf.class.php");
	$bsiCore->exchange_rate_update();
	include("language.php");
	wp_enqueue_style('style-pro', plugins_url().'/bsi-hotel-pro/front/css/prostyle11.css'); 
 
?> 

<script>
		
jQuery(document).ready(function() {
 jQuery.datepicker.setDefaults({ dateFormat: '<?php echo $bsiCore->config['conf_dateformat'];?>'});
    jQuery("#dpd1").datepicker({ 
        minDate: 0,
        maxDate: "+365D",
        numberOfMonths: 2,
        onSelect: function(selected) {
    	var date = jQuery(this).datepicker('getDate');
         if(date){
            date.setDate(date.getDate() + <?php echo $bsiCore->config['conf_min_night_booking'];?>);
          }
          jQuery("#dpd2").datepicker("option","minDate", date)
        } 
    });
 
    jQuery("#dpd2").datepicker({ 
        minDate: 0,
        maxDate:"+365D",
        numberOfMonths: 2,
        onSelect: function(selected) {
          jQuery("#dpd1").datepicker("option","maxDate", selected)
        }
    });  
 jQuery("#datepickerImage").click(function() { 
   jQuery("#dpd1").datepicker("show");
  });
 jQuery("#datepickerImage1").click(function() { 
    jQuery("#dpd2").datepicker("show");
  });
  
  jQuery('#btn_room_search').click(function() { 		
	  	if(jQuery('#dpd1').val()==""){
	  		alert('<?php echo addslashes(ENTER_CHECK_IN_DATE_ALERT);?>'); 
	  		return false;
	 	}else if(jQuery('#dpd2').val()==""){
	  		alert('<?php echo addslashes(ENTER_CHECK_OUT_DATE_ALERT);?>'); 
	  		return false;
	  	} else {
	  		return true;
	 	}	  
	});	
});
</script>

<script language="javascript">
function langchange(lng)
{
	
	<?php
		if(isset($_SESSION['url'])){
			$url = $_SESSION['url'];
		}else{
			$url = get_permalink();
			$_SESSION['url'] = $url;
		}
		if(strpos($url,'?')===false){
			$url = $url.'?lang=';
		}else{
			$url = $url.'&lang=';
		}
		
	?>
	window.location.href = '<?=$url;?>' + lng;
}	
	
</script>

					<div class="select-language" align="center">
                        <select class="input-medium" onChange="langchange(this.value)"> 
                           <?php echo $lang_dd; ?>
                         </select>
               	    
					</div>   <br />
                  <!--  <h1 style="padding-top:10px;"><?php echo $bsiCore->config['conf_hotel_name']; ?> </h1>-->
                     <div class="wizard">
                     	<div class="step1 yellowStep"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/1.png" alt="" /><p><?php echo SELECT_DATES_TEXT; ?></p></div>
                        <div class="step2 grayStep"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/2.png" alt="" /><p><?php echo ROOMS_TEXT; ?> &amp; <?php echo RATES_TEXT; ?></p></div>
                        <div class="step3 grayStep"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/3.png" alt="" /><p><?php echo YOUR_DETAILS_TEXT; ?></p></div>
                        <div class="step4 grayStep"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/4.png" alt="" /><p><?php echo PAYMENT_TEXT; ?></p></div>
                        <div class="step5 grayLastStep"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/5.png" alt="" /><p><?php echo CONFIRM_TEXT; ?></p></div>
                     </div>
                     <div class="progress">
						 <div class="bar bar-warning" style="width: 20%;"></div>
                     </div>
	
                     
                    <div class="wrapper">
                    	
                        <form class="form-horizontal" style="width: 95%; margin:0 2.5% 2.5%" action="<?php the_permalink(); ?>" method="post">
                        <div class="control-group">
                            <label class="control-label" for="checkInDate"><?php echo CHECK_IN_DATE_TEXT; ?>:</label>
                            <div class="controls">
                            	
                                
                               
                                    <input class="input-small" type="text" readonly   id="dpd1"  name="check_in"  data-date-format="<?php echo $bsiCore->bt_date_format(); ?>">
                                  
                                
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="checkOutDate"><?php echo  CHECK_OUT_DATE_TEXT; ?>:</label>
                            <div class="controls">
                            	
                                    <input class="input-small" type="text" readonly    id="dpd2"  name="check_out" data-date-format="<?php echo $bsiCore->bt_date_format(); ?>">
                                   
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="checkInDate"><?php echo ADULT_OR_ROOM_TEXT; ?>:</label>
                            <div class="controls">
                            	<?php echo $bsiCore->capacitycombo();?>
                            </div>
                        </div>
                        <?php echo $bsiCore->getChildcombo();?>
                       
                       <?php echo $bsiCore->get_currency_combo3($bsiCore->currency_code()); ?>
                       
                        <!--<button id="btn_room_search" class="sear-btn" type="submit" onClick="window.location.href ='search-result.html'">Search</button>-->
                        <button id="btn_room_search" class="sear-btn" type="submit" ><?php echo SEARCH_TEXT; ?></button>
                        </form>
                    </div>
