<?php
/**
 *  Tempalte setting calendar
 *
 * @version		1.0
 * @package		AweBooking/admin/
 * @author 		AweTeam
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="form-elements">
    <p><?php esc_html_e( 'Number of units to display', 'awebooking' ); ?></p>
    <select name="limit-page">
        <?php for( $i = 0; $i <= 20; $i++ ) : ?>
        	<option <?php selected( $i, get_option( 'limit-page' ) ); ?> value="<?php echo absint( $i ); ?>"><?php echo absint( $i ); ?></option>
        <?php endfor; ?>
    </select>
    <span class="description">
       <?php esc_html_e( 'Maximum number of rooms in check availability result page.', 'awebooking' ) ?>
    </span>
</div>
