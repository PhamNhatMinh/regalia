<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * AWE Created Room type
 *
 * @version		1.0
 * @package		AweBooking/admin/
 * @author 		AweTeam
 */

if(!empty($current_data)){

	$type_submit        = "edit";
	$type_id            = $current_data[0]->id;
	$type_type          = $current_data[0]->type;
	$type_label         = $current_data[0]->label;
	$data               = unserialize($current_data[0]->data);
	$object_id          = $current_data[0]->object_id;
	$type_base_price    = $data["base_price"];
	$type_min_sleeps    = $data["min_sleeps"];
	$type_max_sleeps    = $data["max_sleeps"];
	$type_min_children  = $data["min_children"];
	$type_min_night     = !empty($data["min_night"]) ? $data["min_night"] : "";
	$type_max_children  = $data["max_children"];
	$extra_adult        = isset($data["extra_adult"]) ? $data["extra_adult"] : "";
	$extra_child        = isset($data["extra_child"]) ? $data["extra_child"] : "";
	$extra_sale         = isset($data["extra_sale"]) ? $data["extra_sale"] : "";

}else{

	$type_submit     = "insert";
	$type_id         = 0;      $type_type          = ""; $type_label        = ""; $type_data            = "";
	$type_min_sleeps = 0;      $type_max_sleeps    = 0;  $type_min_children = "";  $type_max_children    = "";
	$extra_adult     = "";     $extra_child        = ""; $object_id         = ""; $type_base_price      = "";
	$extra_sale      = "";     $type_min_night     = 0;
}

?>
<div id="awe-booking-wraper" class="awe-booking-wraper">
<div class="awe-plugin">
	<div class="clearfix" id="branding">
	  <div class="awe-brand-header">
		<h1 class="page-title"><?php ($type_submit == 'edit') ? _e("[ Edit ]",'awebooking')  : _e('Room Type','awebooking'); echo !empty($type_label) ? " ".$type_label : ""; ?></h1>
	  </div>
	</div>
	<?php
	  if(isset($_SESSION['apb_notice'])){
		?>
		  <div class="updated notice notice-success is-dismissible below-h2" id="message">
		  <p><?php echo esc_html($_SESSION['apb_notice']); ?></p>
		  <button class="notice-dismiss" type="button">
		  <span class="screen-reader-text"><?php _e('Dismiss this notice.','awebooking' ); ?></span></button>
		  </div>
		  <br/>
		<?php
		unset($_SESSION['apb_notice']);
	  }
	?>

	<form class="rooms-management-form rooms-unit-type-edit-form"  method="post" action="">
		<h3 class="hndle"><span><?php _e( 'Room Type Data', 'awebooking' ); ?></span></h3>
		<br/>
		<div class="panel-wrap product_data awe-panel-with-table">
			 <ul class="product_data_tabs awe-tabs">
				<li>
					<a href="#general_product_data"><?php _e( 'General', 'awebooking' ); ?></a>
				</li>
				<li>
					<a href="#sale_price_total_day"><?php _e( 'Discount', 'awebooking' ); ?></a>
				</li>
				<li>
					<a href="#roomtype_package"><?php _e( 'Package', 'awebooking' ); ?></a>
				</li>
				<li>
					<a href="#extra_price"><?php _e( 'Extra Price', 'awebooking' ); ?></a>
				</li>
				<li>
					<a href="#desc_roomtype"><?php _e( 'Description', 'awebooking' ); ?></a>
				</li>
			</ul>

			<div id="general_product_data" class="panel awe_options_panel">
			<?php if(isset($_SESSION['error_room_type'])): ?>
				<div class="notice notice-error"> <p><?php echo $_SESSION['error_room_type']; unset($_SESSION['error_room_type']); ?></p></div>
			<?php endif; ?>
			  <div class="options_group ">
				<p class="awe-form-inline">
					<label for="edit-label"><?php _e("Room Type",'awebooking') ?></label>
					<input type="text" value="<?php if(isset($_SESSION['label'])){ echo $_SESSION['label']; unset($_SESSION['label']); }else{ echo esc_attr($type_label); }  ?>" name="label">
				  <?php AWE_function::awe_help('Name the room type.') ?>
				</p>
				<p class="awe-form-inline">
					<label for="edit-slug"><?php esc_html_e( 'Room Type Slug', 'awebooking' ); ?></label>
					<input type="text" value="<?php if ( isset( $_SESSION['slug'] ) ) { echo $_SESSION['slug']; unset($_SESSION['slug']); } else { echo esc_attr($type_type); } ?>" name="slug">
				  <?php AWE_function::awe_help('Name the room type.') ?>
				</p>
				<p class="awe-form-inline">
					<label for="edit-label"><?php _e("Base price",'awebooking') ?></label>
					<input name="data[base_price]" type="text" class="form-text" value="<?php if(isset($_SESSION['base_price'])){ echo $_SESSION['base_price']; unset($_SESSION['base_price']); }else{ echo esc_attr($type_base_price); }  ?>">
					<?php AWE_function::awe_help('The default price for all room of this room type.') ?>
				</p>
				<p class="awe-form-inline">
					<label for="edit-label"><?php _e("Sleeping capacity",'awebooking') ?></label>
					<input name="data[min_sleeps]" type="text" placeholder="Minimum" class="form-text apb-int"  value="<?php echo esc_attr($type_min_sleeps)  ?>">
					<input type="text" class="form-text apb-int" placeholder="Maximum" name="data[max_sleeps]"  value="<?php echo esc_attr($type_max_sleeps)  ?>">
					<?php AWE_function::awe_help('Minimum - Maximum guests(adult + children) allowed.') ?>
				</p>
				<p class="awe-form-inline">
					<label for="edit-label"><?php _e("Child capacity",'awebooking') ?></label>
					<input type="text" class="form-text apb-int" placeholder="Minimum" name="data[min_children]" value="<?php echo esc_attr($type_min_children)  ?>">
					<input type="text" class="form-text apb-int" placeholder="Maximum" name="data[max_children]" value="<?php echo esc_attr($type_max_children)  ?>">
					<?php AWE_function::awe_help('Minimum - Maximum children allowed.') ?>
				</p>
				<p class="awe-form-inline">
					<label for="edit-label"><?php _e("Minimum Night",'awebooking') ?></label>
					<input type="text" class="form-text apb-int" placeholder="Minimum night" name="data[min_night]" value="<?php echo esc_attr($type_min_night)  ?>">
					<?php AWE_function::awe_help('Minimum night number.') ?>
				</p>
			  </div>
			</div>

			<div id="roomtype_package" class="panel awe_options_panel">
				<div class="options_group ">
					<div class="form-field">
						<table class="field-multiple-table awe-add-option-table">
							<thead>
								<tr>
									<th class="field-label" colspan="2">
										<label><?php _e('Options','awebooking') ?> </label>
									</th>
								</tr>
							</thead>
							<tbody class="form-option-js">
								<?php
									$data_option = $this->get_room_option($type_id,"room_type");
									if(!empty($data_option)):
									foreach ($data_option as $item_option):
								?>
								<tr class="draggable odd item-option op-<?php echo $item_option->id ?>">
									<td>
										<div class="form-item form-type-textfield">
											<div class="form-elements">
												<label><?php _e('Name','awebooking') ?> </label>
												<input type="text" size="6" value="<?php echo $item_option->option_name ?>" name="option_name[]">
											</div>
										</div>
										 <div class="form-item form-type-textfield text-desc">
											<div class="form-elements">
												<label><?php _e('Description','awebooking') ?></label>
												<textarea name="option_desc[]"><?php echo esc_attr($item_option->option_desc) ?></textarea>
											</div>
										</div>

										<div class="form-item form-type-textfield">
											<div class="form-elements">
												<label><?php _e('Price','awebooking') ?> </label>
												<input name="option_value[]" type="text" size="10" value="<?php echo esc_attr($item_option->option_value) ?>">
												<div class="awe-set-type-package">
													<input class="apb-option-type-js" data-id="<?php echo esc_attr($item_option->id) ?>" type="checkbox" <?php checked( $item_option->revision_id,1 ); ?> value="1">
													<input type="hidden" id="apb-option-type-<?php echo esc_attr($item_option->id) ?>" name="type_package[]" value="<?php echo esc_attr($item_option->revision_id) ?>">
													<span><?php _e('Daily package','awebooking') ?> </span>
												</div>
											</div>
										</div>
										<div class="form-item form-type-textfield">
											<div class="form-elements">
												<label>&nbsp; </label>
												<input type="hidden" name="option_id[]" value="<?php echo esc_attr($item_option->id) ?>">
												<button type="button" data-id="<?php echo esc_attr($item_option->id) ?>" class="remove-option-js button"><?php _e('Remove','awebooking') ?></button>
											</div>
										</div>
									</td>
								</tr>
								<?php endforeach; endif; ?>
							</tbody>
							<tfoot>
								<tr>
									<th  colspan="2">
										<button type="button" class="awe-add-option-js button"><?php _e('Add another item','awebooking') ?></button>
										<span class="spinner" style="display: none;"></span>
									</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>

			<div id="extra_price" class="panel awe_options_panel">
				<div class="options_group ">
					<div class="form-field">
					  <table class="field-multiple-table awe-add-option-table">
					   <thead>
							 <tr>
								<th></th>
								<th class="field-label"><label><?php _e('For Adults','awebooking') ?> </label></th>
								<th></th>
							</tr>
							<tr>
								<th class="field-label"><label><?php _e('Amount','awebooking') ?> </label></th>
								<th class="field-label"><label><?php _e('Extra Price','awebooking') ?> </label></th>
								<th class="field-label"><label><?php _e('Action','awebooking') ?> </label></th>
							</tr>
						</thead>
						<tbody class="form-extra-price-adult-js">
						<?php
							if(is_array($extra_adult)):
								foreach ($extra_adult as $item_extra_adult):
						?>
							<tr  class="item-extra-price">
								<td><input type="text" class="apb-int"value="<?php echo esc_attr($item_extra_adult['number']); ?>" name="extra_adult[number][]"></td>
								<td><input type="text" value="<?php echo esc_attr($item_extra_adult['price']); ?>" name="extra_adult[price][]"></td>
								<td><button type="button" class="button remove-extra-price-js"><?php _e('Remove','awebooking') ?></button></td>
							</tr>
						<?php endforeach; endif; ?>
						</tbody>
						 <tfoot>
								<tr>
									<th  colspan="2">
										<button type="button" class="button add_extrad_price_adult_js"><?php _e('Add','awebooking') ?></button>
										<span class="spinner add_extra_price" style="display: none;"></span>
									</th>
								</tr>
							</tfoot>
					  </table>
					  <table class="field-multiple-table awe-add-option-table">
					   <thead>
							 <tr>
								<th></th>
								<th class="field-label"><label><?php _e('For Children','awebooking') ?> </label></th>
								<th></th>
							</tr>
							<tr>
								<th class="field-label"><label><?php _e('Amount','awebooking') ?> </label></th>
								<th class="field-label"><label><?php _e('Extra Price','awebooking') ?> </label></th>
								<th class="field-label"><label><?php _e('Action','awebooking') ?> </label></th>
							</tr>
						</thead>
						<tbody class="form-extra-price-child-js">
						   <?php
								if(is_array($extra_child)):
									foreach ($extra_child as $item_extra_child):
							?>
								<tr  class="item-extra-price">
									<td><input type="text" class="apb-int" value="<?php echo esc_attr($item_extra_child['number']); ?>" name="extra_child[number][]"></td>
									<td><input type="text" value="<?php echo esc_attr($item_extra_child['price']); ?>" name="extra_child[price][]"></td>
									<td><button type="button" class="button remove-extra-price-js"><?php _e('Remove','awebooking') ?></button></td>
								</tr>
							<?php endforeach; endif; ?>
						</tbody>
						<tfoot>
							<tr>
								<th  colspan="2">
									<button type="button" class="button add_extrad_price_child_js"><?php _e('Add','awebooking') ?></button>
									<span class="spinner add_extra_price" style="display: none;"></span>
								</th>
							</tr>
						</tfoot>
					  </table>
					</div>
				</div>
			</div>
			<div id="sale_price_total_day" class="panel awe_options_panel">
				<div class="options_group ">
					<div class="form-field">
					  <table class="field-multiple-table awe-add-option-table">
					   <thead>
							 <tr>
								<th colspan="5" class="field-label"><label>&nbsp; </label></th>
							</tr>
							<tr>
								<th class="field-label"><label><?php _e('Type','awebooking') ?> </label></th>
								<th class="field-label"><label><?php _e('Unit','awebooking') ?> </label></th>
								<th class="field-label"><label><?php _e('Sale type','awebooking') ?> </label></th>
								<th class="field-label"><label><?php _e('Amount','awebooking') ?> </label></th>
								<th class="field-label"><label><?php _e('Action','awebooking') ?> </label></th>
							</tr>
						</thead>
						<tbody class="form-extra-sale-js">
						<?php
							if(is_array($extra_sale)):
								$i = 0;
								foreach ($extra_sale as $item_extra_sale):
								$_i = $i++;
						?>
							<tr class="item-extra-sale">
								<td class="apb-select-sale-type">
									<input type="text" class="apb-input-type-duration-js input-duration-<?php echo esc_attr($_i) ?>" value="<?php echo esc_attr($item_extra_sale['type_duration']) ?>" data-int="<?php echo esc_attr($_i) ?>" name="sale_date[date][]" placeholder="Duration type" readonly >
									<div class="apb-sale-type type-duration-<?php echo esc_attr($_i) ?>">
										<p class="value-duration-js" data-int="<?php echo esc_attr($_i) ?>" data-type="Before-Day"><?php _e( 'Before day', 'awebooking' ); ?></p>
										<p class="value-duration-js" data-int="<?php echo esc_attr($_i) ?>" data-type="Day"><?php _e( 'Day', 'awebooking' ); ?></p>
										<p class="value-duration-js" data-int="<?php echo esc_attr($_i) ?>" data-type="Week"><?php _e( 'Week', 'awebooking' ); ?></p>
										<p class="value-duration-js" data-int="<?php echo esc_attr($_i) ?>" data-type="Month"><?php _e( 'Month', 'awebooking' ); ?></p>
									</div>
								</td>
								<td><input type="text" name="sale_date[total][]" value="<?php echo esc_attr($item_extra_sale['total']) ?>"></td>
								<td class="apb-select-sale-type">
									<?php $list_type_sale = array('replace' => 'Replace price','sub' => 'Subtract from price', 'decrease' => 'Decrease price by %'); ?>
									<input type="text" value="<?php echo $list_type_sale[$item_extra_sale['sale_type']] ?>" class="apb-input-type-sale-js input-sale-<?php echo esc_attr($_i) ?>" data-int="<?php echo esc_attr($_i) ?>" placeholder="Sale type" readonly >

									<input type="hidden" class="input-sale-hidden-<?php echo esc_attr($_i) ?>" value="<?php echo $item_extra_sale['sale_type'] ?>" name="sale_date[type][]">
									<div class="apb-sale-type type-sale-<?php echo esc_attr($_i) ?>">
										<p class="value-type-js" data-int="<?php echo esc_attr($_i) ?>" data-type="sub" data-value="Subtract from price"><?php _e( 'Subtract from price', 'awebooking' ); ?></p>
										<p class="value-type-js" data-int="<?php echo esc_attr($_i) ?>" data-type="decrease" data-value="Decrease price by %"><?php _e( 'Decrease price by %', 'awebooking' ); ?></p>
									</div>
								</td>
								<td><input type="text" name="sale_date[price][]" value="<?php echo esc_attr($item_extra_sale['amount']) ?>"></td>
								<td><button class="button remove-extra-sale-js" type="button"><?php _e('Remove','awebooking') ?></button></td>
							</tr>
						<?php endforeach; endif; ?>
						</tbody>
						 <tfoot>
								<tr>
									<th  colspan="5">
										<button type="button" class="button add_sale_js"><?php _e('Add','awebooking') ?></button>
									</th>
								</tr>
							</tfoot>
					  </table>
					</div>
				</div>
			</div>
			<div id="desc_roomtype" class="panel awe_options_panel">
				<div class="options_group ">
					<p class="awe-form-inline">
						<label for="edit-label"><?php _e('Description','awebooking') ?></label>
						<select name="desc_room_type">
						 <?php  foreach (get_posts(array('post_type' => 'room_type', 'posts_per_page' => -1)) as $post_room_type): ?>
							<option <?php selected($post_room_type->ID,$object_id); ?> value="<?php echo esc_attr($post_room_type->ID) ?>"><?php echo $post_room_type->post_title ?></option>
						 <?php endforeach; ?>
						</select>
						<?php AWE_function::awe_help('Choose the room type description(Room type intro).') ?>
					</p>
				</div>
			</div>
		</div>

		<div class="save-room-type">
			<input type="hidden" name="action" value="<?php echo esc_attr($type_submit) ?>">
			<input type="hidden" name="room_type_id" value="<?php echo esc_attr($type_id) ?>">
			<button type="submit" class="button" ><?php _e('Save room type','awebooking') ?></button>
		</div>
	</form>
</div>
</div>
