<?php 

if(isset($_GET['cdelid'])){
	include("includes/db.conn.php"); 
	include("includes/conf.class.php");
	include("includes/admin.class.php");
	$bsiAdminMain->delete_capacity();
	header("location:admin.php?page=capacity-manager");
	exit;
}
//include("header.php"); 
include("includes/admin.class.php");
//wp_enqueue_script( 'custom_script10', plugins_url().'/bsi-hotel-pro/js/bsi_datatables.js');
wp_enqueue_script( 'custom_script11', plugins_url().'/bsi-hotel-pro/js/DataTables/jquery.dataTables.js');
wp_enqueue_style('custom-style9', plugins_url().'/bsi-hotel-pro/css/data.table.css');
wp_enqueue_style('custom-style10', plugins_url().'/bsi-hotel-pro/css/jqueryui.css');
?>
<script type="text/javascript">
function capacitydelete(cid){
	var ans=confirm('<?php echo DO_YOU_WANT_TO_DELETE_THE_SELECTED_CAPACITY_ALERT;?>');
	if(ans){
		window.location='admin.php?page=capacity-manager&noheader=true&cdelid='+cid;
		return true;
	}else{
		return false;
		
	}
}
</script>
<p>&nbsp;</p>
<div id="container-inside">
<span style="font-size:16px; font-weight:bold"><?php echo CAPACITY_LIST;?></span>
    <input type="button" value="<?php echo ADD_NEW_CAPACITY;?>" onClick="window.location.href='admin.php?page=aad-edit-capacity&id=0'" style="background: #EFEFEF; float:right"/>
<hr style="margin-top:10px;" />
   <table class="display datatable" border="0">
    <thead>
      <tr>
        <th width="20%"><?php echo CAPACITY_TITLE;?></th>
        <th width="20%"><?php echo NO_OF_ADULT_CAPACITY;?></th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    <?=$bsiAdminMain->generateCapacityListHtml()?>
  </table>
    
<!-- html will be generate here-->
      </table>
</div>
<script>
jQuery(document).ready(function() {
	 	var oTable = jQuery('.datatable').dataTable( {
				 "responsive": true,
				"bJQueryUI": true, 
				"sScrollX": "",
				"bSortClasses": false,
				"aaSorting": [[1,'asc']],
				"bAutoWidth": true,
				"bInfo": true,
				"sScrollY": "100%",	
				"sScrollX": "100%",
				"bScrollCollapse": true,
				"sPaginationType": "full_numbers",
				"bRetrieve": true,
				"oLanguage": {
								"sSearch": "Search:",
								"sInfo": "Showing _START_ to _END_ of _TOTAL_ entries",
								"sInfoEmpty": "Showing 0 to 0 of 0 entries",
								"sZeroRecords": "No matching records found",
								"sInfoFiltered": "(filtered from _MAX_ total entries)",
								"sEmptyTable": "No data available in table",
								"sLengthMenu": "Show _MENU_ entries",
								"oPaginate": {
												"sFirst":    "First",
												"sPrevious": "Previous",
												"sNext":     "Next",
												"sLast":     "Last"
											  }
							 }
	} );
} );
</script> 
</div>