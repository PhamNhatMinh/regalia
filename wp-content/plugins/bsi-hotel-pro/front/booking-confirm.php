<?php
session_destroy();

//wp_enqueue_style('style-pro', plugins_url().'/bsi-hotel-pro/front/css/prostyle11.css'); 
?>
 <link rel="stylesheet" href="<?=plugins_url().'/bsi-hotel-pro/front/css/prostyle11.css';?>" type="text/css">       
						<div class="wizard">
                     	<div class="step1 greenStep2"><img alt="" src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/1.png"><p><?php echo SELECT_DATES_TEXT; ?></p></div>
                            <div class="step2 greenStep2"><img alt="" src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/2.png"><p><?php echo ROOMS_TEXT; ?> &amp; <?php echo RATES_TEXT; ?></p></div>
                            <div class="step3 greenStep2"><img alt="" src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/3.png"><p><?php echo YOUR_DETAILS_TEXT; ?></p></div>
                            <div class="step4 greenStep"><img alt="" src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/4.png"><p><?php echo PAYMENT_TEXT; ?></p></div>
                            <div class="step5 yellowLastStep"><img alt="" src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/5.png"><p><?php echo CONFIRM_TEXT; ?></p></div>
                     </div>
                     <div class="progress">
						<div class="bar bar-success" style="width: 100%;">100% Complete</div>
                     </div>
                        <div class="wrapper">
                            <div class="htitel">
                               
                            </div>
                            <!-- start of search row --> 
                            <div class="container-fluid" style="margin:0; padding:0;">
                                <div class="row-fluid" style="padding: 1% 0">
                                    <div class="span12" style="text-align:center; padding:20px 0">
                                    	<p><strong><?php echo THANK_YOU_TEXT; ?>!</strong></p>
                                        <p><?php echo YOUR_BOOKING_CONFIRMED_TEXT; ?>. <?php echo INVOICE_SENT_EMAIL_ADDRESS_TEXT; ?>.</p>
                                         <div style="width:100%;">
                                            <button id="registerButton" style="margin:0 auto" type="button" onClick="window.location.href='<?php the_permalink(); ?>'" ><?php echo BACK_TO_HOME; ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                            <!-- end of search row --> 
                        </div>
<?php

?>