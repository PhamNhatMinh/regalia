<?php  
if(isset($_POST['submitRoomtype'])){
	include("includes/db.conn.php"); 
	include("includes/conf.class.php");
	include("includes/admin.class.php");
	$bsiAdminMain->add_edit_roomtype(); 
	header("location:admin.php?page=room-type-manager");	
	exit;
}

include("includes/conf.class.php");
include("includes/admin.class.php");
global $wpdb;
if(isset($_GET['id']) && $_GET['id'] != ""){
	$id = $bsiCore->ClearInput($_GET['id']);
	if($id){
		$row    = $wpdb->get_row($bsiAdminMain->getRoomtypesql($id),ARRAY_A);
		
		$readonly = 'readonly="readonly"';
	}else{
		$row    = NULL;
		$readonly = '';
	}
}else{
	header("location:admin.php?page=room-type-manager");
	exit;
}
?>  
 <link rel="stylesheet" type="text/css" href="css/jquery.validate.css" />
      <div id="container-inside">
      <span style="font-size:16px; font-weight:bold"><?php echo ROOM_TYPE_ADD_AND_EDIT;?></span>
      <hr />
          <form action="<?php echo admin_url('admin.php?page=aad-new-room-type&noheader=true'); ?>" method="post" id="form1" enctype="multipart/form-data">
          <table cellpadding="5" cellspacing="2" border="0">
            <tr>
              <td><strong><?php echo ROOM_TYPE_TITLE;?>:</strong></td>
              <td valign="middle"><input type="text" name="roomtype_title" id="roomtype_title" class="required" value="<?=$row['type_name']?>" style="width:250px;" /> &nbsp;&nbsp;<?php echo EXAMPLE_DELUXE_AND_STANDARD;?></td>
            </tr>
            <tr>
              <td valign="top"><strong><?php echo DESCRIPTION_FACILITIES_TEXT; ?>:</strong></td>
              <td><textarea rows="5" cols="3" name="description" id="description" style="width:700px; height:150px;"><?php echo $row['description'];?></textarea></td>
            </tr>
            <tr>           
              <td><input type="hidden" name="addedit" value="<?=$id?>"></td>
              <td><input type="submit" value="<?php echo ROOM_TYPE_SUBMIT;?>" name="submitRoomtype" style="background:#e5f9bb; cursor:pointer; cursor:hand;"/></td>
            </tr>
          </table>
        </form>
      </div>
<script type="text/javascript">
	jQuery().ready(function() { jQuery("#form1").validate(); });       
</script>      


