<?php
if(isset($_POST['act']) && $_POST['act'] == 1){
	include("includes/conf.class.php");
	include("includes/admin.class.php");
	if($_POST['roomtype_edit'] > 0){
		$bsiAdminMain->priceplan_edit($_POST['roomtype_edit']);
	}else{
		$bsiAdminMain->priceplan_add_edit(); 
	}
	exit;
}

include("includes/conf.class.php");
include("includes/admin.class.php");
global $wpdb;
$getHTML  = array();
$getHTML1 = array();
$getHTML2 = array();
$row      = array();
$id=$bsiCore->ClearInput($_REQUEST['rtype']);
if($id){
	$text     = '';
	$start_dt = addslashes($_REQUEST['start_dt']);
	if($start_dt != '0000-00-00'){
		
		$row=$wpdb->get_row("SELECT DATE_FORMAT(start_date, '".$bsiCore->userDateFormat."') AS start_date1,
		DATE_FORMAT(end_date, '".$bsiCore->userDateFormat."') AS end_date1, start_date, end_date,roomtype_id,capacity_id,sun,mon,tue,wed,thu,fri,sat FROM `bsi_priceplan`
		where `plan_id`='".$id."' and start_date='".$start_dt."' and default_plan=0",ARRAY_A);
		
	}else{
		
		$row = $wpdb->get_row("SELECT DATE_FORMAT(start_date, '".$bsiCore->userDateFormat."') AS start_date1,
		DATE_FORMAT(end_date, '".$bsiCore->userDateFormat."') AS end_date1, start_date, end_date,roomtype_id,capacity_id,sun,mon,tue,wed,thu,fri,sat FROM `bsi_priceplan`  
		where `plan_id`='".$id."' and start_date='".$start_dt."' and default_plan=1",ARRAY_A);
		
	}
	$rtypeName = $wpdb->get_row("select * from bsi_roomtype where roomtype_ID='".$row['roomtype_id']."'",ARRAY_A);
	
	$getHTML   = $bsiAdminMain->getDatepicker($id, $rtypeName['type_name'], $row['start_date'], $row['end_date'], $row);
	
	$getHTML1  = $getHTML['html'];
	
	$getHTML2  = $getHTML['editPriceplanHTML'];
	
	$text      = '';
	
}else{
	
	$getHTML   = $bsiAdminMain->getDatepicker();
	
	$getHTML1  = $getHTML['html'];
	
	$getHTML2  = $getHTML['editPriceplanHTML'];
	
	$start_dt  = '0000-00-00';
	
	$text      = PLEASE_SELECT_ROOMTYPE_FROM_DROPDOWN;
}
?>
<script type="text/javascript" charset="">
   jQuery(document).ready(function() {
   jQuery("#priceplanaddeit").validate();
   jQuery('#roomtype_id').change(function() {
         if(jQuery('#roomtype_id').val() != 0){
			var querystr = 'actioncode=3&roomtype_id='+jQuery('#roomtype_id').val();		
			jQuery.post("<?php echo plugins_url(); ?>/bsi-hotel-pro/admin_ajax_processor.php", querystr, function(data){												 
				if(data.errorcode == 0){
					 jQuery('#default_capacity').html(data.strhtml)
				}else{
				    jQuery('#default_capacity').html("<span style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px;\">'<?php  echo NOT_FOUND;?>'</span>")
				}
			}, "json");
		} else {
		 jQuery('#default_capacity').html("<span style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px;\">'<?php echo PLEASE_SELECT_ROOMTYPE_FROM_DROPDOWN_ALERT; ?>'</span>")
		}
	});
	
	if(jQuery('#roomtype').val() == 0){
		jQuery('#default_capacity').html("<span style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px;\">'<?php echo PLEASE_SELECT_ROOMTYPE_FROM_DROPDOWN_ALERT; ?>'</span>")
	}
});
 
jQuery(document).ready(function(){
	
	jQuery.datepicker.setDefaults({ dateFormat: '<?php echo $bsiCore->config['conf_dateformat']?>' });
    jQuery("#txtFromDate").datepicker({
        minDate: 0,
        maxDate: "+<?php echo $bsiCore->config['conf_maximum_global_years']; ?>D",
        numberOfMonths: 2,
        onSelect: function(selected) {
        var date = jQuery(this).datepicker('getDate');
        if(date){
            date.setDate(date.getDate() + <?php echo $bsiCore->config['conf_min_night_booking']?>);
        }
          jQuery("#txtToDate").datepicker("option","minDate", date)
        }
    });
    jQuery("#txtToDate").datepicker({ 
        minDate: 0,
        maxDate:"+<?php echo $bsiCore->config['conf_maximum_global_years']; ?>D",
        numberOfMonths: 2,
        onSelect: function(selected) {
           jQuery("#txtFromDate").datepicker("option","maxDate", selected)
        }
    });
	
	jQuery("#txtFromDate").datepicker();
	jQuery("#datepickerImage").click(function() { 
		jQuery("#txtFromDate").datepicker("show");
	});
	
	jQuery("#txtToDate").datepicker();
	jQuery("#datepickerImage1").click(function() { 
		jQuery("#txtToDate").datepicker("show");
	});    
});
</script>
<br />

<div id="container-inside" style="width:80%">
<span style="font-size:16px; font-weight:bold"><?php echo PRICE_PLAN_ADD_AND_EDIT;?></span>
    <input type="button" value="<?php echo PRICE_PLAN_BACK;?>" onClick="window.location.href='admin.php?page=price-plan'" style="background:#e5f9bb; cursor:pointer; cursor:hand; float:right; "/>
<hr style="margin-top:10px;" />
  <form action="<?php echo admin_url('admin.php?page=add-price-plan&noheader=true'); ?>" method="post" id="form1">
    <input type="hidden" name="roomtype_edit" value="<?php echo $id?>" />
    <input type="hidden" name="roomtype" value="<?php echo $row['roomtype_id']?>" />
    <input type="hidden" name="start_date_old" value="<?php echo $start_dt?>" />
    <input type="hidden" name="act" value="1" />
    <table cellpadding="5" cellspacing="2" border="0" style="width:450px;">
      <tr>
        <td colspan="2" align="center" style="font-size:14px; color:#006600; font-weight:bold"><?php if(isset($error_msg)) echo $error_msg; ?></td>
      </tr>
      <?php echo $getHTML1?>
      <tr>
        <td id="default_capacity" colspan="2">
          <?php echo $text?>
          <?php echo $getHTML2?>
        </td>
      </tr> 
    </table>
  </form>
</div>   
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#form1").validate();
     });    
</script> 

