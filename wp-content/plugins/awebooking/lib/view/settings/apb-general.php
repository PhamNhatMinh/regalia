<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 *  Tempalte setting default room setting
 *
 * @version     1.0
 * @package     AweBooking/admin/
 * @author      AweTeam
 */

?>
<div class="form-elements">
	<p><?php esc_html_e( 'How soon a booking can start', 'awebooking' ); ?></p>
	<select name="rooms_booking_start_date">
		<option value="0"><?php esc_html_e( 'Same day bookings', 'awebooking' ); ?></option>
		<?php for ( $i = 1; $i <= 10; $i++ ) : ?>
			<option <?php selected( $i, get_option( 'rooms_booking_start_date' ) ); ?> value="<?php echo esc_attr( $i ); ?>"><?php esc_html_e( 'Day in advance', 'awebooking' ); ?> <?php echo absint( $i ); ?></option>
		<?php endfor; ?>
	</select>
	<span class="description">
		<?php esc_html_e( 'Current date + some date.', 'awebooking' ); ?>
	</span>
</div>
<div class="form-elements">
	<p><?php esc_html_e( 'Max Room', 'awebooking' ); ?></p>
	<input type="number" value="<?php echo absint( get_option( 'max_room' ) ) ?>" name="max_room">
	<span class="description">
		<?php esc_html_e( 'Number of displayed room in check availability.', 'awebooking' ); ?>
	</span>
</div>

<div class="form-elements">
	<p><?php esc_html_e( 'Max adult', 'awebooking' ); ?></p>
	<input type="number" value="<?php echo absint( get_option( 'max_adult' ) ) ?>" name="max_adult">
	<span class="description">
		<?php esc_html_e( 'Number of adult.', 'awebooking' ); ?>
	</span>
</div>

<div class="form-elements">
	<p><?php esc_html_e( 'Max Child', 'awebooking' ); ?></p>
	<input type="number" value="<?php echo absint( get_option( 'max_child' ) ) ?>" name="max_child">
	<span class="description">
		<?php esc_html_e( 'Number of child.', 'awebooking' ); ?>
	</span>
</div>

<div class="form-elements">
	<p><?php esc_html_e( 'Max Night', 'awebooking' ); ?></p>
	<input type="number" value="<?php echo absint( get_option( 'max_night' ) ) ?>" name="max_night">
	<span class="description">
		<?php esc_html_e( 'Number of night.', 'awebooking' ); ?>
	</span>
</div>

<div class="form-elements">
	<p><?php esc_html_e( 'Currency', 'awebooking' ); ?></p>

	<select name="woocommerce_currency" class="form-control">
		<?php foreach ( AWE_function::list_currencies() as $key => $val ) { ?>
			<option <?php selected( get_option( 'woocommerce_currency' ), $key ); ?> value="<?php echo esc_attr( $key ); ?>"><?php echo esc_attr( $val . ' (' . AWE_function::get_currency( $key ) . ')' ); ?></option>
		<?php } ?>
	</select>
</div>

<div class="form-elements">
	<?php
	$currency_pos = get_option( 'woocommerce_currency_pos' ) ? esc_attr( get_option( 'woocommerce_currency_pos' ) ) : 'left';
	$currency = AWE_function::get_currency( get_option( 'woocommerce_currency' ) );
	?>
	<p><?php esc_html_e( 'Currency Position', 'awebooking' ); ?></p>
	<select name="woocommerce_currency_pos">
		<option value="left" <?php selected( $currency_pos, 'left' ); ?>><?php printf( esc_html__( 'Left (%s)', 'awebooking' ), esc_attr( $currency ) . '99.99' ); ?></option>
		<option value="right" <?php selected( $currency_pos, 'right' ); ?>><?php printf( esc_html__( 'Right (%s)', 'awebooking' ), '99.99' . esc_attr( $currency ) ); ?></option>
		<option value="left_space" <?php selected( $currency_pos, 'left_space' ); ?>><?php printf( esc_html__( 'Left with space (%s)', 'awebooking' ), esc_attr( $currency ) . ' 99.99' ); ?></option>
		<option value="right_space" <?php selected( $currency_pos, 'right_space' ); ?>><?php printf( esc_html__( 'Right with space (%s)', 'awebooking' ), '99.99 ' . esc_attr( $currency ) ); ?></option>
	</select>
</div>

<div class="form-elements">
	<p><?php esc_html_e( 'Thousand Separator', 'awebooking' ); ?></p>
	<input name="woocommerce_price_thousand_sep" type="text" value="<?php echo esc_attr( get_option( 'woocommerce_price_thousand_sep' ) ); ?>" style="width:50px;min-width:auto">
</div>

<div class="form-elements">
	<p><?php esc_html_e( 'Decimal Separator', 'awebooking' ); ?></p>
	<input name="woocommerce_price_decimal_sep" type="text" value="<?php echo esc_attr( get_option( 'woocommerce_price_decimal_sep' ) ); ?>" style="width:50px;min-width:auto">
</div>

<div class="form-elements">
	<p><?php esc_html_e( 'Number of Decimals', 'awebooking' ); ?></p>
	<input name="woocommerce_price_num_decimals" type="number" value="<?php echo esc_attr( get_option( 'woocommerce_price_num_decimals' ) ); ?>" style="width:50px;min-width:auto;margin-left:0">
</div>

<div class="form-elements">
	<div class="form-radios">
		<input id="_booking_pending" class="apb_departure" name="_booking_pending" <?php checked( get_option( '_booking_pending' ), 1, true ); ?> type="checkbox" value="1">
		<label for="_booking_pending"><?php esc_html_e( 'Prevent book on pending date', 'awebooking' ); ?></label>
	</div>
</div>

<div class="form-elements">
	<div class="form-radios">
		<input id="apb_show_single_calendar" class="apb_departure" name="apb_show_single_calendar" <?php checked( get_option( 'apb_show_single_calendar' ), 1, true ); ?> type="checkbox" value="1">
		<label for="apb_show_single_calendar"><?php esc_html_e( 'Show calendar in single room type', 'awebooking' ); ?></label>
	</div>
</div>
