<?php
/**
 * The template for displaying loop complated select room
 *
 * Override this template by copying it to your theme
 *
 * @author  AweTeam
 * @package AweBooking/Templates
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<!-- ITEM -->
<div class="apb-room-select-item">
	<div class="img">
		<?php echo get_the_post_thumbnail( $room_type_id, 'thumbnail', false ); ?>
	</div>
	<div class="apb-desc">
		<span class="room-select-th"><?php printf( esc_html__( 'Room %s', 'awebooking' ), absint( $room_num ) ); ?></span>

		<h4><?php echo esc_html( $room->post_title ); ?></h4>

		<p>
			<?php
			printf( esc_html__( '%s Adult', 'awebooking' ), absint( $item['adult'] ) );
			echo ', ';
			printf( esc_html__( '%s Child', 'awebooking' ), absint( $item['child'] ) );
			?>
			<br/>
		</p>

		<p>
			<?php echo esc_html( date_i18n( get_option( 'date_format' ), strtotime( $item['from'] ) ) ); ?>
			<i class="fa fa-long-arrow-right"></i>
			<?php echo esc_html( date_i18n( get_option( 'date_format' ), strtotime( $item['to'] ) ) ); ?>
		</p>
	</div>

	<?php
	$package_price = 0;
	$info_price_day = AWE_function::get_pricing_of_days( $item['from'], $item['to'], $room_type_id, 1 );
	$total_day_price = AWE_function::get_total_day_price( $info_price_day );
	$extra_adult  = get_post_meta( $room_type_id, 'extra_adult', true );
	$extra_child  = get_post_meta( $room_type_id, 'extra_child', true );
	?>
	<div class="apb-room-select-package">
		<span class="room-select-th"><?php esc_html_e( 'Price', 'awebooking' ); ?></span>
		<ul>
			<li>
				<?php printf( esc_html__( '%s night', 'awebooking' ), absint( $total_night ) ); ?>
				<span><?php echo wp_kses_post( AWE_function::apb_price( $room_price ) ); ?> x <?php echo absint( count( $range_date ) - 1 ); ?></span>
			</li>

			<?php
			if ( ! empty( $extra_adult ) && is_array( $extra_adult ) ) {
				foreach ( $extra_adult as $item_extra_adult ) {
					if ( $item['adult'] == $item_extra_adult['number'] ) {
						?>
						<li>
							<?php printf( esc_html__( '%s Adult', 'awebooking' ), absint( $item_extra_adult['number'] ) ); ?> + <?php printf( esc_html__( '%s/night', 'awebooking' ), wp_kses_post( AWE_function::apb_price( $item_extra_adult['price'] ) ) ); ?></span>
							<span><?php echo wp_kses_post( AWE_function::apb_price( $item_extra_adult['price'] ) ) ?> × <?php echo absint( count( $range_date ) - 1 ); ?></span>
						</li>
						<?php
					}
				}
			}
			?>

			<?php if ( ! empty( $extra_child ) && is_array( $extra_child ) ) { ?>
				<?php
				foreach ( $extra_child as $item_extra_child ) {
					if ( $item['child'] == $item_extra_child['number'] ) {
						?>
						<li>
							<?php printf( esc_html__( '%s Child', 'awebooking' ), absint( $item_extra_child['number'] ) ); ?> + <?php printf( esc_html__( '%s/night', 'awebooking' ), wp_kses_post( AWE_function::apb_price( $item_extra_child['price'] ) ) ); ?>
							<span><?php echo wp_kses_post( AWE_function::apb_price( $item_extra_child['price'] ) ) ?> × <?php echo absint( count( $range_date ) - 1 ); ?></span>
						</li>
						<?php
					}
				}
				?>
			<?php } ?>

			<?php if ( ! empty( $item['sale_info'] ) ) { ?>
				<li>
					<?php
					if ( 'sub' == $item['sale_info']['sale_type'] ) {
						echo esc_html__( 'Sale', 'awebooking' ) . ' <span class="apb-amount"> ' . esc_html( AWE_function::get_symbol_of_sale( $item['sale_info']['sale_type'] ) ) . wp_kses_post( AWE_function::apb_price( $item['sale_info']['amount'] ) ) . '</span>';
					} else {
						echo esc_html__( 'Sale', 'awebooking' ) . ' <span class="apb-amount">  -' . ( float ) $item['sale_info']['amount'] . esc_html( AWE_function::get_symbol_of_sale( $item['sale_info']['sale_type'] ) ) . '</span>';
					}
					?>
				</li>
			<?php } ?>

			<?php
			if ( ! empty( $item['package_data'] ) ) {
				foreach ( $item['package_data'] as $info_package ) {
					$getPackage = AWE_function::get_room_option( $room_type_id, 'apb_room_type' );
					foreach ( $getPackage as $item_package ) {
						if ( $item_package->id == $info_package['package_id'] ) {
							$package_price += $info_package['total'] * $item_package->option_value;
							?>
							<li>
								<?php echo esc_html( $item_package->option_name ); ?>
								<span><?php echo absint( $info_package['total'] ) ?> × <?php echo wp_kses_post( AWE_function::apb_price( $item_package->option_value ) ); ?></span>
							</li>
							<?php
						}
					}
				}
			}
			?>
		</ul>
	</div>

	<div class="apb-room-select-price">
		<span class="room-select-th"><?php esc_html_e( 'Total', 'awebooking' ); ?></span>
		<span class="price"><?php echo wp_kses_post( AWE_function::apb_price( $item['price'] ) ); ?></span>
	</div>

</div>
<!-- ITEM -->
