<?php


include(WP_PLUGIN_DIR."/bsi-hotel-pro/includes/conf.class.php");
include("language.php");
wp_enqueue_script( 'custom_script5', plugins_url().'/bsi-hotel-pro/front/js/jquery.validate.js');
wp_enqueue_style('style-pro', plugins_url().'/bsi-hotel-pro/front/css/prostyle11.css'); 

?>
<script id="demo" type="text/javascript">
jQuery(document).ready(function() {
	jQuery("#form1").validate();
});
</script>


						<div class="wizard">
                        <div class="step1 greenStep2"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/1.png" alt="" /><p><?php echo SELECT_DATES_TEXT; ?></p></div>
                        <div class="step2 greenStep2"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/2.png" alt="" /><p><?php echo RATES_TEXT; ?></p></div>
                        <div class="step3 greenStep"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/3.png" alt="" /><p><?php echo YOUR_DETAILS_TEXT; ?></p></div>
                        <div class="step4 yellowStep"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/4.png" alt="" /><p><?php echo PAYMENT_TEXT; ?></p></div>
                        <div class="step5 grayLastStep"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/5.png" alt="" /><p><?php echo CONFIRM_TEXT; ?></p></div>
                       
     
                     </div>
                     <div class="progress">
						<div class="bar bar-success" style="width: 60%;">60% Complete</div>
						 	<div class="bar bar-warning" style="width: 20%;"></div>
                     </div>
                       
                        <div class="wrapper">
                            <div class="htitel"> 
                                <h2 class="fl" style="border:0; margin:0;"><?php echo CC_DETAILS; ?></h2>
                            </div>
                            
                            <!-- start of search row --> 
                            <div class="container-fluid" style="margin:0; padding:0;">
                                <div class="row-fluid" style=" padding: 1% 0">
                                    <div class="span12">
                                       <form name="signupform" id="form1"  action="" method="post" onSubmit="return testCreditCard(); style="width: 95%; margin: 0 2.5%" class="form-horizontal">
                                       <input type="hidden" name="bookingid" value="<?php echo $_POST['x_invoice_num']; ?>" />
                                            <div class="control-group">
                                                <label class="control-label" for="ln"><?php echo CC_HOLDER; ?>:</label>
                                                <div class="controls">
                                                    <input type="text" name="cc_holder_name" id="cc_holder_name" class="input-large required" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="ln"><?php echo CC_TYPE; ?>:</label>
                                                <div class="controls">
                                                    <select name="CardType" id="CardType" class="input-large">
                                                        <option value="AmEx">AmEx</option>
                                                        <option value="DinersClub">DinersClub</option>
                                                        <option value="Discover">Discover</option>
                                                        <option value="JCB">JCB</option>
                                                        <option value="Maestro">Maestro</option>
                                                        <option value="MasterCard">MasterCard</option>
                                                        <option value="Solo">Solo</option>
                                                        <option value="Switch">Switch</option>
                                                        <option value="Visa">Visa</option>
                                                        <option value="VisaElectron">VisaElectron</option>
                                                      </select>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="ccn"><?php echo CC_NUMBER; ?>:</label>
                                                <div class="controls">
                                                    <input type="text" name="CardNumber" id="CardNumber" maxlength="16" class="input-large required" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="ed"><?php echo CC_EXPIRY; ?> (mm/yy):</label>
                                                <div class="controls">
                                                    <input type="text" name="cc_exp_dt" id="cc_exp_dt" maxlength="5" class="input-mini required" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="cc">CCV/CCV2:</label>
                                                <div class="controls">
                                                    <input type="text" name="cc_ccv" id="cc_ccv"  maxlength="4" class="input-mini required"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="ad"><?php echo CC_AMOUNT; ?></label>
                                                <div class="controls"><strong><?php echo (($bsiCore->config['conf_payment_currency']=='1')? $bsiCore->get_currency_symbol($_SESSION['sv_currency']) : $bsiCore->currency_symbol())?><?php echo $_POST['total']; ?></strong><br />
                                                    <label class="checkbox">
                                                        <input type="checkbox" name="tos" id="tos" value="" class="required"/> <?php echo CC_TOS1; ?> <?php echo $bsiCore->config['conf_hotel_name']?>
               <?php echo CC_TOS2; ?> <?php echo $bsiCore->currency_symbol(); ?><?php echo $_POST['total']; ?> <?php echo CC_TOS3; ?>.
                                                    </label>
                                                </div>
                                            </div>                                        
                                    </div>
                                </div>
                            </div>  
                            <!-- end of search row --> 
                        </div>
                        <div class="wrapper" style="margin-bottom: 20px">
                            <div class="container-fluid" style="margin:0; padding:0;">
                                <div class="row-fluid" style="padding: 1% 0">
                                    <div class="span12">
                                        <div class="back1" style="display: inline-block;
    float: left;
    margin-left: 2.5%;
    width: 30%;">
                                            <button id="registerButton" type="button" onClick="window.location.href='<?php echo the_permalink(); ?>'" ><?php echo BTN_CANCEL; ?></button>
                                        </div>
                                       
                                        <div style="display: inline-block;
    float: right;
    margin-right: 2.5%;
    " class="continue1">
                                            <button id="registerButton" type="submit" class="conti" ><?php echo CC_SUBMIT; ?></button>
                                        </div>
                                    </div>
                                </div> 
                            </div>   
                        </div>
                        </form>
                    </div>
            