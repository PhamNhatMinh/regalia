<?php
/*if(!isset($_SESSION['bookingId'])){
	header('Location: booking-failure.php?error_code=9');
}
*/
//include("includes/db.conn.php");
include(WP_PLUGIN_DIR."/bsi-hotel-pro/includes/conf.class.php");
include(WP_PLUGIN_DIR."/bsi-hotel-pro/includes/mail.class.php");
include("language.php");
global $wpdb;
$bsiCore = new bsiHotelCore;
$bsiMail = new bsiMail();
$paymentGatewayDetails = $bsiCore->loadPaymentGateways();	
$emailContent=$bsiMail->loadEmailContent();
$st_account=explode("#",$paymentGatewayDetails['st']['account']);

require_once('stripe/Stripe.php');
$stripe = array(
'secret_key'      => $st_account[0],
'publishable_key' => $st_account[1]
);
Stripe::setApiKey($stripe['secret_key']); 

if (isset($_SESSION['submit'])) {
	unset($_SESSION['submit']);
	
    $error = NULL;
    try {
      if (!isset($_POST['stripeToken']))
        throw new Exception("The Stripe Token was not generated correctly");
		  
		  if($st_account[2]){
		  $customer = Stripe_Customer::create(array(
			  "card" => $_POST['stripeToken'],
			  "email" => $_SESSION['clientEmail'],
			  "description"=>"Booking#".$_SESSION['bookingId']
			  ));
			  
			Stripe_Charge::create(array(
			  "amount" => round(($_SESSION['paymentAmount']*100)), # amount in cents, again
			  "currency" => $bsiCore->currency_code(),
			  "customer" => $customer->id)
			);
			
		  }else{
			  
			   $customer = Stripe_Customer::create(array(
			  "card" => $_POST['stripeToken'],
			  "email" => $_SESSION['clientEmail'],
			  "description"=>"Booking#".$_SESSION['bookingId']
			  ));
			  
			 
		  }
			
    }
    catch (Exception $e) {
      $error = $e->getMessage();
    }

    if ($error == NULL) {
	//*****************************************************************************************
			   	$wpdb->query("UPDATE bsi_bookings SET payment_success=true WHERE booking_id='".$_SESSION['bookingId']."'");
	
				$invoiceROWS = $wpdb->get_row("SELECT client_name, client_email, invoice FROM bsi_invoice WHERE booking_id='".$_SESSION['bookingId']."'",ARRAY_A);
				$wpdb->query("UPDATE bsi_clients SET existing_client = 1 WHERE email='".$invoiceROWS['client_email']."'");
				
				$invoiceHTML = $invoiceROWS['invoice'];		
				$invoiceHTML.= '<br><br><table  style="font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;" cellpadding="4" cellspacing="1"><tr><td align="left" colspan="2" style="font-weight:bold; font-variant:small-caps; background:#eeeeee">'.addslashes(INV_PAY_DETAILS).'</td></tr><tr><td align="left" width="30%" style="font-weight:bold; font-variant:small-caps; background:#ffffff">'.addslashes(INV_PAY_OPTION).'</td><td align="left" style="background:#ffffff">'.$paymentGatewayDetails['st']['name'].'</td></tr></table>';
				
				$wpdb->query("UPDATE bsi_invoice SET invoice = '$invoiceHTML' WHERE booking_id='".$_SESSION['bookingId']."'");
				
				
				
				$emailBody = "Dear ".$invoiceROWS['client_name'].",<br><br>";
				$emailBody .= html_entity_decode($emailContent['body'])."<br><br>";
				$emailBody .= $invoiceHTML;
				$emailBody .= "<br><br>".addslashes(PP_REGARDS).",<br>".$bsiCore->config['conf_hotel_name'].'<br>'.$bsiCore->config['conf_hotel_phone'];
				$emailBody .= "<br><br><font style=\"color:#F00; font-size:10px;\">[ ".addslashes(PP_CARRY)." ]</font>";
				$flag = 1;
				$bsiMail->sendEMail($invoiceROWS['client_email'], $emailContent['subject'], $emailBody, $_SESSION['bookingId'], $flag);
				
				/* Notify Email for Hotel about Booking */
				$notifyEmailSubject = "Booking no.".$_SESSION['bookingId']." - Notification of Room Booking by ".$invoiceROWS['client_name'];
				
				$bsiMail->sendEMail($bsiCore->config['conf_notification_email'], $notifyEmailSubject, $invoiceHTML);
			//*****************************************************************************************	
	
     //header('Location: booking-confirm.php?success_code=1');
	 include('booking-confirm.php');	 
	 die;
    }
    else {
		//header('Location: booking-failure.php?error_code=99&rescode='.htmlentities($error));
		echo "<script>
			alert('".htmlentities($error)."');
			</script>	
		";
    }
  }
  wp_enqueue_script('custom_script', plugins_url().'/bsi-hotel-pro/front/js/jquery.validate.js');
  wp_enqueue_style('style-pro', plugins_url().'/bsi-hotel-pro/front/css/prostyle11.css'); 
  
?>

        <script id="demo" type="text/javascript">
			jQuery(document).ready(function() {
				jQuery("#payment-form").validate();
			});
			</script>
			<script type="text/javascript" src="https://js.stripe.com/v1/"></script>
			  <script type="text/javascript">
				// this identifies your website in the createToken call below
				Stripe.setPublishableKey("<?php echo $stripe['publishable_key']; ?>");
			
				function stripeResponseHandler(status, response) {
					if (response.error) {
						// re-enable the submit button
						jQuery('#registerButton1').removeAttr("disabled");
						// show the errors on the form
						//$(".payment-errors").html(response.error.message);
						alert(response.error.message);
					} else {
						var form$ = jQuery("#payment-form");
						// token contains id, last4, and card type
						var token = response['id'];
						// insert the token into the form so it gets submitted to the server
						form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
								// and submit
						form$.get(0).submit();
					}
				}
			
				jQuery(document).ready(function() {
					jQuery("#payment-form").submit(function(event) {
						// disable the submit button to prevent repeated clicks
						jQuery('#registerButton1').attr("disabled", "disabled");
						// createToken returns immediately - the supplied callback submits the form if there are no errors
						Stripe.createToken({
							number: jQuery('#card-number').val(),
							cvc: jQuery('#card-cvc').val(),
							exp_month: jQuery('#card-expiry-month').val(),
							exp_year: jQuery('#card-expiry-year').val()
						}, stripeResponseHandler);
						return false; // submit from callback
					});
				});

    </script>
        
        
        			
                     
                     
                     <div class="wizard">
                     	 <div class="step1 greenStep2"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/1.png" alt="" /><p><?php echo SELECT_DATES_TEXT; ?></p></div>
                        <div class="step2 greenStep2"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/2.png" alt="" /><p><?php echo ROOMS_TEXT; ?> &amp; <?php echo RATES_TEXT; ?></p></div>
                        <div class="step3 greenStep"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/3.png" alt="" /><p><?php echo YOUR_DETAILS_TEXT; ?></p></div>
                        <div class="step4 yellowStep"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/4.png" alt="" /><p><?php echo PAYMENT_TEXT; ?></p></div>
                        <div class="step5 grayLastStep"><img src="<?php echo plugins_url(); ?>/bsi-hotel-pro/front/images/wizard/5.png" alt="" /><p><?php echo CONFIRM_TEXT; ?></p></div>
                     </div>
                     <div class="progress">
						<div class="bar bar-success" style="width: 60%;">60% Complete</div>
						 	<div class="bar bar-warning" style="width: 20%;"></div>
                     </div>
                         
                        <div class="wrapper">
                            <div class="htitel">
                                <h2 class="fl" style="border:0; margin:0;"><?php echo CC_DETAILS; ?> (Stripe)</h2>
                            </div>
                            <!-- start of search row --> 
                            <div class="container-fluid" style="margin:0; padding:0;">
                                <div class="row-fluid" style="padding: 1% 0">
                                    <div class="span12">
                                        <form name="signupform" id="payment-form"   action="" method="post" class="form-horizontal" style="width: 95%; margin: 0 2.5%">                                            
                                            <div class="control-group">
                                                <label class="control-label" for="ccn"><?php echo CC_NUMBER; ?>:</label>
                                                <div class="controls">
                                                    <input type="text" autocomplete="off" name="card-number" id="card-number" maxlength="16" class="input-large digits">
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="ed"><?php echo CC_EXPIRY; ?> (mm/yyyy):</label>
                                                <div class="controls">
                                                    <input type="text" name="card-expiry-month" id="card-expiry-month" maxlength="2" class="input-mini digits">/<input type="text" name="card-expiry-year" id="card-expiry-year" maxlength="4" class="input-mini required digits" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="cc">CCV/CCV2:</label>
                                                <div class="controls">
                                                    <input type="text" autocomplete="off" name="card-cvc"  id="card-cvc" maxlength="4" class="input-mini  number">
                                                    <?php $_SESSION['submit'] = "submit"; ?>
                                                </div>
                                            </div>
                                          <div class="control-group">
                                                <label class="control-label" for="ad"><?php echo CC_AMOUNT; ?></label>
                                                <div class="controls"><strong><?php echo $bsiCore->currency_symbol().$_SESSION['paymentAmount']; ?></strong><br />
                                                    <label class="checkbox">
                                                        <input type="checkbox" name="tos" id="tos" value="" class="required">
                                                         <input type="hidden" name="ansubmit" value="ansubmit" id="ansubmit" >
                                                        <?php echo CC_TOS1; ?> <?php echo $bsiCore->config['conf_hotel_name']; ?>
               <?php echo CC_TOS2; ?> <?php echo $bsiCore->currency_symbol().$_SESSION['paymentAmount']; ?> <?php echo CC_TOS3; ?>.</a>
                                                    </label>
                                                </div>
                                            </div>
                                          <!-- end of search row -->
                            <div class="wrapper" style="margin-bottom: 20px">
                            <div class="container-fluid" style="margin:0; padding:0;">
                                <div class="row-fluid" style="padding: 1% 0">
                                    <div class="span12">
                                        
                                       
                                        <div class="continue1">
                                            <button id="registerButton" type="submit" class="conti" ><?php echo CC_SUBMIT; ?></button>
                                        </div>
                                    </div>
                                </div> 
                            </div>   
                        </div>
                       </form>
                    </div>                 
                </div>
            </div>
        </div>
