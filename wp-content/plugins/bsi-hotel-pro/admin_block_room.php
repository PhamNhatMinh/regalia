<?php 
global $wpdb;
 
if((isset($_GET['action'])) && ($_GET['action'] == "unblock")){

	
	
	include("includes/db.conn.php");

	include("includes/conf.class.php");

	$booking_id  = $bsiCore->ClearInput($_GET['bid']);

	$roomtype_id = $bsiCore->ClearInput($_GET['rti']);

	$capacity_id = $bsiCore->ClearInput($_GET['cid']);

	$num      = $wpdb->get_row("SELECT count(*) from bsi_reservation br, bsi_bookings bb, bsi_roomtype brt where br.bookings_id=".$booking_id." and

						 bb.is_block=1 and br.room_type_id=brt.roomtype_id group by br.room_type_id",ARRAY_A);

	//$num=mysql_num_rows($result);  

	if(!empty($num)){

		$wpdb->query("delete from bsi_reservation where bookings_id=".$booking_id." and room_type_id=".$roomtype_id);

		$wpdb->query("delete from bsi_bookings where booking_id=".$booking_id."");

	}else{

		$wpdb->query("delete from bsi_reservation where bookings_id=".$booking_id." and room_type_id=".$roomtype_id);	

	}

	header("location:admin.php?page=admin-block-room");

	exit;

}



include("includes/conf.class.php");

include("includes/admin.class.php");
//wp_enqueue_script( 'custom_script10', plugins_url().'/bsi-hotel-pro/js/bsi_datatables.js');
wp_enqueue_script( 'custom_script11', plugins_url().'/bsi-hotel-pro/js/DataTables/jquery.dataTables.js');
wp_enqueue_style('custom-style9', plugins_url().'/bsi-hotel-pro/css/data.table.css');
wp_enqueue_style('custom-style10', plugins_url().'/bsi-hotel-pro/css/jqueryui.css');
?>


<p>&nbsp;</p>
<div id="container-inside">

<span style="font-size:16px; font-weight:bold"><?php echo ROOM_BLOCK_LIST;?></span>

    <input type="button" value="<?php echo CLICK_HERE_TO_BLOCK_ROOM;?>" onClick="window.location.href='admin.php?page=block-room'" style="background: #EFEFEF; float:right;"/>

  <hr style="margin-top:10px;" />

  <table class="display datatable" border="0">

    <thead>

      <tr>

        <th><?php echo DESCRIPTION_AND_NAME;?></th>

        <th><?php echo DATE_RANGE;?></th>

        <th><?php echo ROOMTYPE_CAPACITY;?></th>

        <th><?php echo ADMIN_BLOCK_TOTAL_ROOM;?></th> 

        <th></th>

      </tr>

    </thead>

    <?php echo $bsiAdminMain->getBlockRoomDetails();?>

  </table>

</div>

<script>
 jQuery(document).ready(function() {
	 	var oTable = jQuery('.datatable').dataTable( {
				"bJQueryUI": true,
				"sScrollX": "",
				"bSortClasses": false,
				"aaSorting": [[0,'desc']],
				"bAutoWidth": true,
				"bInfo": true,
				"sScrollY": "100%",	
				"sScrollX": "100%",
				"bScrollCollapse": true,
				"sPaginationType": "full_numbers",
				"bRetrieve": true,
				"oLanguage": {
								"sSearch": "Search:",
								"sInfo": "Showing _START_ to _END_ of _TOTAL_ entries",
								"sInfoEmpty": "Showing 0 to 0 of 0 entries",
								"sZeroRecords": "No matching records found",
								"sInfoFiltered": "(filtered from _MAX_ total entries)",
								"sEmptyTable": "No data available in table",
								"sLengthMenu": "Show _MENU_ entries",
								"oPaginate": {
												"sFirst":    "First",
												"sPrevious": "Previous",
												"sNext":     "Next",
												"sLast":     "Last"
											  }
							 }
	} );
} );

</script>



