<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 *  Tempalte setting mail option
 *
 * @version		1.0
 * @package		AweBooking/admin/
 * @author 		AweTeam
 */

$list_setting_email_option = array(
	array(
		'class' => 'form-elements',
		'name' => __('Recipient(s)', 'awebooking'),
		'type' => array('type'	=> 'textarea','name' => '','cols'=>'60','class' => '','id' => '','placeholder' =>  '','value' =>  ''),
		'desc' => 'Enter recipients (comma separated) for this email. Defaults to <code>admin@gmail.com</code>',
	),
	array(
		'class' => 'form-elements',
		'name' => __('Enable/Disable', 'awebooking'),
		'type' => array('type'	=> 'checkbox','name' => '','class' => '','id' => '','placeholder' =>  '','value' =>  '1'),
		'desc' => 'Enable this email notification',
	),
	array(
		'class' => 'form-elements',
		'name' => __('Subject', 'awebooking'),
		'type' => array('type'	=> 'text','name' => '','class' => '','id' => '','placeholder' =>  'Cancelled order','value' =>  ''),
		'desc' => 'This controls the email subject line. Leave blank to use the default subject: Cancelled order.',
	),
	array(
		'class' => 'form-elements',
		'name' => __('Email Heading', 'awebooking'),
		'type' => array('type'	=> 'text','name' => '','class' => '','id' => '','placeholder' =>  'Cancelled order','value' =>  ''),
		'desc' => 'This controls the main heading contained within the email notification. Leave blank to use the default heading: Cancelled order.',
	),
);
?>
<h2><?php _e( 'Cancelled booking','awebooking' ); ?></h2>
<p><?php _e( 'Cancelled booking emails are sent to the recipient list when bookings have been marked cancelled ( Delete )','awebooking' ); ?>.</p>
<?php
foreach ($list_setting_email_option  as $item) {
	?>
	<div class="<?php echo esc_attr($item['class']) ?>">
	    <p><?php echo esc_attr($item['name']) ?></p>
	    <div class="form-input">
	       <?php
	       if($item['type']['type'] == "text"){
	       		AWE_function::apb_gen_input(array(
                    'type'          => $item['type']['type'],
                    'name'          => $item['type']['name'],
                    'placeholder'   => $item['type']['placeholder'],
                    'class'         => $item['type']['class'],
                    'id'         	=> $item['type']['id'],
                    'value'			=> $item['type']['value']
                ));
	       }
	       if($item['type']['type'] == "checkbox"){
	       		AWE_function::apb_gen_input(array(
                    'type'          => $item['type']['type'],
                    'name'          => $item['type']['name'],
                    'placeholder'   => $item['type']['placeholder'],
                    'class'         => $item['type']['class'],
                    'id'         	=> $item['type']['id'],
                    'value'			=> $item['type']['value']
                ));
                echo $item['desc'];
	       }
	       if($item['type']['type'] == 'textarea'){
				echo "<textarea name=\"{$item['type']['name']}\" cols=\"{$item['type']['cols']}\">{$item['type']['value']}</textarea>";
			}

	       ?>
	    </div>
	     <span class="description">
		   <?php
			   if($item['type']['type'] != "checkbox" ){
			   	 echo ($item['desc']);
			   }
		    ?>
		</span>
	</div>

	<?php
}
?>
	<div class="form-elements">
		<p><?php _e('HTML template', 'awebooking' ); ?></p>
		<div class="form-input">
		 <?php
	       wp_editor(
	          "", "message_not_room",
	          array(
	              'media_buttons' => false,
	              'teeny' => true,
	              'textarea_rows' => '10',
	              )
	          );

	     ?>
		</div>
	</div>
