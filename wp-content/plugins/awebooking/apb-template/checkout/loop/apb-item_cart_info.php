<?php
/**
 * The template for displaying loop item for cart
 *
 * Override this template by copying it to your theme
 *
 * @author  AweTeam
 * @package AweBooking/Templates
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<!-- ITEM -->
<div class="apb-room-selected_item">
    <h6><?php printf( esc_html__( 'Room %d', 'awebooking' ), absint( $room_num ) ); ?></h6>
	<div class="apb-room-seleted_name has-package">
		<h2><?php echo esc_html( $room->post_title ); ?></h2>
	</div>
	<span class="apb-option">
		<?php printf( esc_html__( '%d Adult', 'awebooking' ), absint( $item['adult'] ) ); ?>, <?php printf( esc_html__( '%d Child', 'awebooking' ), absint( $item['child'] ) ); ?>
	</span>

	<div class="apb-room-seleted_package">
		<h6><?php esc_html_e( 'Price/Night', 'awebooking' ); ?></h6>
		<ul>
			<?php
			$year_from = date( 'Y', strtotime( $item['from'] ) );
			$year_to = date( 'Y', strtotime( $item['to'] ) );
			$info_price_day = AWE_function::get_pricing_of_days( $item['from'], $item['to'], $room_type->ID );
			foreach ( $info_price_day as $month => $list_day ) {
				foreach ( $list_day as $day => $price_day ) {
					if ( $year_from < $year_to ) {
						if ( intval( $month ) > 6 ) {
							$year = $year_from;
						} else {
							$year = $year_to;
						}
					} else {
						$year = $year_from;
					}
					?>
					<li>
						<span class="apb-room-seleted_date"><?php echo esc_html( date( get_option( 'date_format' ), strtotime( $month . '/' . $day . '/' . $year ) ) ); ?></span>
						<span class="apb-amount"><?php echo wp_kses_post( AWE_function::apb_price( $price_day ) ); ?></span>
					</li>
					<?php
				}
			}
			?>
		</ul>

		<?php $extra = ''; ?>
		<?php if ( ! empty( $extra_adult ) || ! empty( $extra_child ) ) : ?>
			<?php
			if ( is_array( $extra_adult ) ) {
				foreach ( $extra_adult as $item_extra_adult ) {
					if ( $item['adult'] == $item_extra_adult['number'] ) {
						ob_start();
						?>
						<li>
							<span class="apb-room-seleted_date">
								<?php printf( esc_html__( '%s Adult', 'awebooking' ), absint( $item_extra_adult['number'] ) ); ?>
								+
								<?php printf( esc_html__( '%s/night', 'awebooking' ), wp_kses_post( AWE_function::apb_price( $item_extra_adult['price'] ) ) ) ?>
							</span>
							<span class="apb-amount"><?php echo wp_kses_post( AWE_function::apb_price( $item_extra_adult['price'] ) ) ?> × <?php echo absint( count( $range_date ) - 1 ); ?></span>
						</li>
						<?php
						$extra .= ob_get_clean();
					}
				}
			}
			?>
			<?php
			if ( is_array( $extra_child ) ) {
				foreach ( $extra_child as $item_extra_child ) {
					if ( $item['child'] == $item_extra_child['number'] ) {
						ob_start();
						?>
						<li>
							<span class="apb-room-seleted_date">
								<?php printf( esc_html__( '%s Child', 'awebooking' ), absint( $item_extra_child['number'] ) ); ?>
								+
								<?php printf( esc_html__( '%s/night', 'awebooking' ), wp_kses_post( AWE_function::apb_price( $item_extra_child['price'] ) ) ) ?>
							</span>
							<span class="apb-amount"><?php echo wp_kses_post( AWE_function::apb_price( $item_extra_child['price'] ) ) ?> × <?php echo absint( count( $range_date ) - 1 ); ?></span>
						</li>
						<?php
						$extra .= ob_get_clean();
					}
				}
			}
			?>

			<?php
			if ( ! empty( $extra ) ) {
				echo '<h6>' . esc_html__( 'Extra price', 'awebooking' ) . '</h6>';
				echo '<ul>';
				echo wp_kses_post( $extra );
				echo '</ul>';
			}
			?>
		<?php endif ?>

		<?php if ( ! empty( $item['sale_info'] ) ) { ?>
			<h6><?php esc_html_e( 'Discount price', 'awebooking' ); ?></h6>
			<ul>
				<li>
					<span class="apb-room-seleted_date"><?php esc_html_e( 'Sale', 'awebooking' ); ?></span>
					<?php
					if ( 'sub' == $item['sale_info']['sale_type'] ) {
						?>
						<span class="apb-amount"><?php echo wp_kses_post( AWE_function::get_symbol_of_sale( $item['sale_info']['sale_type'] ) . AWE_function::apb_price( $item['sale_info']['amount'] ) ); ?></span>
						<?php
					} else {
						?>
						<span class="apb-amount">-<?php echo wp_kses_post( $item['sale_info']['amount'] . AWE_function::get_symbol_of_sale( $item['sale_info']['sale_type'] ) ); ?></span>
						<?php
					}
					?>
				</li>
			</ul>
		<?php } ?>


		<?php if ( ! empty( $item['package_data'] ) ) : ?>
			<h6><?php esc_html_e( 'Package', 'awebooking' ) ?></h6>
			<ul>
				<?php
				foreach ( $item['package_data'] as $info_package ) :
					$getPackage = AWE_function::get_room_option( $room_type->ID, 'apb_room_type' );
					foreach ( $getPackage as $item_package ) {
						if ( $item_package->id == $info_package['package_id'] ) {
							?>
							<li>
								<span class="apb-room-seleted_date"><?php echo esc_html( $item_package->option_name ) ?></span>
								<span class="apb-amount">
									<?php
									if ( $item_package->revision_id ) {
										echo wp_kses_post( AWE_function::apb_price( $item_package->option_value ) . ' x ' . ( count( $range_date ) - 1 ) );
									} else {
										echo wp_kses_post( AWE_function::apb_price( $item_package->option_value ) . ' x ' . $info_package['total'] );
									}
									?>
								</span>
							</li>
							<?php
						}
			  		}
			 	endforeach;
			?>
		</ul>
		<?php endif; ?>
	</div>

	<input type="hidden" name="apb-price[]" value="<?php echo ( float ) $price; ?>">
	<input type="hidden" name="apb-room_id[]" value="<?php echo absint( $item['room_id'] ) ?>">
	<input type="hidden" name="apb-from[]" value="<?php echo esc_attr( $item['from'] ) ?>">
	<input type="hidden" name="apb-to[]" value="<?php echo esc_attr( $item['to'] ) ?>">
	<input type="hidden" name="apb-adult[]" value="<?php echo absint( $item['adult'] ) ?>">
	<input type="hidden" name="apb-child[]" value="<?php echo absint( $item['child'] ) ?>">
	<input type="hidden" name="apb-package[]" value="<?php echo esc_attr( serialize( $item['package_data'] ) ) ?>">
	<div class="apb-room-seleted_total-room">
		<?php esc_html_e( 'Total', 'awebooking' ); ?>
		<span class="apb-amount"><?php echo wp_kses_post( AWE_function::apb_price( $price ) ); ?></span>
	</div>

</div>
<!-- END / ITEM -->
